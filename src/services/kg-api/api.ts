import { HttpResponse } from '@/types/global';
import { httpSB, http, toTime } from '@/common';
import qs from 'qs';

import {
  EventSummaryApi,
  // ClassifyItem,
  PlaceBetPrams,
  LeagueListPrams,
  SingleTicketData,
  EventsAndMarketsPrams,
  PlaceParlayBetData,
  OddTypsList,
  MatchItem,
  EventsAndMarkets
} from './types';

import { ParlayTicketPrams, SingleTicketPrams, PlacePreBetData, LeagueItem } from '@/services';

/**
 * @description: 可以获取到滚球、早盘、今日等类目的体育项目类型数量
 * @param language 语种
 * @return {Promise<HttpResponse>}
 */
const getEventSummaryApi = (): Promise<[boolean, EventSummaryApi[]]> => {
  return http.post('/agent/getEventSummary');
};

/**
 * 03:获取赛事和市场数据
 * @description: 传体育运动项目id、赛事区间、赛事id取得当下的赛事盘口信息
 * @param {number} oddsType 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
 * @param {string} sportId 体育项目ID
 * @param {number} periodId 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球
 * @param {string} leagueIds 联赛ids,以,隔开
 * @param {number} selectDate 查询向后指定天数偏移的matches(赛事)数据，传值范围：1-7，当periodId传值为2时，此值才会生效。
 * @param {string} eventIds 指定赛事 ID 来获取赛事状态(包括半场比分数据)
 * @return {Promise<HttpResponse>}
 */

const queryEventsAndMarkets = ({
  oddsType,
  sportId,
  periodId,
  leagueIds,
  queryCount,
  selectDate,
  eventIds,
  isParlay
}: EventsAndMarketsPrams): Promise<HttpResponse> => {
  return http.post<HttpResponse<EventsAndMarkets[]>>('/agent/queryEventsAndMarkets', {
    oddsType,
    sportId,
    periodId,
    leagueIds,
    queryCount,
    selectDate,
    eventIds,
    isParlay
  });
};

/**
 * 05:联赛列表数据
 * @description: 通过体育运动项目id、赛事区间获取联赛列表数据
 * @param {number} periodId 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球
 * @param {number} sportId 体育项目ID
 * @return {Promise<HttpResponse>}
 */
const queryLeagueList = (prams: LeagueListPrams): Promise<[boolean, LeagueItem[]]> => {
  return httpSB.post('/agent/queryLeagueList', prams);
};

/**
 * 06:单条预投注
 * @description: 用于更新单注的盘口数据
 * @param {number} sportType 体育项目ID
 * @param {string} marketId 赔率ID
 * @param {string} key 从queryEventsAndMarkets接口取得,并可参考沙巴附件-投注类型与选项
 * @param {number} oddsType 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
 * @return {Promise<HttpResponse>}
 */
const getSingleTicket = (params: any): Promise<HttpResponse> => {
  return httpSB.post<HttpResponse<SingleTicketData>>('/sports/im/bet/player/getSingleTicket', params);
};

/**
 * 07:单条投注
 * @description: 用户单条投注
 * @param {number} sportType 体育项目ID
 * @param {string} marketId 赔率ID
 * @param {string} price 赔率
 * @param {string} stake 投注金额
 * @param {string} point 点数
 * @param {number} oddsOption 赔率选项 0:不接受盘口变更 (预设)、 1:只接受更好赔率、 2:接受任何赔率
 * @param {number} oddsType 盘口类型 0、默认赔率|1、马来赔率|2、香港赔率|3、Decimal赔率|4、印地赔率|5、美国赔率
 * @param {string} key 从queryEventsAndMarkets接口取得,并可参考沙巴附件-投注类型与选项
 * @return {Promise<HttpResponse>}
 */
const doPlaceBet = ({
  sportType,
  marketId,
  price,
  stake,
  point,
  key,
  oddsType,
  oddsOption
}: PlaceBetPrams): Promise<HttpResponse> => {
  return httpSB.post<HttpResponse<any>>('/sports/im/bet/player/doPlaceBet', {
    sportType,
    marketId,
    price,
    stake,
    point,
    oddsType,
    oddsOption,
    key
  });
};

/**
 * 08:串关预投注
 * @description: 用户单条投注
 * @param {number} sportType 体育项目ID
 * @param {string} marketId 赔率ID
 * @param {string} price 赔率
 * @param {string} stake 投注金额
 * @param {string} point 点数
 * @param {number} oddsOption 赔率选项 0:不接受盘口变更 (预设)、 1:只接受更好赔率、 2:接受任何赔率
 * @param {number} oddsType 盘口类型 0、默认赔率|1、马来赔率|2、香港赔率|3、Decimal赔率|4、印地赔率|5、美国赔率
 * @param {string} key 从queryEventsAndMarkets接口取得,并可参考沙巴附件-投注类型与选项
 * @return {Promise<HttpResponse>}
 */
const getParlayTickets = (prams: SingleTicketPrams[]): Promise<HttpResponse> => {
  return httpSB.post<HttpResponse<PlacePreBetData>>('/sports/im/bet/player/getParlayTickets', prams);
};

/**
 * 08-a:串关投注
 * @description: 用户串关投注
 * @param {array} matches 串关投注信息，需要组装成jsonArray即数组对象传入，具体值烦请直接对照沙巴文档placeParlayBet接口
 * @param {array} combos 串关投注组合，需要组装成jsonArray即数组对象传入，具体值烦请直接对照沙巴文档placeParlayBet接口
 * @param {string} oddsOption 下注选项 0:不接受盘口变更 (预设)、 1:接受任何赔率
 * @return {Promise<HttpResponse>}
 */
const doPlaceParlayBet = (prams: ParlayTicketPrams): Promise<HttpResponse> => {
  return httpSB.post<HttpResponse<PlaceParlayBetData>>('/sports/im/bet/player/placeParlayBet', prams);
};

// /**
//  * @description: 获取投注分类信息
//  * @return {*}
//  */
// const getPlayClassify = (): any => {
//   return httpSB.post<HttpResponse<ClassifyItem[]>>('/sports/im/player/getClassify');
// };

/**
 * @description: 赛事盘口推荐
 * @param {string} sportId 体育id
 * @param {string} periodId 时间类型
 * @param {string} oddsType 盘口类型
 * @param {string} source 数据源
 * @return {*}
 */
const getRecommendedMarketList = (prams: { oddsType: OddTypsList; source: string }): Promise<HttpResponse> => {
  return httpSB.get<HttpResponse<MatchItem[]>>('/sports/nano/player/recommendedMarketList?' + qs.stringify(prams));
};

/**
 * 批量预投注
 * @param params
 * @returns
 */
// interface BetData {
//   betData: {
//     sort: number;
//     sportId: number; //4
//     matchId: number; //2
//     marketId: string; //3
//     point: string; //6
//     price: number; //5
//     betTeam: string;
//     oddsId: number | null; //7
//     betType: number; //1
//     marketType: string;
//   };
// }
// interface Bet {
//   oddsType: number;
//   sort: number;
//   betMode: number;
//   betData: BetData;
// }
const getParlayApi = (): Promise<HttpResponse> => {
  const body = {
    bet: [
      {
        oddsType: 1,
        sort: 1,
        betMode: 1,
        betData: [
          {
            sort: 1,
            sportId: 2, //4
            matchId: 54009224, //2
            marketId: '434565613', //3
            point: '155', //6
            price: 1.91, //5
            betTeam: 'h',
            oddsId: null, //7
            betType: 3, //1
            marketType: '2'
          }
        ]
      }
    ]
  };

  console.log('SSSSSSSS121', body);
  return httpSB.post<HttpResponse<MatchItem[]>>(
    '/agent/getBatchTickets',
    { ...body },
    {
      headers: {
        country: 'zh',
        tenant: '1',
        'os-type': '1',
        token: 'test1-8FAA5F19639148A39AAC4277E79D32D0'
      }
    }
  );
};

/**
 * 获取赛事列表补充信息
 *
 * @param {string} eventIds 赛事ID  ，隔开
 * @return {*}
 */
const getMatchInfoByIds = (eventIds: string): any => {
  return httpSB.post<HttpResponse<any>>('/sports/im/player/getGameDetail', {
    eventIds
  });
};

/**
 * 批量预投注
 * @param params
 * @returns
 */
const doBetApi = (params: Array<SingleBetParam | ParlyBetParam>): Promise<HttpResponse> => {
  return httpSB.post<HttpResponse<MatchItem[]>>('/sports/im/bet/player/batchPlaceBet', params);
};

/**
 * @description:赛果接口
 * @param {string} sportId 体育id
 * @param {string} startTime 开始时间
 * @param {string} endTime 结束时间
 * @param {string} leagueId 联赛id列表
 * @return {*}
 */
const getResultsApi = ({
  sportId,
  startTime,
  endTime,
  leagueId
}: {
  sportId: number;
  startTime: number;
  endTime: number;
  leagueId?: string;
}): Promise<[boolean, { resultsInfo: any[] }]> => {
  //getMarchResults
  return http.post<HttpResponse<any>>(
    '/agent/getMatchResults?' +
      qs.stringify({
        gameId: sportId,
        startTime: toTime(startTime, 'YYYY-MM-DD HH:mm:ss'),
        endTime: toTime(endTime, 'YYYY-MM-DD HH:mm:ss'),
        oddsType: 2,
        sportId: 6,
        periodId: 2,
        leagueId
      })
  );
};

interface DoBetMustParam {
  OpenParlay: boolean;
  sort: string;
}
export type SingleBetParam = DoBetMustParam & DoBetParamItem;
export type ParlyBetParam = DoBetMustParam & ParlySelfParam;
interface ParlySelfParam {
  parlay_odds_option: boolean;
  bet_matches: DoBetParamItem[];
  bet_combos: {
    combo_type: number;
    stake: number;
    bet_count: number;
  }[];
}
interface DoBetParamItem {
  bettype: string;
  customerIP?: string;
  isComboAcceptAnyOdds: boolean;
  key: string;
  live_away_score: string;
  live_home_score: string;
  market: string;
  marketId: number;
  matchId: string;
  oddsType: number;
  outrightTeamId: number;
  point: number;
  price: string;
  sportType: number;
  stake?: number;
  wagerSelectionId: string;
}

/**
 * @description: 查获取视频链接
 * @return {Promise<HttpResponse>}
 */
const getSportVideoByMatchID = (apiUrl: string): Promise<HttpResponse<any>> => {
  return http.get<HttpResponse<any>>(apiUrl);
};

export {
  doPlaceBet,
  getSportVideoByMatchID,
  queryLeagueList,
  getSingleTicket,
  getParlayTickets,
  doPlaceParlayBet,
  getMatchInfoByIds,
  getRecommendedMarketList,
  queryEventsAndMarkets,
  getEventSummaryApi,
  getParlayApi,
  doBetApi,
  getResultsApi
};
