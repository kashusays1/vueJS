import { ref, watch, reactive, nextTick, defineComponent } from 'vue';
import { get } from 'lodash-es';
// Composables
import { useScrollParent } from '@vant/use';
import { useI18n } from 'vue-i18n';
import { useTouch } from '@/hooks';
// Utils
import { preventDefault, getScrollTop } from './utils';
import './style.less';

export type ScrollElement = Element | Window;

const DEFAULT_HEAD_HEIGHT = 80;
const DEFAULT_DURATION = 300;
const LOADING_TEXT = '正在加载中...';
const DEFAULT_BOTTOM_HEIGHT = -80;

// const TEXT_STATUS = ['pulling', 'loosing', 'success'];

type PullRefreshStatus = 'normal' | 'loading' | 'loosing' | 'pulling' | 'success';

export default defineComponent({
  name: 'MobilePullRefresh',
  props: {
    successText: String,
    pullDistance: [Number, String],
    modelValue: {
      type: Boolean,
      default: false
    },
    successDuration: {
      type: [Number, String],
      default: 500
    }
  },
  emits: ['refresh', 'update:modelValue'],

  setup(props, { emit, slots }) {
    let reachTop: boolean;
    let reachBottom: boolean;
    const { t } = useI18n();
    const root = ref<HTMLElement>();
    const scrollParent = useScrollParent(root);
    const state = reactive({
      status: 'normal' as PullRefreshStatus,
      distance: 0,
      duration: 0,
      distanceBottom: 0
    });

    const touch = useTouch();

    const isTouchable = () => state.status !== 'loading' && state.status !== 'success';

    const ease = (distance: number) => {
      const pullDistance = +(props.pullDistance || DEFAULT_HEAD_HEIGHT);

      if (distance > pullDistance) {
        if (distance < pullDistance * 2) {
          distance = pullDistance + (distance - pullDistance) / 2;
        } else {
          distance = pullDistance * 1.5 + (distance - pullDistance * 2) / 4;
        }
      }

      return Math.round(distance);
    };

    const setStatus = (distance: number, isLoading?: boolean) => {
      const pullDistance = +(props.pullDistance || DEFAULT_HEAD_HEIGHT);
      state.distance = distance;

      if (isLoading) {
        state.status = 'loading';
      } else if (distance === 0) {
        state.status = 'normal';
      } else if (distance < pullDistance) {
        state.status = 'pulling';
      } else {
        state.status = 'loosing';
      }
    };

    const LoadingIconRender = () => (
      <div class="pull-refresh__loading-icon">
        <svg-icon name="loading_v2" />
        <div class="pull-refresh__loading-text">{LOADING_TEXT}</div>
      </div>
    );

    const renderStatus = () => {
      const { status, distance } = state;
      if (slots[status]) {
        return slots[status]!({ distance });
      }
      const nodes: any = [<LoadingIconRender />];
      return nodes;
    };

    const showSuccessTip = () => {
      state.status = 'success';

      setTimeout(() => {
        setStatus(0);
        setStatusBottom(0);
      }, +props.successDuration);
    };

    const checkPosition = (event: TouchEvent) => {
      reachTop = getScrollTop(scrollParent.value!) === 0;
      if (reachTop) {
        state.duration = 0;
        touch.start(event);
      }
    };

    const onTouchStart = (event: TouchEvent) => {
      if (isTouchable()) {
        checkPosition(event);
      }
      onTouchStartBottom(event);
    };

    const onTouchMove = (event: TouchEvent) => {
      if (isTouchable()) {
        if (!reachTop) {
          checkPosition(event);
        }

        const { deltaY } = touch;
        touch.move(event);

        if (reachTop && deltaY.value >= 0 && touch.isVertical()) {
          preventDefault(event);
          setStatus(ease(deltaY.value));
        }
      }
      onTouchMoveBottom(event);
    };

    const onTouchEnd = () => {
      if (reachTop && touch.deltaY.value && isTouchable()) {
        state.duration = +DEFAULT_DURATION;

        if (state.status === 'loosing') {
          setStatus(+DEFAULT_HEAD_HEIGHT, true);
          emit('update:modelValue', true);

          // ensure value change can be watched
          nextTick(() => emit('refresh'));
        } else {
          setStatus(0);
        }
      } else {
        onTouchEndBottom();
      }
    };

    const setStatusBottom = (distance: number, isLoading?: boolean) => {
      const pullDistance = +(props.pullDistance || DEFAULT_BOTTOM_HEIGHT);
      state.distanceBottom = distance;

      if (isLoading) {
        state.status = 'loading';
      } else if (distance === 0) {
        state.status = 'normal';
      } else if (+distance > pullDistance) {
        state.status = 'pulling';
      } else {
        state.status = 'loosing';
      }
    };
    const checkPositionBottom = event => {
      const scrollTop = getScrollTop(scrollParent.value!);
      const scrollHeight = get(scrollParent.value, 'scrollHeight');
      const clientHeight = get(scrollParent.value, 'clientHeight');
      reachBottom = scrollHeight - clientHeight <= Math.ceil(scrollTop);
      if (reachBottom) {
        state.duration = 0;
        touch.start(event);
      }
    };
    const onTouchStartBottom = (event: TouchEvent) => {
      if (isTouchable()) {
        checkPositionBottom(event);
      }
    };
    const onTouchEndBottom = () => {
      if (reachBottom && touch.deltaY.value && isTouchable()) {
        state.duration = +DEFAULT_DURATION;

        if (state.status === 'loosing') {
          setStatusBottom(DEFAULT_BOTTOM_HEIGHT, true);
          emit('update:modelValue', true);

          // ensure value change can be watched
          nextTick(() => emit('refresh'));
        } else {
          setStatusBottom(0);
        }
      }
    };

    const onTouchMoveBottom = (event: TouchEvent) => {
      if (isTouchable()) {
        if (!reachBottom) {
          checkPositionBottom(event);
        }

        const { deltaY } = touch;
        touch.move(event);
        if (reachBottom && deltaY.value <= 0) {
          preventDefault(event);
          setStatusBottom(deltaY.value);
        }
      }
    };
    watch(
      () => props.modelValue,
      value => {
        state.duration = +DEFAULT_DURATION;

        if (value) {
          setStatus(+DEFAULT_HEAD_HEIGHT, true);
          setStatusBottom(DEFAULT_BOTTOM_HEIGHT, true);
        } else if (slots.success || props.successText) {
          showSuccessTip();
        } else {
          setStatus(0, false);
          setStatusBottom(0, false);
        }
      }
    );

    return () => {
      const trackStyle = {
        transitionDuration: `${state.duration}ms`,
        transform:
          state.distance && reachTop
            ? `translate3d(0,${state.distance}px, 0)`
            : state.distanceBottom && reachBottom
            ? `translate3d(0,${state.distanceBottom}px, 0)`
            : ''
      };

      return (
        <div ref={root} class="pull-refresh">
          <div
            class={'pull-refresh__track'}
            style={trackStyle}
            onTouchstart={onTouchStart}
            onTouchmove={onTouchMove}
            onTouchend={onTouchEnd}
            onTouchcancel={onTouchEnd}
          >
            <div class={'pull-refresh__head'} style={{ height: `${DEFAULT_HEAD_HEIGHT}px` }}>
              {renderStatus()}
            </div>
            {slots.default?.()}
            <div class="pull-refresh__no-data" style={{ height: `${DEFAULT_BOTTOM_HEIGHT}px` }}>
              {t('lang.sport_common_noMoreData')}
            </div>
          </div>
        </div>
      );
    };
  }
});
