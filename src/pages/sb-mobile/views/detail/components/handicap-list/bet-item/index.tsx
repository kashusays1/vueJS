import { computed, defineComponent, inject, PropType, ref, watch } from 'vue';
import './style.less';
import { MarketStatusEnum } from '@/common/enums';
import { HandicapItem } from '@/services';
import store from '@/store';
import { useBet } from '@/hooks/useBet';
import { Toast } from 'vant';

export default defineComponent({
  name: 'BetItem',
  props: {
    oddInfo: {
      require: true,
      type: Object as PropType<HandicapItem>,
      default: () => {}
    },
    marketStatus: {
      type: Number,
      default: 0
    },
    oddsId: {
      type: String,
      default: ''
    },
    activeBetItem: {
      type: Boolean,
      default: false
    }
  },
  emits: ['handle-prebet', 'add-bet'],
  setup(props, { emit }) {
    // const store = useStore();
    const floatClass = ref('');
    const { isParlayComputed } = useBet();

    const matchId = inject('matchId') as number;

    // 监听值
    watch(
      (): HandicapItem => props.oddInfo,
      (newValue, oldValue) => {
        const newVal = newValue.value;
        const oldVal = oldValue.value;
        if (newVal > oldVal) {
          floatClass.value = 'up-icon';
        } else if (newVal < oldVal) {
          floatClass.value = 'down-icon';
        } else if (newVal === oldVal) {
          floatClass.value = '';
        }
        if (floatClass.value === '') return;
        setTimeout(() => {
          floatClass.value = '';
        }, 5 * 1000);
      }
    );
    // 投注统计
    const betCounter = computed(() => store.getters.betCounter);
    // 串关缓存id
    const currentOddsId = computed(() => store.getters.currentOddsId);

    const isUnlock = computed(() => props.marketStatus !== MarketStatusEnum['running'] || props.oddInfo.value <= 0);

    // 添加投注
    const onHandleAddSportBet = () => {
      if (isUnlock.value) {
        return;
      }
      if (isParlayComputed.value && !currentOddsId.value[matchId] && betCounter.value >= 10) {
        Toast('超过最大串关数');
        return;
      }
      emit('add-bet');
    };

    return () => (
      <div class={{ 'bet-item': true, 'is-bet-active': props.activeBetItem }} onClick={onHandleAddSportBet}>
        <div class="bet-item__type">{props.oddInfo.type}</div>
        <div class="bet-item__odd-value">
          {isUnlock.value ? (
            <div class="is-lock">
              <svg-icon name="lock_h5" />
            </div>
          ) : (
            <span class={['odd-number', floatClass.value]}>{props.oddInfo.value}</span>
          )}
        </div>
      </div>
    );
  }
});
