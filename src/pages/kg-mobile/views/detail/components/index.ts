import DataTotal from './data-total';
import TextLive from './text-live';
import RangeData from './range-data/index.vue';
import LineUp from './live-up';
import Intelligence from './intelligence/index.vue';

export { DataTotal, TextLive, RangeData, LineUp, Intelligence };
