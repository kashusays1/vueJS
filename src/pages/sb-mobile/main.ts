import { createApp } from 'vue';
import '../../public-path';
import './icons';
import '@/style/index.less';
// 黑金主题调整
// import '@/apps/h5/style/dark-golden.less';

import App from './app.vue';

// import { setupI18n } from '@/i18n/setupI18n';
import { initPlugin } from './plugins';
import { initRouter } from './router';
import { setupStore } from '@/store';
import { getThemeStyle, setCssVar, setEvent, setPercentHeight } from '@/common/utils';

import { ThemeEnum } from '@/common';
import { Fn } from '@/types/global';

let instance: any;

interface IRenderProps {
  BASE_URL?: string;
  themeColor?: string;
  themeStyle?: any;
  maintain?: string[];
  token?: string;
  id?: number;
  childListenH5Main?: {
    emit: Fn;
  };
  sportApiDomain?: string;
  apiUrl?: string;
}

// sportApiDomain: window.$sportDomainApi, // 体育接口BaseUrl
// apiUrl: window.tenant.apiUrl, // 平台接口BaseUr

async function render(props: IRenderProps = {}) {
  const { BASE_URL, themeStyle, maintain, childListenH5Main, sportApiDomain, apiUrl } = props;
  sportApiDomain && localStorage.setItem('sportApiDomain', sportApiDomain);
  apiUrl && localStorage.setItem('apiUrl', apiUrl);
  setEvent(childListenH5Main);
  instance = createApp(App);
  // 注册全局组件
  initPlugin(instance);
  const router = initRouter(BASE_URL);
  instance.use(router);
  setupStore(instance, !!maintain ? maintain : []);
  // 设置国际化
  // await setupI18n(instance);
  await router.isReady();
  // 设置主题色
  const defaultTheme = getThemeStyle();
  setCssVar(ThemeEnum[themeStyle || defaultTheme], themeStyle || defaultTheme);
  // store.dispatch('SET_THEME_STYLE', themeStyle || defaultTheme);
  // 设置100vh
  setPercentHeight();
  instance.mount('#microApp');
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
} else {
  // const el = document.getElementById('microApp');
  // el && el.classList.add('is-micro-app');
}

//暴露主应用生命周期钩子
export async function bootstrap() {
  console.log('[sports App] subapp bootstraped');
}

export async function mount(props: IRenderProps) {
  console.log('[sports App] 启动体育!', props);
  render(props);
}

export async function unmount() {
  console.log('[sports App] 卸载体育APP!');
  document.body.style.removeProperty('height');
  instance.unmount();
  instance = null;
}
