/**
 * 余额操作
 */

import { useAuth } from '@/hooks';
import { computed, ref } from 'vue';
import { useStore } from 'vuex';

export function useBlance({ platformCode }: { platformCode: string }) {
  const isFreshing = ref(false);
  const store = useStore();
  const { isLogin } = useAuth();

  const onHandleFresh = async () => {
    if (!isLogin || isFreshing.value) return;
    isFreshing.value = true;
    try {
      await store.dispatch('TRANSFER_INGAME_AMOUNT', { platformCode });
      await store.dispatch('GET_SPORTS_BALANCE');
    } catch (error) {
      isFreshing.value = false;
    } finally {
      setTimeout(() => {
        isFreshing.value = false;
      }, 1000);
    }
  };

  // 显示IM体育约
  const sportBalance = computed(() => {
    return store.getters.getBlacneBySoure('IM') || 0;
  });

  return {
    isFreshing,
    sportBalance,
    onHandleFresh
  };
}
