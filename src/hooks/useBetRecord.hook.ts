/**
 * 投注记录
 */

import { reactive } from 'vue';
import { getOrders, BetOrdersPrams, BetOrder } from '@/services';
import { get } from 'lodash-es';
import dayjs from 'dayjs';
import { Laoding } from 'kg-mobile/compoents';
import { isMobile } from '@/common';
import { Toast } from 'vant';
import { useSportOptins } from '@/hooks';

export const TAB_LIST: { label: string; value: null | 0 | 1 | -1 }[] = [
  {
    label: 'lang.sport_common_quanBu',
    value: null
  },
  {
    label: 'lang.sport_tape_noSettlement',
    value: 0
  },
  {
    label: 'lang.sport_tape_haveAlreadySettled',
    value: 1
  }
  // {
  //   label: '已取消',
  //   value: -1
  // }
];

export interface DateType {
  title: string;
  i18n: string;
  value: {
    beginTime: number;
    endTime: number;
  };
}

export const DATE_LIST: DateType[] = [
  {
    title: '今天',
    i18n: 'lang.sport_common_today',
    value: {
      beginTime: dayjs().startOf('day').valueOf(),
      endTime: dayjs().valueOf()
    }
  },
  {
    title: '昨天',
    i18n: 'lang.sport_common_yesterday',
    value: {
      beginTime: dayjs().startOf('day').subtract(1, 'days').valueOf(),
      endTime: dayjs().startOf('day').valueOf()
    }
  },
  {
    title: '7天内',
    i18n: 'lang.sport_common_last7days',
    value: {
      beginTime: dayjs().startOf('day').subtract(7, 'days').valueOf(),
      endTime: dayjs().valueOf()
    }
  },
  {
    title: '30天内',
    i18n: 'lang.sport_common_last30days',
    value: {
      beginTime: dayjs().subtract(29, 'day').startOf('day').valueOf(),
      endTime: dayjs().valueOf()
    }
  }
];

interface CountType {
  /**
   * 投注额
   */
  betAmount: number;
  /**
   * 投注统计
   */
  betCount: number;
  /**
   * 有效额
   */
  validAmount: number;
  /**
   * 输赢
   */
  winAmount: number;
  /**
   * 币种
   * currency
   */
  currency: string;
}

export function useBetRecord() {
  const { optionsStore } = useSportOptins();

  const betStore = reactive({
    betRecord: [] as BetOrder[],
    total: 0,
    count: {
      betAmount: 0,
      betCount: 0,
      validAmount: 0,
      winAmount: 0,
      currency: ''
    } as CountType
  });

  // 参数
  const form = reactive<BetOrdersPrams>({
    current: 1,
    size: 20,
    currency: optionsStore.value.currency,
    gameKind: 'IM_SPORT',
    status: TAB_LIST[0].value,
    ...DATE_LIST[0].value
  });

  const state = reactive({
    pullLoading: false, // 下拉加载请求
    pullUpLoading: false, // 上拉加载, H5专用
    pages: 10,
    activeTab: 0,
    avtiveFilterDate: 0,
    filterVisible: false,
    finished: false
  });

  /**
   * 获取投注记录
   */
  const getRecordData = async ({ isPull }: { isPull: boolean } = { isPull: false }) => {
    if (isPull) {
      state.pullLoading = true;
    } else {
      isMobile && Laoding.show();
    }
    const { result, data, message } = await getOrders(form);
    if (result) {
      betStore.betRecord = get(data, 'page.records');
      betStore.count.betCount = get(data, 'otherData.betCount');
      betStore.count.betAmount = get(data, 'otherData.betAmount');
      betStore.count.winAmount = get(data, 'otherData.winAmount');
      betStore.count.currency = get(data, 'otherData.currency');

      // betStore.betRecord.sort(
      //   (a: { creationDate: string | number | Date }, b: { creationDate: string | number | Date }) => {
      //     return dayjs(b.creationDate).valueOf() - dayjs(a.creationDate).valueOf();
      //   }
      // );
    } else {
      isMobile && Toast(message);
    }
    if (isPull) {
      state.pullLoading = false;
    } else {
      isMobile && Laoding.hide();
    }
  };

  const toggleDate = () => {
    state.filterVisible = !state.filterVisible;
  };

  const onTabChange = async (
    { key, index }: { key: 'activeTab' | 'avtiveFilterDate'; index: number },
    cb?: () => void
  ) => {
    if (state[key] === index) return;
    state[key] = index;
    switch (key) {
      case 'activeTab':
        form.status = TAB_LIST[index].value;
        break;
      case 'avtiveFilterDate':
        form.beginTime = DATE_LIST[index].value.beginTime;
        form.endTime = DATE_LIST[index].value.endTime;
        break;
      default:
        break;
    }
    form.current = 1;
    !!cb && cb();
    await getRecordData();
  };

  // 上拉加载
  const onListLoad = async () => {
    if (state.pullLoading) {
      betStore.betRecord = [];
      state.pullLoading = false;
    }
    // 请求
    form.current = form.current + 1;
    const { result, data } = await getOrders(form);
    if (result && get(data, 'page.records')) {
      betStore.betRecord = betStore.betRecord.concat(data.page.records);
    }
    state.pullUpLoading = false;
    // 达到条件停止
    if (form.current >= (get(data, 'page.pages') || 0)) {
      state.finished = true;
    }
  };

  return {
    onListLoad,
    onTabChange,
    toggleDate,
    state,
    form,
    betStore,
    getData: getRecordData
  };
}
