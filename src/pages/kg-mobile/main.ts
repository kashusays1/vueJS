import { createApp } from 'vue';
import './public-path';
import './style/index.less';
import './icons';

import App from './app.vue';

import { initPlugin } from './plugins';
import { initRouter } from './router';
import { setupStore } from '@/store';
import { setLoginData } from '@/common';
import { setTheme, ThemeType } from './style/themes';
import { setupI18n } from '@/i18n/setupI18n';
import { useSportOptins } from '@/hooks';
import { useUrlSearchParams } from '@vueuse/core';

let instance: any;

// interface IRenderProps {
//   BASE_URL?: string;
//   themeColor?: string;
//   themeStyle?: string;
//   token?: string;
//   id?: number;
// }

interface RenderProps {
  /**
   * 主题
   */
  theme?: ThemeType;
  /**
   * 语言
   */
  language: string;
  /**
   * 当前货币
   */
  currencySymbol: string;
  /**
   * 用户账户名
   */
  account: string;
  /**
   * 用户令牌
   */
  token: string;
  /**
   * 用户令牌过期时间戳
   */
  tokenExpired: number;
  /**
   * 返回主项目链接
   */
  backUrl?: string;
}

async function render() {
  instance = createApp(App);
  // 取消inject使用ref提示
  instance.config.unwrapInjectedRef = true;
  const { optionsStore, changeSetiing } = useSportOptins();

  // 路由url参数
  const { theme, token, language, currencySymbol, account, tokenExpired } = useUrlSearchParams(
    'history'
  ) as RenderProps;

  console.log('queryParams', { theme, token, language, currencySymbol, account, tokenExpired });

  const router = initRouter();
  instance.use(router);

  // 判断是否为跳转 必须参数
  if (token && language && currencySymbol && account && tokenExpired) {
    // 存登录信息
    setLoginData({ token, account, expireIn: tokenExpired });
  }

  // 设置主题
  setTheme(theme || 'default');
  changeSetiing('theme', theme || optionsStore.value.theme);

  // 设置国际化
  await setupI18n(instance, language || optionsStore.value.lang);

  // 3初始化vuex
  setupStore(instance);

  // 1. 注册全局组件
  initPlugin(instance);

  await router.isReady();
  instance.mount('#microApp');
}

render();

// // 独立运行时
// if (!window.__POWERED_BY_QIANKUN__) {
//   render();
// }

// //暴露主应用生命周期钩子
// export async function bootstrap() {
//   console.log('[sports App] subapp bootstraped');
// }

// export async function mount(props: RenderProps) {
//   console.log('[sports App] 启动体育!', props);
//   render();
// }

// export async function unmount() {
//   console.log('[sports App] 卸载体育APP!');
//   instance.unmount();
//   instance._container.innerHTML = '';
//   instance = null;
// }
