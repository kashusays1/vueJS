import { DataTypeEnum } from '@/common/enums';

type OddTypsList = 1 | 2 | 3 | 4 | 5;
export interface EventSummary {
  /**
   * 早盘赛事数量
   */
  early: number;
  /**
   * 滚球赛事数量
   */
  live: number;
  /**
   * 串关赛事数量
   */
  parlay: number;
  /**
   * 串关赛事数量
   */
  follow: number;
  /**
   * 运动项目 ID
   */
  sport_type: number;
  /**
   * 体育名称
   */
  sport_type_name: string;
  /**
   * 今日赛事数量
   */
  today: number;
}

export interface GetVirtualEventVideoForm {
  // 体育项目ID
  sportType: number;
  // 视频ID,从GetEvents 取得
  streamingOption: number;
  // 视频代码,从GetEvents 取得
  channelCode: string;
}

export interface EventsAndMarketsPrams {
  // 体育项目ID
  sportId: string | number;
  // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球 -1:关注 4: 滚球
  periodId: DataTypeEnum;
  // 查询联赛数据条数
  queryCount?: number;
  // 联赛ids,以,隔开
  leagueIds?: string;
  // 查询向后指定天数偏移的matches(赛事)数据，传值范围：1-7，当periodId传值为2时，此值才会生效。
  selectDate?: number;
  // 指定赛事 ID 来获取赛事状态(包括半场比分数据)
  eventIds?: string | number;
  // 是否为串关
  isParlay?: boolean;
  // 筛选类型
  betTypeIds?: string;
}

export interface MatchEventsPrams {
  // 1、足球 2、篮球 3、美式足球 4、冰上曲棍球 5、网球 6、排球
  sportId: number;
  // e: Early(早盘)、 l: Live(滚球) 、d: Dead ball(死球：包含早盘与今日赛事)、 t: Today(今日)
  marketType: string;
  // 赛事ID(不传会查全部赛事)
  eventIds?: string;
  betType?: number;
  oddsType?: OddTypsList;
}

export interface EventItem {
  ChangeTime: string;
  EventStatus: string;
  HasDeadBallParlay: boolean;
  IsClosed: boolean;
  AwayID: number;
  IsNeutral: boolean;
  EventCode: string;
  GameStatus: number;
  DelayLive: boolean;
  AwayRedCard: number;
  IsMainMarket: boolean;
  CountryCode: string;
  HomeRedCard: number;
  GameSession: number;
  LeagueName: string;
  InJuryTime: number;
  InPlayTime: string;
  LivePeriod: number;
  KickoffTime: string;
  LiveHomeScore: number;
  IsBreak: boolean;
  LiveAwayScore: number;
  IsTest: boolean;
  IsHT: boolean;
  HomeID: number;
  HomeName: string;
  EventID: number;
  HasLive: boolean;
  IsVirtualEvent: boolean;
  GlobalShowTime: string;
  LeagueID: number;
  HasLiveParlay: boolean;
  AwayName: string;
}
export interface MatchEventsData {
  event_version_key: number;
  events: EventItem[] | null;
}

export interface LeagueListPrams {
  // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球
  periodId: number;
  // 体育项目ID
  sportId: number;
  // 是否为串关
  isParlay: boolean;
  //选择日期
  selectDate?: boolean;
}

export interface ShaBaResults {
  //默认值是1 运动项目id  足球1 篮球2
  gameId: number;
  //获取完场 开始时间
  startTime: String;
  //获取完场 结束时间
  endTime: String;
}
// export interface LeagueItem {
//   // 联赛名称
//   leagueName: string;
//   // 联赛id
//   leagueId: string;
// }

export interface SingleTicketData {
  sport_type: number;
  market_id: number;
  is_decimal_type: boolean;
  point: string;
  key: string;
  bet_type: number;
  odds_type: number;
  price: number;
  status: string;
  max_bet: number;
  min_bet: number;
  live_home_score: number;
  live_away_score: number;
}

export interface PlaceBetPrams {
  // 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
  oddsType: OddTypsList;
  // 运动类型 1、足球 2、篮球 3、美式足球 4、冰上曲棍球 5、网球 6、排球
  sportType: number;
  // 赔率ID
  marketId: string | number;
  // 赔率
  price: number;
  // 投注金额
  stake: number;
  // 点数
  point: number;
  // 赔率选项 0:不接受盘口变更 (预设)、 1:只接受更好赔率、 2:接受任何赔率
  oddsOption?: number;
  // 除会员账号外，以上数据均可从GetMarkets或sbGetEventsAndMarkets接口返回的参数列表中获得选取后传入
  key: string;
}

export interface PlaceParlayBetData {
  combos: CombosItem[];
  price_info: ParlayPriceInfoItem[];
  bet_status: number;
  current_combos: ParlayCombosItem[];
  lucky_trans_id: number;
  max_bet: number;
  min_bet: number;
  parlay_bet_accept_sec: number;
  parlay_ticket_status: string;
  single_tickets: any[];
  sys_trans_id: number;
  total_stake: number;
  trans_id: number;
}

export interface ParlayPriceInfoItem {
  bet_price: number;
  bet_type: number;
  current_price: number;
  key: string;
  live_away_score: number;
  live_home_score: number;
  market_id: number;
  odds_type: number;
  point: number;
  sport_type: number;
  state_code: number;
  state_message: string;
}

export interface ParlayCombosItem {
  bet_count: number;
  combo_price: number;
  combo_type: string;
  min_bet: number;
  max_bet: number;
  odds: number;
  stake: number;
}
export interface CombosItem {
  bet_count?: number;
  combo_type?: string;
  max_bet?: number;
  min_bet?: number;
  odds?: string;
  BetCount?: number;
  ComboType?: string;
  MaxBet?: number;
  MinBet?: number;
  Odds?: number;
  combo_price: number;
  stake: number;
}

export interface PriceInfoItem {
  bet_type: number;
  current_price: number;
  key: string;
  live_away_score: number;
  live_home_score: number;
  market_id: number;
  odds_type: number;
  point: number;
  sport_type: number;
  state_code: number;
  state_message: string;
  status: string;
  combo_price: number;
  stake: number;
}

export type MacthMarketInfoPrams = MatchEventsPrams;

export interface RecommendedMatchData {
  mid: number;
  gid: number;
  home: string;
  away: string;
  startDate: string;
  startTime: string;
  status: number;
  type: string;
}

export interface FinishedMatchItem {
  away: string;
  awayHalf: string;
  awayScore: string;
  gameId: string;
  gameName: string;
  home: string;
  homeHalf: string;
  homeScore: string;
  leagueId: number;
  leagueName: string;
  matchId: number;
  startTime: string;
}

export interface getCompletedResultsParams {
  gameId: number;
  startTime: string;
  endTime: string;
  leagueId: number;
}
