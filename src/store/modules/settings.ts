import { SettingsState } from '@/store/types';
import { ActionContext } from 'vuex';
import { SET_OPTION_TYPE_KEY } from '@/common';

const state = {
  betRuleType: Number(localStorage.getItem(SET_OPTION_TYPE_KEY) || 0)
} as SettingsState;

const mutations = {
  setBetRuleType(state: SettingsState, payload: number) {
    state.betRuleType = payload;
  }
};

const actions = {
  // 设置投注规则
  SET_BET_RULE_TYPE({ commit }: ActionContext<any, any>, payload: number) {
    commit('setBetRuleType', payload);
    localStorage.setItem(SET_OPTION_TYPE_KEY, payload.toString());
  }
};

export default {
  state,
  mutations,
  actions
};
