/**
 * 新平台用户数据类型
 */
import { GeetestType } from '@/components';
import { Currency } from '@/hooks';

export interface GameParms {
  kindCode: string;
  gameType: string;
}

export interface UserLoginForm extends GeetestType {
  username: string;
  password: string;
}

export interface LoginAccountInfo {
  accessToken: string;
  refreshToken: string;
  tokenExpireIn: number;
  useInfo: UserInfo;
}

/**
 * 用户信息
 */
export interface UserInfo {
  account: string;
  nickname: string;
  language: string;
  currency: Currency;
  sex: number;
}

export interface GameBalance {
  balance: number;
  errorMsg: string;
  platformCode: string;
  platformName: string;
}
export interface PlatformBalance {
  balance: number;
  freeze: number;
  gameBalance: GameBalance[];
  gameTotal: number;
}

export interface BetOrderData {
  otherData: {
    betAmount: number;
    betCount: number;
    validAmount: number;
    winAmount: number;
  };
  page: {
    /**
     * 数据
     */
    records: BetOrder[];
    /**
     * 每页数量
     */
    size: number;
    /**
     * 总页数
     */
    pages: number;
    /**
     * 总条数
     */
    total: number;
    /**
     * 当前页
     */
    current: number;
  };
}

export interface BetOrder {
  id: number;
  // 注单号
  username: string;
  // 会员账号
  leagueId: number;
  // 联赛 ID
  leagueName: string;
  // 联赛名称
  homeId: number;
  // 主队ID
  awayId: number;
  // 客队ID
  matchTime: number;
  // 赛事开赛时间
  sportType: number;
  // 运动项目ID
  betType: number;
  // 投注类型ID
  odds: string;
  // 注单赔率
  stake: number;
  // 下注金额
  transactionTime: number;
  // 交易时间
  ticketStatus: number;
  // 注单状态
  ticketStatusStr: string;
  winLostAmount: string;
  // 输赢
  afterAmount: string;
  // 下注后，会员账户额度
  currency: number;
  // 币别 ID
  winLostTime: number;
  // 注单结算日期
  oddsType: 1 | 2 | 3 | 4 | 5;
  // 盘口类型
  isLucky: string;
  // 是否为 Lucky
  betTeam: string;
  // 客户下注的选项
  settlementTime: string;
  // 注单结算日期与时间
}

export interface BetOrdersPrams {
  /**
   * 当前页数
   */
  current: number;
  /**
   * 结算状态 0未结束 1已结算 -1已取消
   */
  size?: number;
  /**
   * 每页多少条
   */
  gameKind: string;
  /**
   * 结算状态 0未结束 1已结算 -1已取消
   */
  status?: 0 | 1 | -1 | null;
  /**
   * 开始时间
   */
  beginTime?: any;
  /**
   * 开始时间
   */
  endTime?: any;
  /**
   * 币种标识 不传则查询全部
   */
  currency?: Currency;
}

export interface GameBalanceItem {
  balance: number;
  channelId: number;
  firstCode: string;
  game_code: string;
  liveCode: string;
  platCode: string;
  platformName: string;
  walletCode: string;
}
export interface BalanceData {
  balance: number;
  game: number;
  frozenAmount: number;
  gameBalance: GameBalanceItem[];
}
