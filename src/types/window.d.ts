import DayjsType from 'dayjs';
declare global {
  interface Window {
    dayjs: typeof DayjsType;
    openLoginDialog: any;
    tenant: any;
    $sportDomainApi: any;
    WSclient: any;
    WSclient2: any;
    http: any;
    liveDateTimer: any;
    __POWERED_BY_QIANKUN__: any;
    toRechargePath: any;
    __INJECTED_PUBLIC_PATH_BY_QIANKUN__: any;
    initGeetest: any;
    Hls: any;
    DEBUG: boolean;
    $data: any;
    tenant?: {
      version: string;
      tenantCode: string;
    };
  }
}
