import { RouteEnum } from './router.enum';
export * from './router.enum';
import { Fn } from '#/global';
import { createRouter, createWebHistory, RouteLocationNormalized, Router } from 'vue-router';
import Home from '../views/home/index.vue';

const isDev = process.env.NODE_ENV === 'development';

function registerRouter(router: any) {
  router.beforeEach(async (_to: any, _from: RouteLocationNormalized, next: Fn) => {
    if (window.__POWERED_BY_QIANKUN__) {
      next();
      return;
    }
    next();
  });
}

export function initRouter(BASE_URL = '/'): Router {
  const router = createRouter({
    history: createWebHistory(window.__POWERED_BY_QIANKUN__ ? BASE_URL : '/'),
    routes,
    strict: true,
    scrollBehavior: () => ({ left: 0, top: 0 })
  });
  registerRouter(router);
  return router;
}

const devRoutes = () => {
  const routes = [
    {
      path: RouteEnum.LOGIN,
      name: RouteEnum.LOGIN,
      component: () => import('@/components/auth/login_v2.vue'),
      meta: {
        title: 'Login',
        keepAlive: false
      }
    },
    {
      path: RouteEnum.DEMO,
      name: RouteEnum.DEMO,
      component: () => import('../views/demo.vue'),
      meta: {
        title: 'Demo',
        keepAlive: false
      }
    }
  ];
  return isDev ? routes : [];
};

const routes = [
  ...devRoutes(),
  {
    path: RouteEnum.ROOT,
    name: 'sports',
    component: Home,
    meta: {
      title: 'Sports',
      keepAlive: true
    }
  },
  {
    path: RouteEnum.SPORT_DETAIL,
    name: RouteEnum.SPORT_DETAIL,
    component: () => import('../views/detail/index.vue'),
    meta: {
      title: 'Sports Detail',
      keepAlive: true
    }
  },
  {
    path: RouteEnum.BET_HISTORY,
    name: RouteEnum.BET_HISTORY,
    component: () => import('../views/bet-history/index.vue'),
    meta: {
      title: 'Bet History',
      keepAlive: false
    }
  },
  {
    path: RouteEnum.BET_HISTORY + '-m',
    name: RouteEnum.BET_HISTORY + '-m',
    component: () => import('../views/bet-history/history.vue'),
    meta: {
      title: 'Bet History',
      keepAlive: false
    }
  },
  {
    path: RouteEnum.NOT_FOUND,
    name: RouteEnum.NOT_FOUND,
    component: () => import('../views/error/404.vue'),
    meta: {
      title: '404 NOT FOUND',
      keepAlive: false
    }
  },
  {
    path: RouteEnum.NOTICE,
    name: 'Notice',
    component: () => import('../views/bet-history/detail.vue'),
    meta: {
      title: 'Notice',
      keepAlive: false
    }
  }
  // {
  //   path: '/:catchAll(.*)',
  //   redirect: RouteEnum.NOT_FOUND
  // }
];
