export function getBoundingClientRect(element: Element): DOMRect | number {
  if (!element || !element.getBoundingClientRect) {
    return 0;
  }
  return element.getBoundingClientRect();
}

const docEle = document.documentElement;
const VH_100 = '--100vh';
export function setPercentHeight() {
  const innerH = (window.innerHeight as any) + 'px';
  docEle.style.setProperty(VH_100, innerH);
}

/**
 * set page Title
 * @param {*} title  :page Title
 */
function setDocumentTitle(title: string) {
  document.title = title;
  const ua = navigator.userAgent;
  const regex = /\bMicroMessenger\/([\d.]+)/;
  // 兼容
  if (regex.test(ua) && /ip(hone|od|ad)/i.test(ua)) {
    const i = document.createElement('iframe');
    i.src = '/favicon.ico';
    i.style.display = 'none';
    i.onload = function () {
      setTimeout(function () {
        i.remove();
      }, 9);
    };
    document.body.appendChild(i);
  }
}

export function setTitle(title: string, appTitle?: string) {
  if (title) {
    const _title = title ? ` ${title} - ${appTitle} ` : `${appTitle}`;
    setDocumentTitle(_title);
  }
}

// 去掉斜杠获取URL
export function removeSlashUrl(url: string) {
  if (!url) {
    return '';
  }
  const str = new URL(url);
  return str.origin;
}
