import { defineComponent, onMounted } from 'vue';
import { useSportTypes, DateTypeKeys, markMatchsCount } from '@/hooks';
import { SportIcon } from '@/components';
import { PartialLoading, WeekCalendar } from 'kg-mobile/compoents';
import { DataTypeEnum } from '@/common';
import { useI18n } from '@/hooks';

import './handicap-filter.style.less';
// import { any } from 'expect';

export default defineComponent({
  name: 'HandicapFilter',
  setup() {
    const {
      store,
      initTab,
      dataTypeComp,
      onSportTabchange,
      onDateTabchange,
      activePeriodId,
      activeSportType,
      onWeekDayChange
    } = useSportTypes();
    const { t } = useI18n();

    onMounted(async () => {
      await initTab();
    });

    return () =>
      store.loading ? (
        <PartialLoading class="sports-type" />
      ) : (
        <div class="handicap-filter">
          <div class="match-type">
            <ul class="tabs">
              {dataTypeComp.value.map(item => (
                <li
                  class={{ 'tabs-item': true, active: item.value === activePeriodId.value }}
                  onClick={() => onDateTabchange(item.value)}
                  key={item.code}
                >
                  <span>{t(item.i18n)}</span>
                  <span>{item.count || 0}</span>
                </li>
              ))}
              <li
                class={{ 'tabs-item': true, active: -1 === activePeriodId.value }}
                onClick={() => onDateTabchange(-1)}
              >
                {/* 关注 */}
                <span>{t('lang.sport_handicap_focus')}</span>
                <span>{markMatchsCount.value}</span>
              </li>
            </ul>
          </div>
          <div class="sports-type-container">
            <ul class="sports-type">
              {store.sportTypes.map(item => {
                const count = item[DateTypeKeys[activePeriodId.value]];
                return count > 0 ? (
                  <li
                    key={item.sportType}
                    class={{ 'sports-type-item': true, active: activeSportType.value === item.sportType }}
                    onClick={() => onSportTabchange(item)}
                  >
                    <span class="sports-type-item-counter">{item[DateTypeKeys[activePeriodId.value]]}</span>
                    <div class="sport-icon" title={item.sportTypeName}>
                      <SportIcon size="26" class="ball" sportId={item.sportType} />
                    </div>
                  </li>
                ) : null;
              })}
            </ul>
          </div>
          {activePeriodId.value === DataTypeEnum.Early ? <WeekCalendar onChange={day => onWeekDayChange(day)} /> : null}
        </div>
      );
  }
});
