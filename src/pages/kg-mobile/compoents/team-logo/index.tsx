import { defineComponent } from 'vue';
import style from './teamlogo.module.less';
import { Image } from 'vant';
import errorImg from '@/assets/defalut_team_logo_green.png';
import { getCNDlogoUrl } from '@/common';

export default defineComponent({
  name: 'MobileTeamLogo',
  props: {
    sportId: {
      default: 1,
      type: [Number, String]
    },
    teamId: {
      type: [Number, String]
    },
    size: {
      type: String
    }
  },
  setup(props) {
    return () => {
      // const imgUrl_back = `http://ipis-cdn.speedy4site.com/TeamImage/${props.teamId || ''}.png`;
      const imgUrl = props.teamId ? `${getCNDlogoUrl()}/teamLogo/im/${props.sportId}/${props.teamId || ''}.png` : '#';
      return (
        <Image lazyLoad src={imgUrl} class={[style.teamLogo, props.size]}>
          {{
            error: () => <img src={errorImg} />,
            loading: () => <img src={errorImg} />
          }}
        </Image>
      );
    };
  }
});

// https://kgsports-apk.galalive.vip/teamLogo/im/1/39670.png
