/**
 * 顶部滚动渐变变化
 */
import { Ref, ref, onBeforeUnmount } from 'vue';
import { debounce } from 'lodash-es';

export function useScrollHeader() {
  const scrollFn: Ref<any> = ref();
  const isRelative: Ref<boolean> = ref(true);

  /**
   * 顶部滚动监听变化
   * 参数 target:目标元素
   * 参数 anchor:锚点元素
   * 参数 navHeight:是否滚动至
   * fn: 到达目标执行的事件
   */
  const headerFadeToggle = (
    { target, anchor, navHeight }: { target: string; anchor: string; navHeight: boolean },
    addFn?: () => void,
    removeFn?: () => void,
    scrollEle: HTMLElement = document.body
  ) => {
    const targetElement: HTMLElement | null = document.querySelector(target);
    const navBarElemet: HTMLElement | null = document.querySelector('#sport-header');
    !!targetElement && (targetElement.style.opacity = '0');
    scrollEle.scrollTo(0, 0); // 有些浏览器很骚，返回的时候不会回到顶部 MD
    document.body.scrollTo(0, 0); // 有些浏览器很骚，返回的时候不会回到顶部 MD
    scrollFn.value = () => {
      const anchorElement = document.querySelector(anchor) as HTMLElement;
      const anchorTop: number = !!anchorElement ? anchorElement.offsetTop : 0; // 锚点高度
      const scrollTop = scrollEle.scrollTop; // 窗口滚动高度
      const headerHeight = navHeight ? navBarElemet?.offsetHeight || 0 : 0;
      const percent = Math.floor((scrollTop / anchorTop) * 100) / 100; // 渐变百分比
      isRelative.value = !!(scrollTop > 50);
      if (scrollTop > 50 && scrollTop >= anchorTop - headerHeight) {
        !!addFn && addFn();
        !!targetElement && (targetElement.style.opacity = '1');
        return;
      } else {
        !!removeFn && removeFn();
      }
      !!targetElement && (targetElement.style.opacity = `${percent}`);
    };
    scrollFn.value = debounce(scrollFn.value, 30);
    const timer = setTimeout(() => {
      document.addEventListener('scroll', scrollFn.value, true);
      clearTimeout(timer);
    }, 500);
  };

  onBeforeUnmount(() => {
    document.removeEventListener('scroll', scrollFn.value, true);
  });

  return {
    headerFadeToggle,
    isRelative
  };
}

// 使用 fn()可选
// nextTick(() => {
//   headerFadeToggle('#header', '#anchor-ele', fn());
// });
