/**
 * 检查登录状态
 */
import { computed } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import { RouteEnum } from 'kg-mobile/router/router.enum';
import { Toast } from 'vant';
import { isMobile, SPORT_H5_URL_KEY, SPORT_WEB_URL_KEY } from '@/common';
import { useI18n } from '@/hooks';

const isDevTool = process.env.NODE_ENV === 'development';

// 返回web的url
const webUrl = localStorage.getItem(SPORT_WEB_URL_KEY) || '';

// 返回web的url
const H5Url = localStorage.getItem(SPORT_H5_URL_KEY) || '';

export function useAuth() {
  const store = useStore();
  const router = useRouter();
  const { t } = useI18n();

  const account = computed(() => {
    return store.getters.account;
  });

  //是否登录
  const isLogin = computed(() => store.getters.isLogin);

  // 登录
  const toLogin = () => {
    if (isDevTool) {
      router.push(RouteEnum.LOGIN);
      return;
    }
    isMobile ? toMobileRegedit() : toWebRegedit();
  };

  // 注册
  const toMobileRegedit = () => {
    window.open(H5Url + '/m/entry/login');
  };

  // 注册
  const toWebRegedit = () => {
    window.open(webUrl + '/entry/login');
  };

  // 检查登录, 登录信息组件处理
  const checkIsLogin = (): boolean => {
    if (!isLogin.value) {
      // 弹出Toast
      isMobile && Toast(t('lang.sport_common_loginTips'));
      console.warn('WARN: 请先登录!');
      return false;
    }
    return true;
  };

  // 登出
  const signOut = () => store.dispatch('SIGN_OUT');

  return {
    signOut,
    toMobileRegedit,
    toWebRegedit,
    toLogin,
    isLogin,
    account,
    checkIsLogin
  };
}
