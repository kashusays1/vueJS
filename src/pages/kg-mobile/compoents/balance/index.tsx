import { defineComponent, computed } from 'vue';
import { currency, CurrencyIcons } from '@/common';
import { SvgIcon } from '@/components';
import { useBlance } from '@/hooks';
import './balance.style.less';
import { useStore } from 'vuex';

export default defineComponent({
  name: 'HeaderBalance',
  emits: ['to-recharge'],
  setup(_props, { emit }) {
    const store = useStore();
    const currencySymbol = computed(() => {
      const info = store.state.user;
      return info.currency;
    });

    const { sportBalance, isFreshing, onHandleFresh } = useBlance({
      platformCode: 'im'
    });

    return () => (
      <div class="balance-info">
        <div class="icon-money" style={{ backgroundImage: `url(${CurrencyIcons[currencySymbol.value]})` }}></div>
        <span class="text-money" onClick={() => onHandleFresh()}>
          {currency(sportBalance.value)} {currencySymbol.value}
        </span>
        <div class="icon-recharge" onClick={() => emit('to-recharge')}>
          <div class="loading-icon">
            <SvgIcon name="refresh" class={{ 'is-load': isFreshing.value }} />
          </div>
        </div>
      </div>
    );
  }
});
