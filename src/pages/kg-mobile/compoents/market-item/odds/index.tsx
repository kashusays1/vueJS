import { defineComponent, PropType } from 'vue';
import { SelType } from '@/services/kg-api';
import { OddsPrice } from 'kg-mobile/compoents';
import { HomeMarketOdds, _keyNameByBettype } from '@/common';
import { hasTicketId } from '@/hooks';
import './odds.less';

export default defineComponent({
  name: 'MatchOdds',
  props: {
    oddset: {
      type: Array as PropType<HomeMarketOdds[]>,
      default: () => ['', '', '', '', '', '']
    },
    matchId: {
      type: Number,
      required: true
    },
    sportId: {
      type: Number
    }
  },
  emits: ['addBetTicket'],
  setup(props, { emit }) {
    const LockOddsRender = () => (
      <div class="lock-odds">
        <svg-icon name="lock" />
      </div>
    );

    const NoneOddsRender = () => <div class="none-odds">-</div>;

    return () => (
      <div class={'odds-swiper'}>
        <div class="odds-swiper-container">
          {props.oddset.map((HT_FT_Item, index) => (
            <div class="odds" key={HT_FT_Item.OddsId || index}>
              {HT_FT_Item.sels.map((sel: SelType, i) => (
                <div class="col" key={i}>
                  {!sel ? (
                    <div class="odds-item">
                      <NoneOddsRender />
                    </div>
                  ) : HT_FT_Item.locked ? (
                    <div class="odds-item">
                      <LockOddsRender />
                    </div>
                  ) : (
                    <div
                      class={[
                        'odds-item',
                        hasTicketId({ market_id: sel.WagerSelectionId, key: sel.Key, matchId: props.matchId })
                          ? 'selected'
                          : null
                      ]}
                      onClick={() =>
                        emit('addBetTicket', {
                          bettype: HT_FT_Item.Bettype,
                          betTypeName: HT_FT_Item.BettypeName,
                          matchId: props.matchId,
                          market_id: HT_FT_Item.OddsId,
                          sport_type: props.sportId,
                          odds_type: sel.OddsType,
                          wagerSelectionId: sel.WagerSelectionId,
                          price: sel.Price,
                          periodId: HT_FT_Item.PeriodId,
                          point: sel.Point,
                          key: sel.Key,
                          keyName: sel.KeyName,
                          outrightTeamId: 0
                        })
                      }
                    >
                      <span class="label">{_keyNameByBettype(HT_FT_Item.Bettype, sel)}</span>
                      <OddsPrice class="value" value={sel.Price} />
                    </div>
                  )}
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>
    );
  }
});
