import { task } from 'gulp';

const { bootstrapI18n } = require('./scripts/i18n');

/**
 * 拉取i8n资源
 */
task('cms-i18n', async done => {
  try {
    await bootstrapI18n();
  } catch (e) {
    console.error(e);
  } finally {
    done();
  }
});
