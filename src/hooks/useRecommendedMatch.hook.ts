/**
 * 旧版赛事推荐hook
 */

import { computed, reactive } from 'vue';
import { getRecommendedMatchList } from '@/services/sb-api';
import { isEmpty } from 'lodash-es';
import { useStore } from 'vuex';
import { TechId } from '@/common/enums/sportsTechEnum';

// (足球)比赛状态 0:比赛异常 1:未开赛 2:上半场 3:中场 4:下半场 5:加时赛 6:加时赛(弃用) 7:点球决战 8:完场 9:推迟 10:中断 11:腰斩 12:取消 13:待定
const GAME_PART_SOCCERBALL = {
  '2': '上半场',
  '3': '半场',
  '4': '下半场',
  '5': '加时赛',
  '7': '点球决战'
};

// (篮球)比赛状态 0:比赛异常 1:未开赛 2:第一节 3:第一节完 4:第二节 5:第二节完 6:第三节 7:第三节完 8:第四节 9:加时 10:完场 11:中断 12:取消 13:延期 14:腰斩 15:待定
const GAME_PART_BASKETBALL = {
  '0': '比赛异常',
  '2': '第一节',
  '3': '第一节完',
  '4': '第二节',
  '5': '第二节完',
  '6': '第三节',
  '7': '第三节完',
  '8': '第四节',
  '9': '加时'
};

// 转换游戏时间
const formatGameTime = (d: any) => {
  let result = '';
  const stage = '';
  const { gameTime, gamePart, gid } = d;
  if (gid == 1) {
    result = gameTime;
    // 针对足球半场的特殊逻辑，因为在半场休息时候，返回的时间为空
    if (gamePart === '3') {
      result = '45:00';
    } else if (!isEmpty(result)) {
      if (!['2', '4', '5', '7'].includes(gamePart)) {
        result = '';
      }
      return result;
    }
  } else if (gid == 2) {
    result = gameTime;
  } else {
    result = gameTime;
  }
  result = (result || '').trim() || '进行中';
  return `${stage || ''} ${result}`;
};

// 格式化时间段
const formatStage = (d: any) => {
  const { gid, gamePart } = d;
  let stage = '';
  switch (gid) {
    case 1:
      stage = GAME_PART_SOCCERBALL[gamePart];
      break;
    case 2:
      stage = GAME_PART_BASKETBALL[gamePart];
      break;
    default:
      break;
  }
  return stage;
};

export function useRecommendedMatch() {
  const vuexStore = useStore();

  const store = reactive({
    recMatchList: []
  });

  const getRecommendedData = async () => {
    const [isSuccess, res] = await getRecommendedMatchList('shaba');
    if (isSuccess) {
      store.recMatchList = res;
    }
  };

  const transformData = computed(() => {
    return !isEmpty(store.recMatchList)
      ? store.recMatchList.map((d: any) => {
          d.formattedGameTime = formatGameTime(d);
          d.formateStage = formatStage(d);
          const tech = vuexStore.getters.getTechById(d.mid);
          if (!tech) return d;
          const [homeCorners, awayCorners] = tech.get(TechId.CORNER) || [d.homeCorners, d.awayCorners];
          const [homeHalfScore, awayHalfScore] = tech.get(TechId.HALF_SCORE) || [d.homeHalfScore, d.awayHalfScore];
          return {
            ...d,
            homeCorners,
            awayCorners,
            homeHalfScore,
            awayHalfScore
          };
        })
      : [];
  });

  return {
    transformData,
    reStore: store,
    getRecommendedData
  };
}

export { formatGameTime, formatStage };
