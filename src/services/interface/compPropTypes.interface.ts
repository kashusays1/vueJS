/**
 * 组件数据类型定义, 集中在此文件
 */
// import { CombosItem } from '@/services';
import { Ref } from 'vue';
import { MatchMoreInfo } from '../im-api';

// 体育分类
export interface SportSortItem {
  /**
   * 体育分类
   */
  sportId: number;
  /**
   * 体育名字
   */
  sportName: string;
  /**
   * 早盘赛事数量
   */
  earlyCount?: number;
  /**
   * 今日赛事数量
   */
  todayCount?: number;
  /**
   * 滚球赛事数量
   */
  liveCount?: number;
  /**
   * 串关赛事数量
   */
  parlayCount?: number;
  /**
   * 关注的数量
   */
  followdCount: number;
  /**
   * 关注的赛事id
   */
  followdEventIds: number[];
}

export interface LeagueItem {
  // 联赛名称
  leagueName: string;
  // 联赛id
  leagueId: string;
  // 首字母
  letter?: string;
}

export interface LeagueFilterPops {
  // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球
  periodId: number;
  // 体育项目ID
  sportId: number;
  // 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
  oddsType: number;
}

// 单条预投注类型
export interface PreBetItem {
  sport_type: number;
  market_id: number;
  is_decimal_type: boolean;
  point: string;
  key: string;
  bet_type: number;
  odds_type: number;
  price: number;
  status: string;
  max_bet: number;
  min_bet: number;
  live_home_score: number;
  live_away_score: number;
  [name: string]: any;
}

export interface SingleTicketPrams {
  // 体育项目ID;
  sport_type: number;
  // 赔率ID;
  market_id: number;
  // 从queryEventsAndMarkets接口取得,并可参考沙巴附件-投注类型与选项
  key: string;
  // 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
  odds_type: 1 | 3 | 2 | 4 | 5;
  // 赔率名称
  key_name?: string;
}

export interface HandicapItem {
  type: any;
  value: number;
}

// 格式化后的 单条预投注类型
// export interface PreBetItem {
//   sportType: number;
//   marketId: number;
//   isDecimalType: boolean;
//   point: string;
//   key: string;
//   betType: number;
//   oddsType: number;
//   price: number;
//   status: string;
//   maxBet: number;
//   minBet: number;
//   liveHomeScore: number;
//   liveAwayScore: number;
// }

export interface ParlayTicketPrams {
  bet_matches: { combo_type: string; stake: number }[];
  bet_combos: {
    key: string;
    market_id: string;
    point: number;
    price: number;
    sport_type: number;
  }[];
  parlay_odds_option: number;
}

export interface SortForm {
  betType: number;
  leagueIds: string;
  periodId: number;
  sportId: number;
  switchTypeKey: string;
  isCompleted: boolean;
  selectDate: number;
  eventIds: string;
  isParlay?: boolean;
}

export interface OddInfoComp {
  Price: number;
  Combo: number;
  MaxBet: number;
  MinBet: number;
  Point: number;
  Point2: number;
  Bettype: number;
  MarketStatus: number;
  BettypeName: string;
  OddStatus: number | Ref<number>;
  OddStatusLabel: string | Ref<string>;
  IsLive: number;
  [name: string]: any;
}

// 选中得票条目
export interface TicketItemComp {
  betPram: SingleTicketPrams;
  matchInfo: MatchMoreInfo;
  oddInfo: OddInfoComp;
}

export interface PlacePreBetData {
  combos: {
    bet_count: number;
    combo_type: string;
    max_bet: number;
    min_bet: number;
  }[];
  price_info: {
    bet_type: number;
    current_price: number;
    key: 'h';
    live_away_score: number;
    live_home_score: number;
    market_id: number;
    odds_type: number;
    point: number;
    sport_type: number;
    state_code: number;
    state_message: string;
    status: string;
  }[];
}
