/**
 * 获取赛事数据
 * @description: 获取足球事件
 */

import {
  lineupApi,
  liveTextApi,
  analysisApi,
  informationApi,
  bLiveTextApi,
  bAnalysisApi,
  bLineupApi,
  bIntelligenceApi,
  bInformationApi
} from '@/services';
import { useIntervalFn } from '@vueuse/core';
import { ref, onMounted, computed, ComputedRef, Ref } from 'vue';
import { forEach, isEmpty, split } from 'lodash-es';
export interface FootballEvents {
  real: any;
  lineUp: any;
  analysis: any;
  intelligence: any;
}

interface useEventsDataFnReturn {
  soccerBallEventsData: ComputedRef<FootballEvents>;
  bBallAnalysisData: Ref<any>;
  isEmptyFootballEventData: ComputedRef<boolean>;
}

export function useEventsData({
  matchId,
  sportId
}: {
  matchId: number | string;
  sportId: number;
}): useEventsDataFnReturn {
  const soccerBallEventsData: ComputedRef<FootballEvents> = computed(() => {
    return {
      real: realTimeList.value,
      lineUp: lineUpData.value,
      analysis: analysisList.value,
      intelligence: intelligenceList.value
    };
  });

  // 足球实时
  const realTimeList = ref([]);
  // 足球阵容
  const lineUpData = ref({});
  // 足球分析
  const analysisList = ref({});
  // 足球情报
  const intelligenceList = ref({});
  // 足球赛事数据是否为空
  const isEmptyFootballEventData = computed(() => {
    return (
      isEmpty(realTimeList.value) &&
      isEmpty(lineUpData.value) &&
      isEmpty(analysisList.value) &&
      isEmpty(intelligenceList.value)
    );
  });
  /**
   *格式化阵容
   *
   * @param {{
   *     awayArray: string;
   *     awayBackup: string;
   *     awayLineup: string;
   *     homeArray: string;
   *     homeBackup: string;
   *     homeLineup: string;
   *     logos: string;
   *   }} {
   *     awayArray,
   *     awayBackup,
   *     awayLineup,
   *     homeArray,
   *     homeBackup,
   *     homeLineup,
   *     logos
   *   }
   * @return {*}
   */
  const _formatLineup = ({
    awayArray,
    awayBackup,
    awayLineup,
    homeArray,
    homeBackup,
    homeLineup,
    logos
  }: {
    awayArray: string;
    awayBackup: string;
    awayLineup: string;
    homeArray: string;
    homeBackup: string;
    homeLineup: string;
    logos: string;
  }) => {
    const logoStrArr = split(logos, ';');
    logoStrArr.pop();
    const logoMap: { [key: string]: string } = {};
    forEach(logoStrArr, l => {
      const [id, logo] = split(l, ',');
      logoMap[id] = `http://cdn.sportnanoapi.com/soccerBall/player/${logo}`;
    });
    const _formatBackup = (backup: string) => {
      const temp = split(backup, ';');
      temp.pop();
      return temp.map(t => {
        const [id, name, , no] = split(t, ',');
        return { id, name, no, logo: logoMap[id] };
      });
    };
    const homeBackupData = _formatBackup(homeBackup);
    const awayBackupData = _formatBackup(awayBackup);
    const homeLineupData = _formatBackup(homeLineup);
    const awayLineupData = _formatBackup(awayLineup);
    const backup = {
      home: homeBackupData,
      away: awayBackupData
    };
    const lineup = {
      home: homeLineupData,
      away: awayLineupData
    };
    return {
      awayArray,
      homeArray,
      backup,
      lineup
    };
  };

  const _formatAnalysis = (data: { injury: { home: { logo: string }[]; away: { logo: string }[] } }) => {
    // console.log(data);
    // const {
    //   injury: { home, away }
    // } = data;
    const injury = data.injury;
    const home = injury?.home;
    const away = injury?.away;
    home &&
      home.forEach((h: { logo: string }) => {
        h.logo = `http://cdn.sportnanoapi.com/soccerBall/player/${h.logo}`;
      });
    away &&
      away.forEach((a: { logo: string }) => {
        a.logo = `http://cdn.sportnanoapi.com/soccerBall/player/${a.logo}`;
      });
    return data;
  };

  const methods = {
    //实时数据
    GetRealTimelist: async () => {
      const api = sportId === 1 ? liveTextApi : bLiveTextApi;
      const [isSuccess, data] = await api({ source: 'sb', matchId });
      if (isSuccess) {
        realTimeList.value = data;
      }
    },
    //阵容数据接口  阵容暂未找到 可用数据
    GetIntellGence: async () => {
      const api = sportId === 1 ? lineupApi : bLineupApi;
      const [isSuccess, data] = await api({ source: 'sb', matchId });
      if (isSuccess) lineUpData.value = _formatLineup(data);
    },
    //分析数据接口
    GetAnalylist: async () => {
      const api = sportId === 1 ? analysisApi : bIntelligenceApi;
      const [isSuccess, data] = await api({ source: 'sb', matchId });
      if (isSuccess) analysisList.value = _formatAnalysis(data);
    },
    //情报数据接口
    GetInforlist: async () => {
      const api = sportId === 1 ? informationApi : bInformationApi;
      const [isSuccess, data] = await api({ source: 'sb', matchId });
      if (isSuccess) intelligenceList.value = data;
    }
  };
  useIntervalFn(methods.GetRealTimelist, 6000);

  /**
   * 获取足球时间数据
   */
  const getEventsData = () => {
    methods.GetRealTimelist();
    methods.GetIntellGence();
    methods.GetAnalylist();
    methods.GetInforlist();
  };
  const bBallAnalysisData = ref({});

  onMounted(async () => {
    getEventsData();
    if (sportId === 2) {
      const [res, data] = await bAnalysisApi({ source: 'sb', matchId });
      if (res) {
        bBallAnalysisData.value = data || {};
      }
    }
  });

  return {
    soccerBallEventsData,
    bBallAnalysisData,
    isEmptyFootballEventData
  };
}
