import { computed, defineComponent, nextTick, onMounted } from 'vue';
import './style/cart.style.less';
import { Popup } from 'vant';
import BettingOrders from './betting-orders';
import BettingResult from './result';
import { useAuth, useBet } from '@/hooks';
import { useRoute } from 'vue-router';
import { RouteEnum } from 'jbb-mobile/router/router.enum';
import { isEmpty } from 'lodash-es';

export default defineComponent({
  name: 'ShoppingCart',
  setup() {
    const { betTickets, ticketSize, clearAllTickets, openTickets, handleClosePopup, state } = useBet();
    const { isLogin } = useAuth();
    const route = useRoute();
    // 显示购物车
    const showCart = computed(() => {
      const whiteRoutes = [RouteEnum.ROOT, RouteEnum.HOME, RouteEnum.SPORT_DETAIL] as string[];
      const showRoute = whiteRoutes.includes(route.path);
      return !isEmpty(betTickets.value) && showRoute && isLogin.value;
    });
    onMounted(() => {
      nextTick(() => {
        !isLogin.value && clearAllTickets();
      });
    });

    const ShopCartRender = () =>
      showCart.value ? (
        <div class="shopping-cart" onClick={openTickets}>
          <span class="order-count">
            <i>{ticketSize.value}</i>
          </span>
        </div>
      ) : null;

    return () => [
      <ShopCartRender />,
      <Popup
        teleport="#microApp"
        safeAreaInsetBottom
        v-model:show={state.isShowPopup}
        onClick-overlay={handleClosePopup}
        round
        position="bottom"
        style={{ height: '80%', width: '100%', border: '1px solid blue' }}
      >
        {state.betted ? <BettingResult /> : <BettingOrders onClearAllTickets={clearAllTickets} />}
      </Popup>
    ];
  }
});
