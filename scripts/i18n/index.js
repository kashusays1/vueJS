const args = require('minimist')(process.argv.slice(2));
const { creatI18nFile } = require('./buildI18n');

async function bootstrapI18n() {
  await creatI18nFile(args.app);
}

module.exports = {
  bootstrapI18n
};
