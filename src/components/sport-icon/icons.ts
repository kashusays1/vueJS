export const sportIcons = {
  1: {
    general: require('./icons/1.png'),
    gray: require('./icons/gray_1.png')
  },
  2: {
    general: require('./icons/2.png'),
    gray: require('./icons/gray_2.png')
  },
  3: {
    general: require('./icons/3.png'),
    gray: require('./icons/gray_3.png')
  },
  4: {
    general: require('./icons/4.png'),
    gray: require('./icons/gray_4.png')
  },
  5: {
    general: require('./icons/5.png'),
    gray: require('./icons/gray_5.png')
  },
  6: {
    general: require('./icons/6.png'),
    gray: require('./icons/gray_6.png')
  },
  7: {
    general: require('./icons/7.png'),
    gray: require('./icons/gray_7.png')
  },
  16: {
    general: require('./icons/16.png'),
    gray: require('./icons/gray_16.png')
  },
  18: {
    general: require('./icons/18.png'),
    gray: require('./icons/gray_18.png')
  },
  26: {
    general: require('./icons/26.png'),
    gray: require('./icons/gray_26.png')
  },
  50: {
    general: require('./icons/50.png'),
    gray: require('./icons/gray_50.png')
  },
  9999: {
    general: require('./icons/1.png'),
    gray: require('./icons/gray_1.png')
  }
};
