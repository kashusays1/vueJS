import { defineComponent } from 'vue';
// import { HeaderBalance } from '..';
import './currency-sheet.style.less';
import { useCurrencySheet } from './useCurrencySheet.hook';
import { DATE_LIST } from '@/common/utils';
export default defineComponent({
  name: 'Header',
  props: ['type'],
  setup(props) {
    const { store } = useCurrencySheet();
    return () => (
      <div class="currency-sheet">
        <div class="currency-sheet-header">
          <div>取消</div>
          <div class="currency-sheet-header-title">{props.type === 'currency' ? '选择币种' : '筛选日期'}</div>
          <div>确认</div>
          {/* <HeaderBalance /> */}
        </div>
        <div class="currency-sheet-container">
          {props.type === 'currency' &&
            store.currencyList.map((item, index) => (
              <div class={{ 'currency-sheet-item': true, active: index === 1 }} key={item.code}>
                <div class="currency-sheet-item-icon">
                  <img src={item.icon} />
                </div>
                <div class="currency-sheet-item-name">
                  {item.name}({item.code})
                </div>
                <div class="currency-sheet-item-balance">
                  <input name="money" type="radio" />
                  {/* {item.blacne} */}
                </div>
              </div>
            ))}
          {props.type === 'time' &&
            DATE_LIST.map((item, index) => (
              <div class={{ 'currency-sheet-item': true, active: index === 1 }} key={item.title}>
                <div class="currency-sheet-item-name" style={{ textAlign: 'center' }}>
                  {item.title}
                  {/* ({item.code}) */}
                </div>
                <div class="currency-sheet-item-balance">
                  {/* <input name="money" type="radio" /> */}
                  {/* {item.blacne} */}
                </div>
              </div>
            ))}
        </div>
        {/* <div>
          <div class="to-wallet">进入钱包</div>
        </div> */}
      </div>
    );
  }
});
