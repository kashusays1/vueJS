/**
 * 玩法菜单
 */
export function useMenuHook(): MenuItem[] {
  return [];
}

export interface MenuItem {
  sportId: number;
  sportName: string;
  count: number;
}
