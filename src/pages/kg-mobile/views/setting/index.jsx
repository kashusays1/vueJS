import { defineComponent } from 'vue';
import Event from './setting.module.less';
import { CellGroup, Cell, Checkbox, Switch, Radio, RadioGroup } from 'vant';
import { MarketTypeList, BetPreferenceList } from '@/common/enums';
import { useSportOptins, useI18n } from '@/hooks';
import { languageList, useLocale } from '@/i18n';

export default defineComponent({
  name: 'JBBsportPersonal',
  setup() {
    const { optionsStore, changeSetiing } = useSportOptins();
    const { changeLocale } = useLocale();
    const { t } = useI18n();

    return () => (
      <div>
        <CellGroup>
          <Cell class={Event.titleCell} title={t('lang.sport_setting_markOptions')} />
          <div class={Event.wrap}>
            {MarketTypeList.map(item => (
              <div
                onClick={() => changeSetiing('marketType', item.value)}
                class={[Event.setItem, optionsStore.value.marketType === item.value ? Event.active : null]}
                key={item.code}
              >
                {t(item.label)}
              </div>
            ))}
          </div>
          <Cell class={Event.titleCell} title={t('lang.sport_setting_betOptions')} />
          <RadioGroup v-model={optionsStore.value.preference} direction="horizontal">
            {BetPreferenceList.map(item => (
              <Cell class={Event.specCell}>
                <Radio name={item.value}>{t(item.label)}</Radio>
              </Cell>
            ))}
          </RadioGroup>
          <Cell class={Event.titleCell} title={t('lang.sport_setting_soundsSwitch')}>
            <template right-icon class={[Event.displayBlock]}>
              <Switch size="22px" v-model={optionsStore.value.switchSound} />
            </template>
          </Cell>
          <Cell class={Event.specCell}>
            <Checkbox v-model={optionsStore.value.betSuccessSound} disabled={!optionsStore.value.switchSound}>
              {/* 投注成功音效 */}
              {t('lang.sport_setting_betSuccessSound')}
            </Checkbox>
          </Cell>
          <Cell class={Event.specCell}>
            <Checkbox v-model={optionsStore.value.confirmationSound} disabled={!optionsStore.value.switchSound}>
              {/* 投注确认中音效 */}
              {t('lang.sport_setting_betconfirmationSound')}
            </Checkbox>
          </Cell>
          <Cell class={Event.titleCell} title={t('lang.sport_common_switchLanguage')} />
          <RadioGroup onChange={lang => changeLocale(lang)} v-model={optionsStore.value.lang} direction="horizontal">
            {languageList.map(item => (
              <Cell class={Event.specCell}>
                <Radio name={item.seriNo}>{item.info}</Radio>
              </Cell>
            ))}
          </RadioGroup>
        </CellGroup>
      </div>
    );
  }
});
