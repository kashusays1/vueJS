import { defineComponent, PropType, reactive } from 'vue';
import './market-list.style.less';
import { Collapse, DataEmpty, LeagueFilter, MarketItem } from 'kg-mobile/compoents';
import { SportIcon, SvgIcon } from '@/components';
import { Popup } from 'vant';
import { isEmpty } from 'lodash-es';
import { StoreMarketData, useSportTypes, useI18n } from '@/hooks';
import { PeriodI18n } from '@/common';

// 全场/半场
const FT_HT_LABELS = ['lang.sport_earlyHandicap_halfTime', 'lang.sport_earlyHandicap_audience'];

export default defineComponent({
  name: 'MarketList',
  props: {
    marketData: {
      type: Array as PropType<StoreMarketData[]>,
      default: () => []
    }
  },
  emits: ['leagueChange'],
  setup(props, { emit }) {
    const { activeSportType, activePeriodId } = useSportTypes();
    const { t } = useI18n();

    const states = reactive({
      showLeagueFilter: false,
      avtiveTime: 0,
      isExpandAll: false
    });

    const filterToggle = () => {
      states.showLeagueFilter = !states.showLeagueFilter;
    };

    const switchFTorHT = (index: number) => {
      if (states.avtiveTime === index) return;
      states.avtiveTime = index;
    };

    const confirm = async (data: string[]) => {
      const gids = data.join(',');
      emit('leagueChange', gids);
    };

    const MarketListHeader = () => (
      <div class="market-list-header">
        <div
          class={['toggle-icon', states.isExpandAll ? 'rotate180deg' : null]}
          onClick={() => (states.isExpandAll = !states.isExpandAll)}
        ></div>
        <div class={'curret-ball'}>
          <SportIcon class={'curret-ball-icon'} sportId={activeSportType.value} />
          <span class={'curret-ball-name'}>{t(PeriodI18n[activePeriodId.value])}</span>
        </div>
        <div class={'market-types'}>
          {FT_HT_LABELS.map((text, index) => (
            <div
              key={index}
              onClick={() => switchFTorHT(index)}
              class={{ 'market-types-item': true, active: index === states.avtiveTime }}
            >
              {t(text)}
            </div>
          ))}
        </div>
        <div class="league-icon" onClick={() => filterToggle()}>
          <SvgIcon name="menu" />
        </div>
      </div>
    );

    return () => [
      isEmpty(props.marketData) ? (
        <DataEmpty title="暂无盘口" desc="数据没了" />
      ) : (
        <div class="market-list">
          <MarketListHeader />
          <Collapse expandAll={states.isExpandAll} class={{ 'is-toggle': states.avtiveTime === 1 }}>
            {props.marketData.map(item => (
              <MarketItem data={item} key={item.leagueId} />
            ))}
          </Collapse>
        </div>
      ),
      <Popup
        teleport="#microApp"
        v-model:show={states.showLeagueFilter}
        position="right"
        style={{ height: '100%', width: '100%' }}
      >
        {states.showLeagueFilter ? (
          <LeagueFilter sportId={1} onConfirm={confirm} onClose={() => filterToggle()} />
        ) : null}
      </Popup>
    ];
  }
});
