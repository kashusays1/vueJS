import { TimerEnum, useTimerHook } from '@/hooks';
import { reactive, computed } from 'vue';
import { isEmpty } from 'lodash-es';
import { convertSportsDetailList, SportIdEnum, isMobile, DataTypeEnum } from '@/common';
import { EventsAndMarketsPrams, queryEventsAndMarkets } from '@/services/im-api';
import { Laoding } from 'kg-mobile/compoents';

interface Store {
  raw: object;
  info: any;
  odds: any[];
  periodId: number;
  leagueId: string;
  leagueName: string;
  showMatchs: boolean;
  showSheet: boolean;
  group: number;
  groups: any[];
  /**
   * 页面空数据状态
   */
  errorPage: boolean;
}

const initData = {
  raw: {},
  info: {},
  odds: [],
  periodId: 1,
  leagueId: '',
  showMatchs: false,
  showSheet: false,
  groups: [],
  group: 0,
  leagueName: '',
  errorPage: false
};

const groupList = [
  {
    group: 1,
    name: '让球&大/小',
    i18n: 'lang.sport_common_HandicapOverUnder'
  },
  {
    group: 2,
    name: '波胆',
    i18n: 'lang.sport_common_correctScore'
  },
  {
    group: 3,
    name: '角球',
    i18n: 'lang.sport_common_cornerKick'
  },
  {
    group: 4,
    name: '进球',
    i18n: 'lang.sport_common_goal'
  },
  {
    group: 5,
    name: '半场',
    i18n: 'lang.sport_common_haftTime'
  },
  {
    group: 6,
    name: '特别投注',
    i18n: 'lang.sport_common_special'
  }
];
const store = reactive<Store>(initData);
export function useDetailHook() {
  const { addTimer } = useTimerHook();
  const getData = async (params: EventsAndMarketsPrams) => {
    isMobile && Laoding.show();
    const fn = async () => {
      const [res, data] = await queryEventsAndMarkets(params);
      isMobile && Laoding.hide();
      if (isEmpty(data)) {
        store.errorPage = true;
        return;
      }

      if (res) {
        const {
          info,
          odds,
          groups = [],
          leagueId,
          leagueName
        } = convertSportsDetailList(data, {
          sportId: params.sportId as SportIdEnum,
          periodId: params.periodId
        });
        store.raw = data;
        store.info = info;
        store.odds = odds;
        store.groups = [
          { group: 0, name: '所有盘口', i18n: 'lang.sport_common_all' },
          ...(groupList.filter(g => groups.includes(g.group)) || [])
        ];

        store.leagueId = leagueId;
        store.leagueName = leagueName;
      }
    };
    addTimer(TimerEnum.DETAIL, fn, params.periodId === DataTypeEnum.Live ? 6000 : 60000);
  };
  const showOdds = computed(() => {
    if (store.group === 0) return store.odds;
    return store.odds.filter(odd => (odd.group || []).includes(store.group));
  });
  const changeGroup = (group: number) => {
    store.group = group;
  };

  return { store, showOdds, getData, changeGroup };
}
