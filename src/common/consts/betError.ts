export const ParlayComboType = [
  {
    label: '2串1',
    code: 'Doubles',
    value: 0
  },
  {
    label: '3串1',
    code: 'Trebles',
    value: 1
  },
  {
    label: '3串4',
    code: 'Trixie',
    value: 2
  },
  {
    label: '幸运7',
    code: 'Lucky7',
    value: 3
  },
  {
    label: '4串1',
    code: 'Fold4',
    value: 4
  },
  {
    label: '洋基',
    code: 'Yankee',
    value: 5
  },
  {
    label: '幸运15',
    code: 'Lucky15',
    value: 6
  },
  {
    label: '5串1',
    code: 'Fold5',
    value: 7
  },
  {
    label: '超级美国佬',
    code: 'Canadian',
    value: 8
  },
  {
    label: '幸运 31',
    code: 'Lucky31',
    value: 9
  },
  {
    label: '6串1',
    code: 'Fold6',
    value: 10
  },
  {
    label: '亨氏',
    code: 'Heinz',
    value: 11
  },
  {
    label: '幸运 63',
    code: 'Lucky63',
    value: 12
  },
  {
    label: '7串1',
    code: 'Fold7',
    value: 13
  },
  {
    label: '超级亨氏',
    code: 'SuperHeinz',
    value: 14
  },
  {
    label: '幸运 127',
    code: 'Lucky127',
    value: 15
  },
  {
    label: '8串1',
    code: 'Fold8',
    value: 16
  },
  {
    label: '大亨',
    code: 'Goliath',
    value: 17
  },
  {
    label: '幸运255',
    code: 'Lucky255',
    value: 18
  },
  {
    label: '9串1',
    code: 'Fold9',
    value: 19
  },
  {
    label: '10串1',
    code: 'Fold10',
    value: 20
  },
  {
    label: '11串1',
    code: 'Fold11',
    value: 21
  }
];

export const PreBetError = {
  3: {
    // label: '找不到该会员资料',
    label: '盘口关闭',
    code: 'Customer not found',
    value: 3
  },
  6: {
    label: '球头已不存在',
    code: 'Point isUtil expired',
    value: 6
  },
  7: {
    label: '赛事 ID 错误或是赛事已关',
    code: 'Match closed or invalid Market ID',
    value: 7
  },
  12: {
    label: '赔率正在更新',
    code: 'Odds suspend',
    value: 12
  },
  14: {
    label: '余额不足',
    code: 'Credit problem',
    value: 14
  },
  15: {
    label: '超过单场投注限额',
    code: 'Over max per match',
    value: 15
  },
  24: {
    label: '盘口关闭',
    code: 'Close price',
    value: 24
  },
  25: {
    label: '盘口关闭',
    code: 'Market closed',
    value: 25
  },
  8: {
    label: '赔率变更',
    code: 'Odds changed',
    value: 8
  },
  603: {
    label: '登录过期，请重新登录',
    value: 603
  }
};

export const DoBetError = [
  undefined,
  {
    label: '执行失败',
    code: 'Failed execution',
    value: 1
  },
  undefined,
  {
    label: '找不到该会员资料',
    code: 'Customer not found',
    value: 3
  },
  {
    label: '厂商注单号码重复',
    code: 'Vendor_Trans_ID Duplicated',
    value: 4
  },
  {
    label: 'SQL 执行错误',
    code: 'SQL Exception',
    value: 5
  },
  {
    label: '球头已不存在',
    code: 'Point is expired',
    value: 6
  },
  {
    label: '赛事 ID 错误或是赛事已关闭',
    code: 'Match closed or invalid Market ID',
    value: 7
  },
  {
    label: '赔率变更',
    code: 'Odds changed',
    value: 8
  },
  {
    label: '厂商标识符错误',
    code: 'Invalidate vendor_id',
    value: 9
  },
  {
    label: '系统维护中',
    code: 'Under maintenance',
    value: 10
  },
  {
    label: '超过最大/最小投注限额',
    code: 'Stake problem',
    value: 11
  },
  {
    label: '执行失败',
    code: '赔率正在调整',
    value: 12
  },
  {
    label: '余额不足',
    code: 'Credit problem',
    value: 14
  },
  {
    label: '超过每场下注限额',
    code: 'Over max per match',
    value: 15
  },
  {
    label: '超过单注下注限额',
    code: 'Over max per bet',
    value: 16
  },
  {
    label: '赔率错误',
    code: 'Odds errorn',
    value: 17
  },
  {
    label: '低于最低串关赛事数量',
    code: 'Under can bet count',
    value: 19
  },
  {
    label: '串关错误',
    code: 'Lucky single problem',
    value: 20
  },
  {
    label: '账号无法投注',
    code: 'Diabled bet',
    value: 21
  },
  {
    label: '赛事比分改变',
    code: 'Score changed',
    value: 22
  },
  {
    label: '赛事球头改变',
    code: 'Point changed',
    value: 23
  },
  {
    label: '盘口暂时关闭',
    code: 'Close price',
    value: 24
  },
  {
    label: '盘口关闭',
    code: 'Market closed',
    value: 25
  }
];
