import { defineComponent, nextTick, toRaw, watch, onMounted, PropType } from 'vue';
import { LeagueItem } from '@/services/kg-api';
import './match-filter.style.less';

import { Button, IndexBar, CheckboxGroup, IndexAnchor, Checkbox } from 'vant';
import { MobileHeader } from '@/components/mobile-header';
import { DataEmpty } from 'kg-mobile/compoents';

import { useLeagueFliter } from './useLeagueFliter.hook';
import { useRoute } from 'vue-router';
import { isEmpty } from 'lodash-es';
import { useI18n } from '@/hooks';

interface AllLeagueList {
  [name: string]: LeagueItem[];
}

export default defineComponent({
  name: 'LeagueFilter',
  props: {
    sportId: {
      type: Number,
      require: true
    },
    isResult: {
      type: Boolean,
      default: false
    },
    time: {
      type: Object as PropType<{ startTime: number; endTime: number }>,
      require: false
    }
  },
  emits: ['confirm', 'close'],
  setup(props, { emit }) {
    const route = useRoute();
    const { t } = useI18n();
    const {
      getLeaguesData,
      leagueStore,
      leagueCounter,
      letters,
      checkAll,
      toggleAll,
      checkboxGroupDom,
      allLeagueCheckedlength,
      isBtnDisabled
    } = useLeagueFliter({ isResult: props.isResult });

    onMounted(async () => {
      leagueStore.form.periodId = Number(route.query.periodId);
      leagueStore.form.sportId = Number(props.sportId);
      if (props.time) {
        leagueStore.resultForm.startTime = props.time.startTime;
        leagueStore.resultForm.gameId = props.sportId as unknown as string;
        leagueStore.resultForm.endTime = props.time.endTime;
      }
      await getLeaguesData();
    });

    // 默认全选
    watch(letters, val => {
      if (isEmpty(val)) return;
      nextTick(checkAll);
    });

    // 关闭
    const onHandleClose = async () => emit('close');

    // 确认选中
    const confirmChecked = async () => {
      emit('confirm', toRaw(leagueStore.allLeagueChecked));
      await onHandleClose();
    };

    // 多选框UI
    const renderCheckbox = (item: LeagueItem) => (
      <Checkbox name={item.leagueId} class="league-check">
        <div class="league-info van-ellipsis">{item.leagueName}</div>
      </Checkbox>
    );

    // 所有联赛dom
    const RenderAllLeague = () => (
      <div class="all-league">
        <div class="text-01">{t('lang.sport_filterLeague_allLeagues')}A-Z</div>
        <IndexBar class="index-bar" indexList={letters.value} sticky={false}>
          <CheckboxGroup v-model={leagueStore.allLeagueChecked} ref={checkboxGroupDom}>
            <div style={{ height: '5px' }}></div>
            {!isEmpty(letters.value) &&
              letters.value.map((el: keyof AllLeagueList, index: number) => (
                <>
                  <IndexAnchor index={`${index}`}>{el}</IndexAnchor>
                  {!!leagueStore.leaguesList[el] &&
                    leagueStore.leaguesList[el].map((item: LeagueItem, i: number) => (
                      <div class="league-select" key={i + '_LI_KEY'}>
                        {renderCheckbox(item)}
                      </div>
                    ))}
                </>
              ))}
          </CheckboxGroup>
        </IndexBar>
      </div>
    );

    // 底部Dom
    const FooterRender = () => (
      <div class="filter-footer">
        <div class="console-btn">
          <div
            onClick={checkAll}
            class={[
              'van-checkbox__icon van-checkbox__icon--round league-check',
              !!leagueStore.isCheckedAll ? 'van-checkbox__icon--checked' : null
            ]}
            style="display: flex;"
          >
            <div class="van-checkbox__icon">
              <i class="van-badge__wrapper van-icon van-icon-success"></i>
            </div>
            <span class="invert-checked">
              {/* 全选 */}
              {t('lang.sport_common_selectAll')}
            </span>
          </div>
          <span class="invert-checked" onClick={toggleAll}>
            {/* 反选 */}
            {t('lang.sport_common_selectReverse')}
          </span>
        </div>
        <Button disabled={isBtnDisabled.value} class="confirm" onClick={confirmChecked}>
          {/* 确定 */}
          {t('lang.sport_common_confirm')}({allLeagueCheckedlength.value})
        </Button>
      </div>
    );

    return () => {
      return (
        <div class="league-filter">
          <div class="filter-header">
            <MobileHeader
              back={false}
              onClick-left={onHandleClose}
              title={t('lang.sport_filterLeague') + leagueCounter.value}
            />
          </div>
          {isEmpty(leagueStore.leaguesList) ? (
            <DataEmpty
              title={t('lang.sport_handicap_noneMatch')}
              desc={t('lang.sport_common_lookElsewhere')}
              class="nodata"
            />
          ) : (
            <div class="filter-content">
              <RenderAllLeague />
            </div>
          )}
          <FooterRender />
        </div>
      );
    };
  }
});
