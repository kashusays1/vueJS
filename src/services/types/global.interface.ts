import { SourceType } from '@/types/config';

export interface GetMessageForm {
  /**
   * 语言代码
   */
  country?: string;
  /**
   * 开始时间
   */
  startDate: string;
  /**
   * 结束时间
   */
  endDate: string;
  source: SourceType;
}
