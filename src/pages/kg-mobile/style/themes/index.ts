import { ThemeEnum, cssRootVar } from '@/common';

const themes = {
  default: {
    '--van-primary-color': ThemeEnum.green,
    '--sp-primary-color': ThemeEnum.green,
    '--sp-bg-color': '#f2f2f2',
    '--sp-white-color': '#fff',
    '--sp-border-color': '#f2f2f2'
  },
  golden: {
    '--van-primary-color': ThemeEnum.golden,
    '--sp-primary-color': ThemeEnum.golden
  },
  green: {
    '--van-primary-color': ThemeEnum.green,
    '--sp-primary-color': ThemeEnum.green,
    '--sp-bg-color': '#f2f2f2',
    '--sp-white-color': '#fff',
    '--sp-border-color': '#f2f2f2'
  },
  dark: {
    '--van-primary-color': ThemeEnum.darkGold,
    '--sp-primary-color': ThemeEnum.darkGold,
    '--sp-bg-color': '#f2f2f2',
    '--sp-white-color': '#333',
    '--sp-border-color': ThemeEnum.darkGold
  }
};

export type ThemeType = 'default' | 'golden' | 'green' | 'dark';

// 设置主题
export function setTheme(theme: ThemeType = 'default') {
  cssRootVar(themes[theme]);
}
