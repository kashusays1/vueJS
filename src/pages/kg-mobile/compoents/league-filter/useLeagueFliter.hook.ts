/**
 * 联赛筛选
 */

import { reactive, computed, ref } from 'vue';
import { queryLeagueList } from '@/services/kg-api';
import { getThirdLeagueId } from '@/services';
import { LeagueItem } from '@/services/kg-api';
import { formatLeagueList, MarketType } from '@/common';
import { isEmpty } from 'lodash-es';
import { useSportOptins } from '@/hooks';

interface Letters {
  [name: string]: LeagueItem[];
}

interface LeagueStore {
  leaguesList: Letters | [];
  allLeagueChecked: string[];
  isCheckedAll: boolean;
  resultForm: {
    startTime: number;
    endTime: number;
    gameId: string;
    thirdType: string;
  };
  form: {
    periodId: number;
    // 体育项目ID
    sportId: number;
    isParlay: boolean;
    oddsType: MarketType;
  };
}

const checkboxGroupDom = ref<any>(null);

export function useLeagueFliter({ isResult } = { isResult: false }) {
  const { optionsStore } = useSportOptins();

  const store = reactive<LeagueStore>({
    form: {
      periodId: 0,
      sportId: 0,
      isParlay: true,
      oddsType: optionsStore.value.marketType
    },
    resultForm: {
      startTime: 0,
      endTime: 0,
      gameId: '',
      thirdType: 'im'
    },
    leaguesList: {},
    allLeagueChecked: [],
    isCheckedAll: false
  });

  // 是否可以点击
  const isBtnDisabled = computed(() => isEmpty(store.allLeagueChecked));

  const allLeagueCheckedlength = computed(() => store.allLeagueChecked.length);
  // 首字母
  const letters = computed(() => Object.keys(store.leaguesList) || []);

  const leagueCounter = computed(() => {
    let reulst = 0;
    letters.value.map((el: any) => {
      reulst += store.leaguesList[el].length;
    });
    return reulst;
  });

  // 全选
  const checkAll = () => {
    !!checkboxGroupDom.value && checkboxGroupDom.value.toggleAll(true);
    store.isCheckedAll = true;
  };

  // 反选
  const toggleAll = () => {
    store.isCheckedAll = !store.isCheckedAll;
    !!checkboxGroupDom.value && checkboxGroupDom.value.toggleAll();
  };

  const getLeaguesData = async () => {
    if (isResult) {
      await getSesultData();
    } else {
      await getMarketLeagues();
    }
  };

  const getMarketLeagues = async () => {
    const [res, data] = await queryLeagueList(store.form);
    if (res) {
      const d = await formatLeagueList(data);
      store.leaguesList = d;
    }
  };

  const getSesultData = async () => {
    const [res, data] = await getThirdLeagueId(store.resultForm);
    if (res) {
      const d = await formatLeagueList(data);
      store.leaguesList = d;
    }
  };

  return {
    leagueStore: store,
    checkAll,
    toggleAll,
    allLeagueCheckedlength,
    getLeaguesData,
    checkboxGroupDom,
    leagueCounter,
    letters,
    isBtnDisabled
  };
}
