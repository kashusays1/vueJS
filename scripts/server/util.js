function parse(value) {
  try {
    return JSON.parse(value);
  } catch (e) {
    return null;
  }
}
module.exports = {
  parse
};
