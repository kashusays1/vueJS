import { defineComponent, ref } from 'vue';

import { Collapse, CollapsePanel, LeagueFilter, KeyboardInput, ShoppingCart } from 'kg-mobile/compoents';
import { Popup } from 'vant';

import { useI18n } from '@/hooks';
import { useLocale } from '@/i18n/useLocale';

export default defineComponent({
  name: 'JBBdemo',
  setup() {
    const isExpand = ref(false);
    const show = ref(false);
    const showLoading = ref(false);
    const { t } = useI18n();

    const amount = ref('');

    const { changeLocale } = useLocale();
    const expandAll = () => {
      isExpand.value = !isExpand.value;
    };

    return () => (
      <div style="padding-bottom: 40px">
        <div class="v">
          <button onClick={() => changeLocale('en')}>EN</button>
          <button onClick={() => changeLocale('zh-CN')}>zh-CN</button>
          <hr />
          国际化: {t('lang.001JiangBuHuiCha')}
        </div>
        <ShoppingCart />
        <Popup v-model:show={show.value} position="bottom" style={{ height: '100%', width: '100%' }}>
          <LeagueFilter onClose={() => (show.value = !show.value)} />
        </Popup>
        <div>
          <button onClick={() => (show.value = !show.value)}>{show.value ? '打开联赛' : '收起联赛'}全部</button>
          <button onClick={() => (showLoading.value = !showLoading.value)}>
            {showLoading.value ? '打开加载' : '收起加载'}
          </button>
          <button onClick={() => expandAll()}>{isExpand.value ? '展开' : '收起'}全部</button>
        </div>
        <Collapse expandAll={isExpand.value}>
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>
              // header: () => <div>Collapse 折叠面板</div>
            }}
          />
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>,
              header: () => <div>Collapse 折叠面板</div>
            }}
          />
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>,
              header: () => <div>Collapse 折叠面板</div>
            }}
          />
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>,
              header: () => <div>Collapse 折叠面板</div>
            }}
          />
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>,
              header: () => <div>Collapse 折叠面板</div>
            }}
          />
          <CollapsePanel
            v-slots={{
              default: () => <div>将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。</div>,
              header: () => <div>Collapse 折叠面板</div>
            }}
          />
        </Collapse>
        <KeyboardInput currency="CNY" max={1000} min={10} v-model:amount={amount.value}>
          <div style="width: 50%;">单注:</div>
        </KeyboardInput>
        <KeyboardInput currency="JPY" max={1000} min={10} v-model:amount={amount.value} />
        <div class="demo-anit"></div>
      </div>
    );
  }
});
