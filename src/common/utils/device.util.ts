import device from 'current-device';

// 手机
export const isMobile = device.mobile();
// 苹果手机
export const isIOS = device.iphone() && isMobile;
// 安卓手机
export const isAndriod = device.android() && isMobile;
