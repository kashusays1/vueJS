import { defineComponent, onMounted } from 'vue';
import { Icon } from 'vant';
import './recommended-match.style.less';

import { useRecommendedMarket, hasTicketId, useBet, StoreMatchInfo } from '@/hooks';
import { OddsPrice, TeamLogo } from 'kg-mobile/compoents';
import { SelType } from '@/services/kg-api';
import { isEmpty } from 'lodash-es';
import { promiseTimeout } from '@vueuse/shared';
import { useTimeoutPoll } from '@vueuse/core';
import { useRouter } from 'vue-router';
import { RouteEnum } from 'kg-mobile/router/router.enum';
import { _keyNameByBettype } from '@/common';

export default defineComponent({
  name: 'RecommendedMatch',
  props: {
    periodId: {
      type: Number,
      default: 3
    }
  },
  setup(props) {
    const { getRecommendedMarketData, recStore } = useRecommendedMarket();
    const { addTicket } = useBet();
    const router = useRouter();

    const fetchData = async () => {
      await promiseTimeout(5000);
      await getRecommendedMarketData();
    };

    // sportId=1&periodId=3&matchId=52800361
    const pathToDetail = ({ sportId, matchId }: any) => {
      router.push({
        path: RouteEnum.SPORT_DETAIL,
        query: {
          sportId,
          periodId: props.periodId,
          matchId
        }
      });
    };

    const { resume } = useTimeoutPoll(fetchData, 500 * 1000);

    onMounted(async () => {
      await getRecommendedMarketData();
      resume();
    });

    const LockOddsRender = () => (
      <div class="lock-odds">
        <svg-icon name="lock" />
      </div>
    );

    const RecHeacer = ({ teamInfo }: { teamInfo: StoreMatchInfo }) => (
      <div class="rec-header">
        <span class={{ label: true, 'is-live': teamInfo.isLive }}>
          {teamInfo.isLive ? teamInfo.time || 'LIVE' : `${teamInfo.date} ${teamInfo.time}`}
        </span>
        <span class="league-name">{teamInfo.leagueName}</span>
        <div class="odd-count" onClick={() => pathToDetail({ sportId: teamInfo.sportId, matchId: teamInfo.id })}>
          <span>{teamInfo.count}</span>
          <Icon name="arrow" size={12} />
        </div>
      </div>
    );

    const Teams = ({ teamInfo, BettypeName }: { teamInfo: StoreMatchInfo; BettypeName: string }) => (
      <div class="teams">
        <div class="home-team">
          <div class="t-logo">
            <TeamLogo sportId={teamInfo.sportId} teamId={teamInfo.homeId} />
          </div>
          <span class={'team-name'}>{teamInfo.home}</span>
        </div>
        <div class="center-info">
          {teamInfo.isLive ? (
            <div class="show-score">
              {teamInfo.score && teamInfo.score[0]} : {teamInfo.score && teamInfo.score[1]}
            </div>
          ) : (
            <div class="vs">VS</div>
          )}
          <div class="odd-type">{BettypeName || '--'}</div>
        </div>
        <div class="away-team">
          <div class="t-logo">
            <TeamLogo sportId={teamInfo.sportId} teamId={teamInfo.awayId} />
          </div>
          <span class={'team-name'}>{teamInfo.away}</span>
        </div>
      </div>
    );
    return () => (
      <div class="recommended-match">
        <div class="scroll-container">
          {!isEmpty(recStore.recMatchList)
            ? recStore.recMatchList.map(item => (
                <div key={item.key} class="recommended-match-item">
                  <div class="contaier">
                    <RecHeacer teamInfo={item.info} />
                    <Teams teamInfo={item.info} BettypeName={item.oddset[0].BettypeName} />
                    <div class="odds">
                      {item.oddset[0].sels.map((sel: SelType) => (
                        <div
                          onClick={() =>
                            addTicket({
                              bettype: item.oddset[0].Bettype,
                              betTypeName: item.oddset[0].BettypeName,
                              matchId: item.info.id,
                              market_id: item.oddset[0].OddsId,
                              sport_type: item.info.sportId,
                              odds_type: sel.OddsType,
                              wagerSelectionId: sel.WagerSelectionId,
                              price: sel.Price,
                              periodId: item.oddset[0].PeriodId,
                              point: sel.Point,
                              key: sel.Key,
                              keyName: sel.KeyName,
                              outrightTeamId: 0
                            } as any)
                          }
                          class={[
                            'odds-item',
                            hasTicketId({ market_id: sel.WagerSelectionId, key: sel.Key, matchId: item.info.id })
                              ? 'active'
                              : null
                          ]}
                        >
                          {item.oddset[0]?.lock ? (
                            <LockOddsRender />
                          ) : (
                            <>
                              <span>{_keyNameByBettype(item.oddset[0].Bettype, sel)}</span>
                              <OddsPrice class="odds-value" value={sel.Price} />
                            </>
                          )}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              ))
            : null}
        </div>
      </div>
    );
  }
});
