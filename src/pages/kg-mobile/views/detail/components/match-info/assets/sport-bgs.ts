export const sportBgs = {
  1: {
    general: require('./1.png')
  },
  2: {
    general: require('./2.png')
  },
  4: {
    general: require('./4.png')
  },
  5: {
    general: require('./5.png')
  },
  6: {
    general: require('./6.png')
  },
  18: {
    general: require('./18.png')
  },
  26: {
    general: require('./26.png')
  },
  50: {
    general: require('./50.png')
  },
  999: {
    general: require('./999.png')
  }
};
