export enum RouteEnum {
  ROOT = '/',
  HOME = '/home',
  SPORT_DETAIL = '/detail',
  BET_HISTORY = '/bet-history',
  LOGIN = '/auth',
  SEARCH = '/search',
  ORDERS = '/orders',
  SETTING = '/setting',
  NOT_FOUND = '/404',
  NOTICE = '/notice',
  RESULT = '/result',
  DEMO = '/demo'
}
