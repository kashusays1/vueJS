import { reactive } from 'vue';
import { getSportNameById, isMobile, SportIdEnum, getDayTimestamp, getWeek } from '@/common';
import { isEmpty, groupBy } from 'lodash-es';
import { getResultsApi } from '@/services/kg-api';
import { useDebounceFn } from '@vueuse/core';
import { Laoding } from 'kg-mobile/compoents';

import dayjs from 'dayjs';

interface DayWeek {
  week: any;
  date: string;
  startTime: number;
  endTime: number;
}

function generateLast7days() {
  const temlsit: DayWeek[] = [];
  for (let index = 0; index < 7; index++) {
    temlsit[index] = {
      week: getWeek(getDayTimestamp(7 - index)),
      date: dayjs().subtract(index, 'day').format('MM/DD'),
      startTime: dayjs().subtract(index, 'day').startOf('day').valueOf(),
      endTime: dayjs().subtract(index, 'day').endOf('day').valueOf()
    };
  }
  return temlsit;
}

export const LAST_7_DAYS = generateLast7days();

export const SPORTS_LIST = [
  {
    label: getSportNameById(SportIdEnum.Soccerball) || '足球',
    value: SportIdEnum.Soccerball
  },
  {
    label: getSportNameById(SportIdEnum.Basketball) || '篮球',
    value: SportIdEnum.Basketball
  }
];

export function useEventResult() {
  const state = reactive({
    showLeagus: false,
    showSheet: false,
    matchMapByLeagueId: {} as any,
    showTypeKey: 'time' as 'time' | 'sport',
    currentTimeIndex: 0,
    currentSportIndex: 0
  });

  const form = reactive({
    sportId: SPORTS_LIST[0].value,
    startTime: generateLast7days()[0].startTime,
    endTime: generateLast7days()[0].endTime,
    leagueId: ''
  });

  const openSheet = ({ type }: { type: 'time' | 'sport' }) => {
    state.showSheet = true;
    state.showTypeKey = type;
  };

  const closeSheet = () => {
    state.showSheet = false;
  };

  const openLeagues = () => {
    state.showLeagus = !state.showLeagus;
  };

  const getData = async (cb?: () => void) => {
    isMobile && Laoding.show();
    const [res, data] = await getResultsApi(form);
    isMobile && Laoding.hide();
    if (!isEmpty(data) && res) {
      state.matchMapByLeagueId = isEmpty(data) ? {} : groupBy(data, 'leagueId');
    }
    if (isMobile) {
      document.documentElement.scrollTop = 0;
      document.body.scrollTop = 0;
    }
    cb && cb();
  };

  const onHandleDate = async (index: number) => {
    if (state.currentTimeIndex === index) return;
    state.currentTimeIndex = index;
    form.startTime = LAST_7_DAYS[state.currentTimeIndex].startTime;
    form.endTime = LAST_7_DAYS[state.currentTimeIndex].endTime;
    await getData();
    closeSheet();
  };

  const onHandleSport = async (index: number) => {
    if (state.currentSportIndex === index) return;
    state.currentSportIndex = index;
    form.sportId = SPORTS_LIST[state.currentSportIndex].value;
    await getData();
    closeSheet();
  };

  return {
    getData,
    onHandleDate: useDebounceFn(onHandleDate, 200),
    onHandleSport: useDebounceFn(onHandleSport, 200),
    openSheet,
    closeSheet,
    openLeagues,
    state
  };
}
