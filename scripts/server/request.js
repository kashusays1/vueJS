const axios = require('axios');

// 请求地址
const BASE_URL = process.env.VUE_APP_CMS_API || 'https://api2-tgitnt.kg999vip.com';

const service = axios.create({
  timeout: 5000,
  headers: {
    // "Content-Type": "application/x-www-form-urlencoded",
    'Content-Type': 'application/json'
  }
});

service.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const result = response.data.code === 0;
    return Object.assign(response.data, { result });
  },
  error => {
    return Promise.reject(error);
  }
);

function getI18nJson(lang = 'en', type) {
  const apiUrl = `${BASE_URL}/api/messageI18n/international/list?language=${lang}${type ? `&type=${type}` : ''}`;
  console.log('apiUrl', apiUrl);
  return axios.get(apiUrl);
}

function getSystemConfig() {
  return axios.get(`${BASE_URL}/api/config/system`);
}

module.exports = {
  getI18nJson,
  getSystemConfig
};
