import { defineComponent, onMounted } from 'vue';
import { get } from 'lodash';
import { MarketItem, Collapse } from 'kg-mobile/compoents';

import { useEventsAndMarkets } from '@/hooks';
import { useRoute } from 'vue-router';

import './related-match.less';

export default defineComponent({
  name: 'JBBsportDataTotal',
  props: {
    sportName: {
      type: String,
      default: ''
    },
    leagueId: {
      type: String,
      default: ''
    }
  },
  setup(props) {
    const route = useRoute();
    const { eventStore, getEventsAndMarkets } = useEventsAndMarkets();
    const { sportId, periodId } = route.query;

    onMounted(async () => {
      eventStore.form.periodId = Number(periodId);
      eventStore.form.sportId = Number(sportId);
      eventStore.form.leagueIds = props.leagueId;
      await getEventsAndMarkets();
    });

    return () => (
      <div class="related-match">
        <div class="related-match-header">
          <span>{get(eventStore, 'marketList.0.leagueName')}</span>
        </div>
        <div class="related-match-content">
          <Collapse>
            {eventStore.marketList.map(item => (
              <MarketItem data={item} key={item.leagueId} />
            ))}
          </Collapse>
        </div>
      </div>
    );
  }
});
