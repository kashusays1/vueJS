/**
 * useCurrencySheet
 * 货币选择
 */
import { reactive } from 'vue';

import A1 from './assets/美国@3x.png';
import A2 from './assets/日元@3x.png';
import A3 from './assets/money.png';
const TEST_DATAT = [
  {
    name: '全部',
    code: 'USD',
    icon: A3,
    blacne: 20
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  },
  {
    name: '美元',
    code: 'USD',
    icon: A1,
    blacne: 20
  },
  {
    name: '日元',
    code: 'JPY',
    icon: A2,
    blacne: 200
  }
];

interface Currency {
  name: string;
  code: string;
  icon: string;
  blacne: number;
}

interface CurrencySheetStore {
  selectedCurrency: number;
  currencyList: Currency[];
}

export function useCurrencySheet() {
  const store = reactive<CurrencySheetStore>({
    selectedCurrency: 0,
    currencyList: TEST_DATAT
  });
  return {
    store
  };
}
