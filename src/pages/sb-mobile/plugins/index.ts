import { App } from 'vue';
import vantList from './vant';
import componentList from './comp';
import { Lazyload } from 'vant';

export function initPlugin(app: App): void {
  app.use(Lazyload);
  [...vantList, ...componentList].forEach(component => app.component(component.name, component));
}
