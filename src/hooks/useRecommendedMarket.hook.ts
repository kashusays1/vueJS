/**
 * 旧版赛事推荐hook
 */

import { reactive } from 'vue';
import { getRecommendedMarketList, OddsetData } from '@/services/kg-api';
import { StoreMatchInfo, useSportOptins } from '@/hooks';
import { isEmpty, map } from 'lodash-es';
import { _formatMatchInfo, _formatRecMarketOdds } from '@/common';
// import { TechId } from '@/common/enums/sportsTechEnum';

export interface RecTeamInfo {
  IsLive: 1 | 0;
  LeagueName: string;
  MarketCount: number;
  SportType: number;
  HomeName: string;
  AwayName: string;
  SportName: string;
  HomeID: number;
  AwayID: number;
  MatchId: number;
  LeagueId: string;
  oddset: OddsetData[];
}

export function useRecommendedMarket() {
  const { optionsStore } = useSportOptins();

  const store = reactive({
    recMatchList: [] as {
      key: string;
      info: StoreMatchInfo;
      oddset: OddsetData[];
    }[],
    form: {
      oddsType: optionsStore.value.marketType,
      source: 'im'
    }
  });

  const getRecommendedMarketData = async () => {
    const [isSuccess, data] = (await getRecommendedMarketList(store.form)) as [boolean, RecTeamInfo[]];
    if (isSuccess) {
      const tempList: any[] = [];
      // 转换数据
      if (!isEmpty(data)) {
        map(data, (marketItem: RecTeamInfo, idx) => {
          const result = {
            key: marketItem.MatchId + '_mk' + idx,
            info: _formatMatchInfo({
              match: marketItem,
              league: { LeagueName: marketItem.LeagueName, SportName: marketItem.SportName },
              isInProgress: marketItem.IsLive === 1,
              sportId: marketItem.SportType
            }),
            oddset: _formatRecMarketOdds(marketItem.oddset)
          };
          tempList.push(result);
        });
      }
      store.recMatchList = tempList;
    }
  };

  return {
    recStore: store,
    getRecommendedMarketData
  };
}
