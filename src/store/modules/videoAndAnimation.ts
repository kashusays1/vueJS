import { ActionContext } from 'vuex';
import { getSportUnionData } from '@/services';

import { SourceType } from '@/types/config';

interface VideoAnimateState {
  videoAndAnimationData: {
    animate: Map<string | number, string>;
    video: Map<string | number, string>;
  };
}

const initState = (): VideoAnimateState => ({
  videoAndAnimationData: {
    animate: new Map(),
    video: new Map()
  }
});

const state: VideoAnimateState = initState();

const getters = {
  getVideoByMid: (state: VideoAnimateState) => (mid: number | string) => state.videoAndAnimationData.video.get(mid),
  getAnimateyByMid: (state: VideoAnimateState) => (mid: number | string) => state.videoAndAnimationData.animate.get(mid)
};

const actions = {
  /**
   * 获取比赛媒体消息（直播视频、动画）
   */
  async GET_SPORT_MEDIA_DATA(
    { commit }: ActionContext<VideoAnimateState, any>,
    { source }: { source: SourceType } = { source: 'sb' }
  ) {
    const [res, data] = await getSportUnionData();
    const multimedia = {
      animate: new Map(),
      video: new Map()
    };
    if (res && typeof data === 'object') {
      data.video2.forEach((item: any) => {
        const key = source === 'im' ? item.imId : item.sbId;
        key && multimedia.video.set(key, item.videoListUrl);
      });
      data.animate.forEach((item: any) => {
        const key = source === 'im' ? item.imId : item.sbId;
        key && multimedia.animate.set(key, item.url);
      });
    }
    commit('SET_SPORT_MEDEA_DATA', multimedia);
    return multimedia;
  }
};

// mutations
const mutations = {
  SET_SPORT_MEDEA_DATA(state: VideoAnimateState, payload: any) {
    state.videoAndAnimationData = payload;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
