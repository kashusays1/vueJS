/**
 * 数据格式化
 */

import { isEmpty, isArray, map, filter, sortBy, fill, uniq, split } from 'lodash-es';
import { EventSummary } from '@/services';
import { DataTypeEnum, SportIdEnum, MarketStatusEnum, SPORT_MAP_KEY } from '@/common/enums';
import { SportTypeItem } from '@/store/types';
import { OddsetData, SelType } from '@/services/im-api';
import { eventTimeFormatForIM } from '@/common/utils';
import dayjs from 'dayjs';

let oddTemp: any = {};

const convertSportsSort = (list: EventSummary[]) => {
  const temp: {
    live: SportTypeItem[];
    today: SportTypeItem[];
    early: SportTypeItem[];
  } = {
    live: [],
    today: [],
    early: []
  };
  const keys = Object.keys(temp);
  list.forEach(item => {
    keys.forEach(k => {
      item[k] > 0 &&
        temp[k].push({
          sportId: item.sport_type,
          sportName: item.sport_type_name,
          count: item[k],
          live: item.live
        });
    });
  });
  return new Map([
    [DataTypeEnum.Live, temp.live],
    [DataTypeEnum.Today, temp.today],
    [DataTypeEnum.Early, temp.early]
  ]);
};

/**
 * 格式化体育盘口列表数据
 */
const convertSportsHandicap = (
  list: any[],
  {
    periodId,
    sportId,
    showOdds,
    oldOdds
  }: { periodId: DataTypeEnum; sportId: SportIdEnum; showOdds: number[]; oldOdds: any }
) => {
  // 是否滚球
  const isInProgress = periodId === DataTypeEnum.Live;
  // 暂时先注解掉
  // const filterList = filter(list, l => !isEmpty(filter(l.matches, m => m.ParentID === 0)));
  const formatedList = map(list, league => {
    return {
      leagueId: league.LeagueId,
      key: `${league.LeagueId}_${league.matches[0].MatchId}`,
      time: dayjs(league.matches[0].ShowTime).valueOf(),
      leagueName: league.LeagueName,
      matches: map(league.matches, match => ({
        info: _formatMatchInfo({ match, league, isInProgress, sportId }),
        odds: _formatMatchOdds({ odds: match.oddset, showOdds, oldOdds })
      }))
    };
  });
  setTimeout(() => {
    oddTemp = {};
  });
  return { list: formatedList, odd: oddTemp };
};

/**
 *格式化赛事基本信息
 *
 * @param {*} { match, isInProgress, sportId }
 * @return {*}
 */
function _formatMatchInfo({ match, league, isInProgress, sportId }: any): any {
  const { score = {}, period } = isInProgress ? _formatScoreAndTimeText({ match, sportId }) : { period: '' };
  const filterTime = (time: string): string => {
    if (time && !time.includes(':')) return '';
    return time;
  };
  const isNeutral = (sportId: number): boolean => {
    return sportId === 1;
  };
  return {
    sportName: league.SportName,
    time: isInProgress ? filterTime(match.RunTime) : dayjs(match.ShowTime).format('HH:mm'),
    home: match.HomeName,
    homeId: match.HomeID,
    away: match.AwayName,
    awayId: match.AwayID,
    leagueName: league.LeagueName,
    market: match.Market,
    id: match.MatchId,
    isLive: isInProgress,
    count: match.MarketCount,
    isNeutral: match.IsNeutral && isNeutral(sportId),
    date: dayjs(match.ShowTime).format('MM/DD'),
    period,
    sportId,
    ...score
  };
}

/**
 *格式化比分信息和时间文本
 *
 * @param {*} { match, isInProgress, sportId }
 */
function _formatScoreAndTimeText({ match, sportId }: any) {
  return {
    score: _formatScore({ match, sportId }),
    period: eventTimeFormatForIM({ ...match, ...match.MoreInfo, SportType: sportId })
  };
}
/**
 *格式化比分
 *
 * @param {*} {match, sportId}
 */
function _formatScore({ match, sportId }) {
  // 主队分数 客队分数 目前进行到第几节 赛事总节数 目前赛事时间
  const { ScoreH, GetGamesA, ScoreA, GetGamesH } = match.MoreInfo;
  // console.log(LivePeriod, GameSession);
  // 足球
  if (sportId === SportIdEnum.Soccerball) {
    const { RedCardH, RedCardA, YellowCardH, YellowCardA } = match.MoreInfo;
    return {
      sportId: sportId,
      score: [ScoreH || 0, ScoreA || 0],
      tech: {
        ht: [0, 0], // TODO 半场比分
        red: [RedCardH || 0, RedCardA || 0],
        yellow: [YellowCardH || 0, YellowCardA || 0]
      }
    };
  }
  // 篮球
  if (sportId === SportIdEnum.Basketball) {
    const { A1Q, A2Q, A3Q, A4Q, H1Q, H2Q, H3Q, H4Q, LLP } = match.MoreInfo;
    return {
      sportId: sportId,
      score: [ScoreH || 0, ScoreA || 0],
      current: Number(LLP) - 1,
      sections:
        H1Q !== undefined
          ? [
              [H1Q || 0, A1Q || 0],
              [H2Q || 0, A2Q || 0],
              [H3Q || 0, A3Q || 0],
              [H4Q || 0, A4Q || 0]
            ]
          : []
    };
  }
  //网球
  if (sportId === SportIdEnum.Tennis) {
    const { TennisAwayGameScore, TennisHomeGameScore, TennisHomePointScore, TennisAwayPointScore, TennisCurrentSet } =
      match.MoreInfo;
    return {
      sportId: sportId,
      score: [ScoreH || 0, ScoreA || 0],
      current: TennisCurrentSet - 1,
      pointScore: [TennisHomePointScore, TennisAwayPointScore],
      sections: map(TennisHomeGameScore, (v, k) => {
        return [v, TennisAwayGameScore[k]];
      })
    };
  }
  //排球、羽毛球、乒乓球
  if (sportId === SportIdEnum.Volleyball || sportId === SportIdEnum.Badminton || sportId === SportIdEnum.TableTennis) {
    const { H1S, H2S, H3S, H4S, H5S, A1S, A2S, A3S, A4S, A5S, GetGamesH, GetGamesA, LLP } = match.MoreInfo;
    return {
      sportId: sportId,
      score: [ScoreH || 0, ScoreA || 0],
      current: Number(LLP) - 1,
      sections: [
        [H1S, A1S],
        [H2S, A2S],
        [H3S, A3S],
        [H4S, A4S],
        [H5S, A5S],
        [GetGamesH, GetGamesA]
      ]
    };
  }

  // 其他
  return {
    sportId: sportId,
    score: [ScoreH || GetGamesH, ScoreA || GetGamesA]
  };
}
/**
 *格式化赛事盘口信息
 *
 * @param {{ odds: any[]; showOdds: number[] }} { odds, sportId }
 * @return {*}
 */
function _formatMatchOdds({ odds, showOdds, oldOdds }: { odds: any[]; showOdds: number[]; oldOdds: any }) {
  // const showOdd = FAST_BET_TYPES.get(sportId) || DEFAULT_BET_TYPES;
  return map(showOdds, id => {
    const oldOdd = sortBy(
      filter(odds, o => id === o.Bettype),
      s => -s.Sort
    )[0] || {
      Bettype: id
    };
    const odd = { ...oldOdd, sels: _fixListOddSel(oldOdd.sels, id) };
    return {
      betType: odd.Bettype,
      sels: map(odd.sels, sel => {
        const change =
          sel.Price > oldOdds[`${odd.OddsId}_${sel.Key}`]
            ? 'rise'
            : sel.Price < oldOdds[`${odd.OddsId}_${sel.Key}`]
            ? 'reduce'
            : 'smooth';
        sel.Price && (oddTemp[`${odd.OddsId}_${sel.Key}`] = sel.Price);
        return sel.filled
          ? sel
          : {
              betType: odd.Bettype,
              betTypeName: odd.BettypeName,
              category: odd.Category,
              combo: odd.Combo,
              marketStatus: odd.MarketStatus,
              maxBet: odd.MaxBet,
              oddsId: odd.OddsId,
              oddsType: odd.oddsType,
              key: sel.Key,
              selKey: `${odd.OddsId}_${sel.Key}`,
              keyName: sel.KeyName,
              name: _formatSelName(odd.Bettype, sel),
              price: sel.Price,
              point: sel.Point,
              locked: odd.MarketStatus !== MarketStatusEnum['running'] || sel.Price <= 0,
              change
            };
      })
    };
  });
}

/**
 *补充列表盘口
 *
 * @param {any[]} sels
 * @param {number} betType
 */
function _fixListOddSel(sels: any[] | undefined, betType: number) {
  // 独赢处理
  if (betType === 5 || betType === 15) {
    return sels
      ? sels.length === 3
        ? [sels[0], sels[2], sels[1]]
        : fill(Array(3), { locked: true, filled: true })
      : fill(Array(3), { filled: true });
  }
  return sels
    ? sels.length > 0
      ? sels
      : fill(Array(2), { locked: true, filled: true })
    : fill(Array(2), { filled: true });
}

function _formatSelName(betType: number, { KeyName, Point }: { Key: string; KeyName: string; Point: number }) {
  // console.log('betType', Key, betType);

  if ([1].includes(betType)) {
    return Point;
  } else if (
    [
      // 单双类型
      5,
      // 普通显示3列类型
      22, 707, 713,
      // 波胆类型
      6,
      // 反波胆类型
      158
    ].includes(betType)
  ) {
    return KeyName;
    // } else if (
    //   [
    //     // 显示主客类型
    //     1
    //   ].includes(betType)
    // ) {
    //   return {
    //     h: '主',
    //     a: '客'
    //   }[Key];
  } else if (
    [
      // 大小盘类型
      2
    ].includes(betType)
  ) {
    return KeyName + (Point ? Point : '');
  } else if (
    [
      // 独赢类型
      3
    ].includes(betType)
  ) {
    // return {
    //   '1': '主',
    //   x: '和',
    //   '2': '客'
    // }[Key];
    return KeyName;
    // } else if (
    //   [
    //     // 双重机会类型
    //     24
    //   ].includes(betType)
    // ) {
    //   return {
    //     '1x': '主 或 和',
    //     '12': '主 或 客',
    //     '2x': '客 或 和'
    //   }[Key];
    // } else if (
    //   [
    //     // 显示大小
    //     702, 705, 709, 9003, 9009, 9013, 9019, 9025, 9029, 9035, 9047, 9053, 9058, 9060, 9070
    //   ].includes(betType)
    // ) {
    //   return {
    //     o: '大',
    //     u: '小'
    //   }[Key];
  } else {
    return KeyName || Point;
  }
}

/**
 * 赛事详情-列表数据
 */
const convertSportsDetailList = (
  list: any[],
  { periodId, sportId }: { periodId: DataTypeEnum; sportId: SportIdEnum }
): any => {
  if (isEmpty(list) || !isArray(list) || isEmpty(list[0].matches) || !isArray(list[0].matches)) return {};
  const newList = _mergeSameData(list[0].matches[0].oddset);

  let groups: any[] = [];
  let formatedList = map(newList, item => {
    groups = [...groups, ...(item[0].group || [])];
    return {
      bettype: item[0].Bettype,
      bettypeName: item[0].BettypeName,
      category: item[0].Category,
      group: item[0].group,
      showTeamName: _isShowTeamName(item[0].Bettype),
      list: map(item, item2 => {
        return {
          oddsId: item2.OddsId,
          periodId: item2.PeriodId,
          combo: item2.Combo,
          maxBet: item2.MaxBet,
          sels: _setSels(item2.sels, item2.Bettype),
          isOneLine: _showOneName(item2.Bettype)
        };
      })
    };
  });
  //过滤掉空数据sels
  formatedList = formatedList.filter(x1 => !isEmpty(x1.list.filter(x2 => !isEmpty(x2.sels))));
  // 是否滚球
  const isInProgress = periodId === DataTypeEnum.Live;
  const match = list[0].matches[0];
  const result = {
    leagueId: list[0].LeagueId,
    leagueName: list[0].LeagueName,
    info: _formatMatchInfo({ match, league: list[0], isInProgress, sportId }),
    groups: uniq(groups),
    odds: formatedList
  };
  return result;
};
function _showOneName(Bettype: number) {
  // 让球、大小球、独赢、胜负、单双5、波胆、反波胆
  return ![1, 2, 3, 4, 6, 158].includes(Bettype);
}
function _setSels(sels: any[], Bettype: number) {
  // 波胆
  if (Bettype === 6 || Bettype === 158) {
    return _formatScoreBet(sels);
  }
  const arr = map(sels, item => {
    return {
      name: _formatSelName(Bettype, item),
      price: item.Price,
      ...item
    };
  });
  return arr;
}
function _formatScoreBet(sels: any[]) {
  const other = sels[sels.length - 1].KeyName.indexOf('-') === -1 ? sels.pop() : {};
  const [home, tie, away]: [any[], any[], any[]] = [[], [], []];
  sels.forEach(sel => {
    const [homeScore, awayScore] = sel.KeyName.split('-');
    if (Number(homeScore) > Number(awayScore)) {
      home.push({
        name: sel.KeyName,
        price: sel.Price,
        ...sel
      });
    } else if (Number(homeScore) < Number(awayScore)) {
      away.push({
        name: sel.KeyName,
        price: sel.Price,
        ...sel
      });
    } else {
      tie.push({
        name: sel.KeyName,
        price: sel.Price,
        ...sel
      });
    }
  });
  return [home, tie, away, { name: other.KeyName, price: other.Price }];
}
function _isShowTeamName(betType: number) {
  if (betType === 1) return true;
  return false;
}
/**
 * 将相同的Bettype放一个数组里面,并排序
 */
function _mergeSameData(list: any[]) {
  const keyArr: Array<any> = [];
  const newArr: Array<any> = [];
  map(list, item => {
    const key = `${item.Bettype}_${item.PeriodId}`;
    if (!keyArr.includes(key)) {
      keyArr.push(key);
      newArr.push(
        list
          .filter(x => `${x.Bettype}_${x.PeriodId}` === key && x.BettypeName === item.BettypeName)
          .sort((a, b) => {
            return a.Sort - b.Sort;
          })
      );
    }
  });
  return newArr;
}
const LOCAL_SP_MAPS = localStorage.getItem(SPORT_MAP_KEY);

/**
 * 获取运动名字
 * @param {id: number}
 */
function getSportNameById(id: number): string {
  if (LOCAL_SP_MAPS) {
    return JSON.parse(LOCAL_SP_MAPS)[id];
  }
  return '';
}

// 首页扩展接口
export interface HomeMarketOdds extends OddsetData {
  /**
   * 锁盘
   */
  locked: boolean;
}

function _keyNameByBettype(Bettype: number, { KeyName, Point }: SelType): string {
  let text = KeyName;
  switch (Bettype) {
    case 1:
      text = Point;
      break;
    case 2:
      text = KeyName + Point;
      break;
    case 3:
      text = KeyName;
      break;
    default:
      break;
  }
  return text;
}

/**
 * 转换首页盘口
 */
function _formatHomeMarketOdds(sportId: number, oddset: OddsetData[], PeriodId: number): any[] {
  console.log('sportId, oddset, PeriodId', sportId, oddset, PeriodId);
  if (isEmpty(oddset))
    return [
      { sels: ['', ''] },
      { sels: ['', ''] },
      { sels: ['', '', ''] },
      { sels: ['', ''] },
      { sels: ['', ''] },
      { sels: ['', '', ''] }
    ];
  const SORT_KEYS = sportId == 1 || sportId == 2 ? [1, 2, 3, 1, 2, 3] : [1, 2, 4, 1, 2, 4];
  const tempList: any[] = [];
  SORT_KEYS.map((key, index) => {
    const obj = oddset.find(e => e.Bettype > 0 && PeriodId == (index < 3 ? 1 : 2));
    // const obj = oddset.find(e => e.Bettype === key && PeriodId == (index < 3 ? 1 : 2));
    // console.log(oddset, obj, key, 'oddsetKey');
    // console.log(obj, 'obj');
    // console.log(obj, 'obj');
    const emptyData = key === 3 ? { sels: ['', '', ''] } : { sels: ['', ''] };
    tempList.push(obj ? obj : emptyData);
  });
  tempList.map(item => {
    // item.locked = item.sels[0].MarketStatus !== 'running' || item.Price <= 0;
    item.locked = item.sels[0].MarketStatus !== 'running' || item.sels[0].Price <= 0;
  });
  // console.log('tempList', tempList);
  return tempList;
}

/**
 * 转换首页盘口
 */
function _formatRecMarketOdds(oddset: OddsetData[]): any[] {
  if (isEmpty(oddset)) return [{ sels: ['', ''] }];
  const oddslist = oddset;
  oddslist.map((item: any) => {
    item.locked = item.MarketStatus !== MarketStatusEnum['running'];
  });
  return oddslist;
}

export function formatSportsTech(str = '') {
  const matchTempStrs = split(str, '&');
  return new Map(
    matchTempStrs.map(mStr => {
      const [matchId, otherStr] = split(mStr, '^');
      const [, , ...techStrs] = split(otherStr, ';');
      techStrs.pop();
      const techMap = new Map(
        techStrs.map(tStr => {
          const [techType, ...techValue] = split(tStr, ',').map(s => (s === 'null' ? null : Number(s)));
          return [techType, techValue];
        })
      );
      return [Number(matchId), techMap];
    })
  );
}

export {
  _formatRecMarketOdds,
  convertSportsSort,
  convertSportsHandicap,
  convertSportsDetailList,
  _formatMatchInfo,
  getSportNameById,
  _keyNameByBettype,
  _formatHomeMarketOdds
};
