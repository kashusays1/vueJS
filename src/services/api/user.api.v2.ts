import { v2_http } from '@/common';
import { HttpResponse_v2 } from '#/global';
import {
  BetOrderData,
  BetOrdersPrams,
  GameParms,
  LoginAccountInfo,
  PlatformBalance,
  UserInfo,
  UserLoginForm
} from '../types/user.interface.v2';

/**
 * 登录接口
 */
function signInApi(parm: UserLoginForm): Promise<HttpResponse_v2<LoginAccountInfo>> {
  return v2_http.post('/api/auth/sign_in', parm);
}

/**
 * 跳转游戏
 */
function jumpGameApi(parm: GameParms): Promise<HttpResponse_v2<string>> {
  return v2_http.get('/api/game/play', parm);
}

/**
 * captchaCode 验证码
 */
function captchaCode(params: any): Promise<HttpResponse_v2> {
  return v2_http.get('/api/captcha/code', params);
}

/**
 * 获取全平台游戏余额
 */
function getAllPlatformBalanceApi(): Promise<HttpResponse_v2<PlatformBalance>> {
  return v2_http.get('/api/game/queryAllPlatformBalance');
}

/**
 * 获取全平台游戏余额
 */
function freeTransferApi({ platformCode }: { platformCode: string }): Promise<HttpResponse_v2> {
  return v2_http.get('/api/game/freeTransfer', { platformCode });
}

/**
 * 刷新凭证token
 */
function refreshTokenApi({ refreshToken }: { refreshToken: string }): Promise<HttpResponse_v2<any>> {
  return v2_http.post('/api/auth/token/refresh', { refreshToken });
}

/**
 * 获取用户信息
 */
function getUserInfoApi(): Promise<HttpResponse_v2<UserInfo>> {
  return v2_http.get('/api/user/info');
}

/**
 * 获取用户信息
 */
function getDomainListApi(): Promise<HttpResponse_v2<string>> {
  return v2_http.get('/api/tenant/domain/list');
}

/**
 * 用户退出登录
 */
function uersSignOutApi(): Promise<HttpResponse_v2<string>> {
  return v2_http.post('/api/auth/logout');
}

/**
 * 获取体育注单
 */
const getOrders = (prams: BetOrdersPrams): Promise<HttpResponse_v2<BetOrderData>> => {
  return v2_http.get(`/api/bet/getSportBetRecords`, prams);
};

export {
  getOrders,
  signInApi,
  getUserInfoApi,
  uersSignOutApi,
  freeTransferApi,
  refreshTokenApi,
  getDomainListApi,
  captchaCode,
  jumpGameApi,
  getAllPlatformBalanceApi
};
