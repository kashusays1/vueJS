import SvgIcon from '@/components/svg-icon/index.vue';
import TeamLogo from '../components/team-logo/index.vue';
import SportIcon from '../components/SportIcon.vue';

export default [SvgIcon, TeamLogo, SportIcon];
