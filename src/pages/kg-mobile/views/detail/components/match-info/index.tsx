import { defineComponent, PropType, reactive, computed, ComputedRef } from 'vue';
import { TeamLogo } from 'kg-mobile/compoents';
import VideoView from './video';
import { sportBgs } from './assets/sport-bgs';
import { SvgIcon } from '@/components';
import infoStyle from './match-info.module.less';
import { useStore } from 'vuex';
import { SportIdEnum } from '@/common';
import { get } from 'lodash-es';
import { useI18n } from '@/hooks';

export default defineComponent({
  name: 'MatchInfo',
  props: {
    matchInfo: {
      type: Object as PropType<any>,
      default: () => ({
        score: [0, 0]
      })
    }
  },
  emits: ['openData', 'change'],
  setup(props, { emit }) {
    const vuexStore = useStore();
    const { t } = useI18n();
    const state = reactive({
      active: 'socore'
    });

    const tabTools: ComputedRef<
      {
        icon: string;
        visible: boolean;
      }[]
    > = computed(() => [
      {
        icon: 'socore',
        visible: true
      },
      {
        icon: 'video',
        visible: !!videoUrl.value
      },
      {
        icon: 'animation',
        visible: !!animationUrl.value
      },
      {
        icon: 'data',
        visible: props.matchInfo.sportId === SportIdEnum['Soccerball']
      }
    ]);

    // 视频链接
    const videoUrl = computed(() => {
      return props.matchInfo.id ? vuexStore.getters?.getVideoByMid(props.matchInfo.id) : '';
    });

    // 动画链接
    const animationUrl = computed(() => {
      return props.matchInfo.id ? vuexStore.getters?.getAnimateyByMid(props.matchInfo.id) : '';
    });

    const handleMoreIconChnage = (indexStr: string) => {
      if (state.active === indexStr) return;
      if (indexStr === 'data') {
        emit('openData');
        return;
      }
      const infoNode = document.getElementById('match-info');
      if (indexStr === 'video' || indexStr === 'animation') {
        infoNode?.classList.add(infoStyle.isVideoView);
      } else {
        infoNode?.classList.remove(infoStyle.isVideoView);
      }
      emit('change', indexStr);
      state.active = indexStr;
    };

    const VideoRender = () => <VideoView class={infoStyle.videoView} videoSource={videoUrl.value} />;

    const AnimteRender = () =>
      animationUrl.value ? (
        <div class={infoStyle.animateView}>
          <iframe src={animationUrl.value}></iframe>
        </div>
      ) : null;

    const IconsRender = () => (
      <div class={infoStyle.moreIcons}>
        {tabTools.value.map(item => (
          <div
            onClick={() => handleMoreIconChnage(item.icon)}
            key={item.icon}
            v-show={item.visible}
            class={[infoStyle.moreIconItem, state.active === item.icon ? infoStyle.active : null]}
          >
            <SvgIcon name={'event_' + item.icon} />
          </div>
        ))}
      </div>
    );

    const SoccerTechRender = () => (
      <div class={infoStyle.tech}>
        <span class={infoStyle.ht}>HT</span>
        <span class={infoStyle.techText}>
          {props.matchInfo.tech?.ht[0]}-{props.matchInfo.tech?.ht[1]}
        </span>
      </div>
    );

    const showMatchInfoTime = computed(() => {
      return [1, 2].includes(props.matchInfo.sportId);
    });
    const LiveRender = () => (
      <div class={infoStyle.centerInfo}>
        <div class={infoStyle.scoreText}>
          {props.matchInfo.score
            ? props.matchInfo.score.map((sc: any, index: number) => (
                <>
                  {sc}
                  {index === 0 ? ':' : null}
                </>
              ))
            : null}
        </div>
        {props.matchInfo.period && <div class={infoStyle.turn}>{t(props.matchInfo.period)}</div>}
        <div class={infoStyle.timeText}>{showMatchInfoTime.value ? props.matchInfo.time || '--' : ' '}</div>
        {props.matchInfo.id === SportIdEnum.Soccerball ? <SoccerTechRender /> : null}
        {props.matchInfo.isNeutral ? <SvgIcon class="neutral" name="neutral" /> : null}
      </div>
    );

    const NotStaredRender = () => (
      <div class={infoStyle.centerInfo}>
        <div class={infoStyle.turn}>未开始 {props.matchInfo.time || '--'}</div>
        <div class={infoStyle.timeText}>{props.matchInfo.date || '--'}</div>
        {props.matchInfo.isNeutral ? <SvgIcon class="neutral" name="neutral" /> : null}
      </div>
    );

    const InfoRender = () => (
      <>
        <div class={infoStyle.infoTop}>
          <div class={infoStyle.leftTeam}>
            <div class={infoStyle.teamName}>{props.matchInfo.home || '--'}</div>
            <TeamLogo
              class={infoStyle.homeTeamIcon}
              sportId={props.matchInfo.sportId}
              teamId={props.matchInfo.homeId}
            />
            <div class={infoStyle.cards}>
              {props.matchInfo.tech?.yellow[0] ? (
                <div class={infoStyle.yellowCard}>{get(props.matchInfo, 'tech.yellow[0]')}</div>
              ) : null}
              {props.matchInfo.tech?.red[0] ? (
                <div class={infoStyle.redCard}>{get(props.matchInfo, 'tech.red[0]')}</div>
              ) : null}
            </div>
          </div>
          {props.matchInfo.isLive ? <LiveRender /> : <NotStaredRender />}
          <div class={infoStyle.rightTeam}>
            <div class={infoStyle.teamName}>{props.matchInfo.away || '--'}</div>
            <TeamLogo
              class={infoStyle.awayTeamIcon}
              sportId={props.matchInfo.sportId}
              teamId={props.matchInfo.awayId}
            />
            <div class={infoStyle.cards}>
              {props.matchInfo.tech?.red[1] ? (
                <div class={infoStyle.redCard}>{get(props.matchInfo, 'tech.red[1]')}</div>
              ) : null}
              {props.matchInfo.tech?.yellow[1] ? (
                <div class={infoStyle.yellowCard}>{get(props.matchInfo, 'tech.yellow[1]')}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div class={infoStyle.infoBg}>
          <img src={(props.matchInfo.sportId && sportBgs[props.matchInfo.sportId]?.general) || sportBgs[999].general} />
        </div>
      </>
    );

    return () => (
      <div class={infoStyle.info} id="match-info">
        <div class={infoStyle.infoContainer}>
          {state.active === 'socore' && <InfoRender />}
          {state.active === 'video' ? <VideoRender /> : null}
          {state.active === 'animation' ? <AnimteRender /> : null}
          <IconsRender />
        </div>
      </div>
    );
  }
});
