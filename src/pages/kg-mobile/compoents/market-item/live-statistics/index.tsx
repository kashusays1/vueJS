/**
 * 滚球事件显示
 */
import { defineComponent, PropType } from 'vue';
import style from './live-statistics.module.less';
import { SvgIcon } from '@/components';

import { useSoccerScore } from './live-statistics';
import { StoreMatchInfo } from '@/hooks';
import { SportIdEnum } from '@/common';

export default defineComponent({
  name: 'LiveEvetsStatistics',
  props: {
    info: {
      type: Object as PropType<StoreMatchInfo>,
      default: () => ({})
    }
  },
  setup(props) {
    // 转换足球
    const { soccerTech } = useSoccerScore(props.info.id);

    const FootballRender = () => (
      <>
        {soccerTech.value.homeCorners && soccerTech.value.awayCorners ? (
          <>
            <SvgIcon name="corner" />
            <span class={style.EventVal}>
              {soccerTech.value.homeCorners}-{soccerTech.value.awayCorners}
            </span>
          </>
        ) : null}
        {soccerTech.value.homeHalfScore && soccerTech.value.awayHalfScore ? (
          <>
            <SvgIcon name="ht" />
            <span class={style.EventVal}>
              {soccerTech.value.homeHalfScore || props.info.tech?.ht[0]}-
              {soccerTech.value.awayHalfScore || props.info.tech?.ht[1]}
            </span>
          </>
        ) : null}
      </>
    );

    const OtherSocerRender = () => {
      const formatData = props.info.sections || [[0, 0]];
      return (
        <div class={style.OtherSocerEvents}>
          {formatData.map((item, index) => (
            <span
              v-show={index <= Number(props.info?.current)}
              class={[style.EventVal, props.info?.current === index ? style.Current : null]}
            >
              {item[0] || 0}-{item[1] || 0}
            </span>
          ))}
        </div>
      );
    };

    return () => (
      <div class={style.EvetsStatistics}>
        {props.info.sportId === SportIdEnum.Soccerball && props.info.isLive ? <FootballRender /> : <OtherSocerRender />}
      </div>
    );
  }
});
