import { App } from 'vue';
import eleList from './element';
import componentList from './comp';

export function initPlugin(app: App): void {
  [...eleList, ...componentList].forEach(component => app.component(component.name, component));
}
