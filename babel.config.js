// var TENANT = process.env.TENANT || "kgty";
// if(TENANT === 'kgty') {
//   TENANT = 'default';
// }

(module.exports = {
  "presets": ["@vue/cli-plugin-babel/preset"],
  "plugins": [
    [
      "import",
      {
        "libraryName": "vant",
        "libraryDirectory": "es",
        "style": true
      },
      "vant"
    ]
  ]
})
