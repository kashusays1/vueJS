import { HttpCode } from '@/common';

declare interface Fn<T = any, R = T> {
  (...arg: T[]): R;
}

declare interface PromiseFn<T = any, R = T> {
  (...arg: T[]): Promise<R>;
}

declare interface IObj<T = any> {
  [key: string]: T;
  [key: number]: T;
}

declare function parseInt(s: string | number, radix?: number): number;

declare function parseFloat(string: string | number): number;

declare type Nullable<T> = T | null;

declare type RefType<T> = T | null;

declare type CustomizedHTMLElement<T> = HTMLElement & T;

declare type Indexable<T extends any = any> = {
  [key: string]: T;
};

declare type Recordable<T extends any = any> = Record<string, T>;

declare type ReadonlyRecordable<T extends any = any> = {
  readonly [key: string]: T;
};

declare type Hash<T> = Indexable<T>;

declare type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

declare type LabelValueOptions = {
  label: string;
  value: any;
}[];

declare type EmitType = (event: string, ...args: any[]) => void;

declare type TargetContext = '_self' | '_blank';

declare type TimeoutHandle = ReturnType<typeof setTimeout>;

declare type IntervalHandle = ReturnType<typeof setInterval>;

declare interface ComponentElRef<T extends HTMLElement = HTMLDivElement> {
  $el: T;
}

declare type ComponentRef<T extends HTMLElement = HTMLDivElement> = ComponentElRef<T> | null;

declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>;

type IsSame<A, B> = A | B extends A & B ? true : false;

declare interface ChangeEvent extends Event {
  target: HTMLInputElement;
}

declare interface WheelEvent {
  path?: EventTarget[];
}

declare type HttpResponse<T = any> = [
  boolean,
  (
    | {
        code: HttpCode;
        message: string;
        data?: T;
      }
    | T
  )
];

declare type ResponseCode = 0 | 1 | 600 | 603 | 401 | 403 | 500 | 10005 | 20003;

declare type HttpResponse_v2<T = any, C = ResponseCode> = {
  code: C;
  data: T;
  message: string;
  timestamp?: string | number;
  path?: string;
  succeed: boolean;
  result: boolean;
};

declare type WsResponse<T = any> = {
  code?: HttpCode;
  message: string;
  data?: T;
  type?: string;
};
