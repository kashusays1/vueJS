import pako from 'pako';

// 压缩
export function zip(inStr: string) {
  const binaryString = pako.gzip(encodeURIComponent(inStr), { to: 'string' });
  return btoa(binaryString);
}
// 解压
export function unzip(inStr: string) {
  let strData = atob(inStr);
  const charData = strData.split('').map(x => x.charCodeAt(0));
  const binData = new Uint8Array(charData);
  const outData = pako.inflate(binData);
  strData = String.fromCharCode.apply(null, new Uint16Array(outData) as any);
  return decodeURIComponent(strData);
}
