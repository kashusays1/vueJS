import { defineComponent, computed, watch, onBeforeMount } from 'vue';
import { Progress } from 'vant';
import { get } from 'lodash-es';
import { useRoute } from 'vue-router';
import { useWindowScroll } from '@vueuse/core';

import { eventTimeFormat } from '@/common/utils';

import style from './progress.module.less';
import { useTimer } from './hooks/useTimer';

export default defineComponent({
  props: {
    store: {
      type: Object
    },
    fixed: {
      type: Boolean
    }
  },
  setup(props) {
    const { y } = useWindowScroll();
    const route = useRoute();
    const { getSecond, formatTime, getTime, start } = useTimer();
    const sportType = computed(() => {
      const match = get(props.store, 'raw.0');
      if (!match) return 0;
      return Number(match.SportType);
    });
    const isProgress = computed(() => {
      return [1, 2].includes(sportType.value);
    });
    const runTime = computed(() => {
      const time = get(props.store, 'info.time');
      return formatTime(getSecond(time) + getTime());
    });
    const pivotText = computed(() => {
      const data = get(props.store, 'raw.0.matches.0');
      const moreData = get(data, 'MoreInfo');

      return eventTimeFormat({
        SportType: sportType.value,
        InPlayTime: moreData.InPlayTime,
        RunTime: isProgress.value ? runTime.value : data.RunTime,
        TennisCurrentSet: moreData.TennisCurrentSet,
        LivePeriod: moreData.LivePeriod,
        LLP: moreData.LLP,
        IsHT: data.IsHT
      });
    });

    const percentage = computed(() => {
      try {
        const reg = /^\d{1,}:\d{2}$/;
        if (!reg.test(runTime.value)) return 0;
        const array = runTime.value.split(':');
        const time = +array[0] + +array[1] / 60;
        const total = time > 90 ? 120 : 90;
        return (time / total) * 100;
      } catch {
        return 0;
      }
    });

    const isVisible = computed(() => {
      return Number(route.query.periodId) == 3;
    });
    watch(
      () => isProgress.value,
      () => {
        if (isVisible.value && isProgress.value) start();
      }
    );
    onBeforeMount(stop);
    return () => {
      const divStyle = props.fixed
        ? { opacity: (y.value > 100 ? 100 : y.value) / 100, zIndex: y.value > 2 ? 20 : y.value }
        : {};
      return isVisible.value && sportType.value ? (
        <div class={[style.progress, props.fixed ? style.fixed : style.sticky]} style={divStyle}>
          {isProgress.value ? (
            <Progress
              percentage={percentage.value}
              stroke-width={'2px'}
              pivot-text={pivotText.value}
              color={'#BB3537'}
              track-color={'#9A9A9A'}
            />
          ) : (
            <div class={style.track}>
              <span class={style.text}>{pivotText.value}</span>
            </div>
          )}
        </div>
      ) : null;
    };
  }
});
