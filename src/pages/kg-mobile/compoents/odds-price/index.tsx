import { ref, watch, defineComponent } from 'vue';
import styleClass from './odds-price.module.less';

export default defineComponent({
  name: 'OddsPrice',
  props: {
    value: {
      type: Number,
      default: 0
    }
  },
  setup(props) {
    const floatClass = ref('');

    // 监听值
    watch(
      () => props.value,
      (newVal, oldVal) => {
        if (newVal > oldVal) {
          floatClass.value = styleClass.up;
        } else if (newVal < oldVal) {
          floatClass.value = styleClass.down;
        }
        setTimeout(() => {
          floatClass.value = '';
        }, 3 * 1000);
      }
    );

    return () => <span class={[floatClass.value, styleClass.oddsValue]}>{props.value}</span>;
  }
});
