import { defineComponent, computed, onMounted, watch, onBeforeUnmount } from 'vue';
import { Header, RecommendedMatch, MarketList, HandicapFilter } from 'kg-mobile/compoents';
import './home.style.less';

import { localMarketedMatchs, TimerEnum, useEventsAndMarkets, useTimerHook } from '@/hooks';
import { DataTypeEnum } from '@/common';
import { useRoute } from 'vue-router';

import { PullRefresh } from '@/components';

export default defineComponent({
  name: 'JBBsportHome',
  setup() {
    const { eventStore, getEventsAndMarkets } = useEventsAndMarkets();
    const { clearTimer } = useTimerHook();
    const route = useRoute();

    const periodId = computed(() => Number(route.query.periodId || 3));

    const onLeagueChange = async (ids: string) => {
      eventStore.form.leagueIds = ids;
      await getEventsAndMarkets();
    };

    const scrollTop = () => {
      const homeDom = document.getElementsByClassName('sport-home')[0];
      if (homeDom) {
        homeDom.scrollTop = 0;
      }
    };

    onMounted(async () => {
      // console.log('route.query', route.query);
      if (!route.query.sportId) return;
      eventStore.form = Object.assign(eventStore.form, route.query);
      await getEventsAndMarkets();
    });
    onBeforeUnmount(() => {
      clearTimer(TimerEnum.LIST);
    });

    watch(
      () => route.query,
      async (urlQuerys: { selectDate?: string; periodId: string; sportId: string }) => {
        console.log(urlQuerys, 'route change', route.path);
        if (!urlQuerys.sportId || route.path !== '/') return;
        const ids = localMarketedMatchs.value.get(Number(urlQuerys.sportId)) || [];
        if (Number(urlQuerys.periodId) === DataTypeEnum.Followed) {
          eventStore.form.periodId = DataTypeEnum.All;
          eventStore.form.eventIds = ids.join(',');
        } else {
          eventStore.form.periodId = Number(urlQuerys.periodId);
          eventStore.form.eventIds = '';
        }
        eventStore.form.sportId = Number(urlQuerys.sportId);
        eventStore.form.selectDate = urlQuerys.selectDate ? Number(urlQuerys.selectDate) : null;
        await getEventsAndMarkets();
        scrollTop();
      }
    );

    const onRefresh = async () => {
      await getEventsAndMarkets(true);
    };

    return () => (
      <div class="sport-home">
        <div style={{ height: periodId.value === DataTypeEnum.Early ? '5.10613rem' : '3.71957rem' }}> </div>
        <div class="home-sticky">
          <Header />
          <HandicapFilter />
        </div>
        <PullRefresh v-model={eventStore.loading} onRefresh={() => onRefresh()}>
          <RecommendedMatch
            periodId={periodId.value}
            v-show={[DataTypeEnum.Live, DataTypeEnum.Today].includes(periodId.value)}
          />
          <div class="home-container">
            <MarketList marketData={eventStore.marketList} onLeagueChange={(ids: string) => onLeagueChange(ids)} />
          </div>
        </PullRefresh>
      </div>
    );
  }
});
