import { defineComponent } from 'vue';
import { Icon } from 'vant';
import { SvgIcon } from '@/components';
import { useRouter } from 'vue-router';
import './header.style.less';

// 加了个参数, 依次 参数back默认回到上一页, 加backPath路由指定到某路由,emit('click-right') 自定义

export default defineComponent({
  name: 'MobileHeader',
  props: {
    shadow: {
      type: Boolean,
      default: false
    },
    /**
     * 左侧返回是否显示
     */
    isBack: {
      type: Boolean,
      default: true
    },
    /**
     * 标题
     */
    title: {
      type: String,
      default: ''
    },
    /**
     * 右边内容
     */
    right: {
      type: String,
      default: ''
    },
    /**
     * 固定置顶
     */
    fiexd: {
      type: Boolean,
      default: false
    },
    /**
     * 是否返回上一页
     */
    back: {
      type: Boolean,
      default: true
    },
    /**
     * 返回的路由
     */
    backPath: {
      type: String,
      default: ''
    },
    /**
     * 展示新图标
     */
    newIcon: {
      type: Boolean,
      default: false
    }
  },
  emits: ['click-left'],
  setup(props, { slots, emit }) {
    const router = useRouter();
    const onClickLeft = () => {
      if (props.backPath) {
        router.push(props.backPath);
        return;
      }
      emit('click-left');
    };
    return () => (
      <div class={['top-header', props.shadow ? 'shadow' : null]}>
        <div class={['top-header-content', props.fiexd ? 'fixed' : null]}>
          {slots.left ? (
            <div class={'top-header-left'}>{slots.left()}</div>
          ) : (
            <div class={'top-header-left'}>
              {props.newIcon ? (
                <div class={'top-header-left-icon'} onClick={() => (props.back ? router.go(-1) : onClickLeft())}>
                  <SvgIcon name="back" />
                </div>
              ) : (
                <Icon name="arrow-left" size="0.4rem" onClick={() => (props.back ? router.go(-1) : onClickLeft())} />
              )}
            </div>
          )}
          <div class={'top-header-center'}>{slots.title ? slots.title() : <span>{props.title}</span>}</div>
          <div class={'top-header-right'}>{slots.default?.() || slots.right?.()}</div>
        </div>
      </div>
    );
  }
});
