import { defineComponent, ref } from 'vue';
import './collapse.style.less';
// import { remove } from 'lodash-es';

export default defineComponent({
  name: 'CollapsePanel',
  setup(_props, { slots }) {
    const isExpand = ref(false);

    const handleClickToggle = () => {
      isExpand.value = !isExpand.value;
    };

    return () => (
      <div class={['collapse-panel', isExpand.value ? 'is-expand' : null]}>
        <div class="collapse-panel-header" onClick={() => handleClickToggle()}>
          <div class="expand-icon">
            <svg-icon name="arrow_bottom" />
          </div>
          <div class="collapse-panel-header-conetnt">{slots.header?.()}</div>
        </div>
        <div class="collapse-panel-content">{slots.default?.()}</div>
      </div>
    );
  }
});
