import type { App } from 'vue';
import { createStore, Store } from 'vuex';
import { Modules } from './types';

import bet from './modules/bet';
import sports from './modules/sports';
import sportsTech from './modules/sportTech';
import settings from './modules/settings';
import user from './modules/user';
import videoAndAnimation from './modules/videoAndAnimation';

const env = process.env;
const isDev = env.NODE_ENV === 'development';

const store: Store<Modules> = createStore({
  strict: isDev,
  modules: {
    bet,
    videoAndAnimation,
    sports,
    sportsTech,
    settings,
    user
  }
});

export function setupStore(app: App<Element>, maintains: string[] = []) {
  console.log(maintains);
  app.use(store);
}

export default store;
