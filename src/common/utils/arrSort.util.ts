export function dateSort(arr: any[]) {
  const data = arr.sort((a, b) => {
    const t1 = Date.parse(a.ShowTime);
    const t2 = Date.parse(b.ShowTime);
    return t1 - t2;
  });
  return data;
}

/**
 * 希尔排序
 * 核心：通过动态定义的 gap 来排序，先排序距离较远的元素，再逐渐递进
 * @param {*} arr
 * 来源 https://zhuanlan.zhihu.com/p/69201111?from_voters_page=true
 * 耗时：15ms
 */
export function shellSort(arr: any[], key: string) {
  const len = arr.length;
  let gap = Math.floor(len / 2);

  while (gap > 0) {
    // gap距离
    for (let i = gap; i < len; i++) {
      const temp = arr[i][key];
      let preIndex = i - gap;
      while (arr[preIndex][key] > temp) {
        arr[preIndex + gap][key] = arr[preIndex][key];
        preIndex -= gap;
      }
      arr[preIndex + gap][key] = temp;
    }
    gap = Math.floor(gap / 2);
  }

  return arr;
}
