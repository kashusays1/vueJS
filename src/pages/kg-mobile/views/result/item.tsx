import { defineComponent } from 'vue';
import { TeamLogo } from 'kg-mobile/compoents';
import './match-item.less';
import { toTime } from '@/common';
import { useI18n } from '@/hooks';
import { get } from 'lodash-es';

export default defineComponent({
  name: 'ResultItem',
  props: {
    info: {
      type: Object,
      default: () => ({})
    }
  },
  setup(props) {
    const { t } = useI18n();

    const _isHight = (a: number, b: number) => {
      if (a === 0 && b === 0) return false;
      return a >= b;
    };

    return () => (
      <div class="sports-match-item">
        <div class="left-info">
          <div class="time-header">
            <span class="match-time"> {toTime(props.info.startTime, 'MM-DD HH:mm') || '--'} </span>
          </div>
          <div class="team-name">
            <span class="item">
              <div class="itemH">
                <TeamLogo class="t-logo" sportId={props.info.gameId} teamId={props.info.home} />
                <span>{props.info.home || '--'}</span>
              </div>
            </span>
            <span class="item">
              <div class="itemA">
                <TeamLogo class="t-logo" sportId={props.info.gameId} teamId={props.info.away} />
                <span>{props.info.away || '--'}</span>
              </div>
            </span>
          </div>
        </div>
        <div class="right-info">
          <div class="time-header">
            <div class="match-info-title">
              {get(props.info, 'gameId') === '1' ? (
                <span class="ht-title">{t('lang.sport_earlyHandicap_halfTime')}</span>
              ) : null}
              <span class="match-time">完场</span>
            </div>
          </div>
          <div class="match-item-content">
            <div class="score-right">
              {get(props.info, 'gameId') === '1' ? (
                <div class="score">
                  <span>{props.info.homeHalf || '0'}</span>
                  <span>{props.info.awayScore || '0'}</span>
                </div>
              ) : null}
              <div class="score">
                <span>{props.info.awayHalf || '0'}</span>
                <span>{props.info.homeScore || '0'}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
