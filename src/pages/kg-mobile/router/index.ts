import { Fn } from '#/global';
import { createRouter, createWebHistory, RouteLocationNormalized, Router } from 'vue-router';
import { RouteEnum } from './router.enum';
const showDevRoutes = process.env.NODE_ENV === 'development';

const devRoute = [
  {
    path: RouteEnum.LOGIN,
    name: RouteEnum.LOGIN,
    component: () => import(/* webpackChunkName: "login_page" */ '@/components/auth/login_v2.vue'),
    meta: {
      title: 'Login'
    }
  },
  {
    path: RouteEnum.DEMO,
    name: RouteEnum.DEMO,
    component: () => import(/* webpackChunkName: "demo_page" */ 'kg-mobile/views/demo/demo'),
    meta: {
      title: 'Demo'
    }
  }
];

const mainRoutes = [
  {
    path: RouteEnum.ROOT,
    name: RouteEnum.ROOT,
    component: () => import(/* webpackChunkName: "home_page" */ 'kg-mobile/views/home/index'),
    meta: {
      title: 'Home'
    }
  },
  {
    path: RouteEnum.SETTING,
    name: RouteEnum.SETTING,
    component: () => import(/* webpackChunkName: "settting_page" */ 'kg-mobile/views/setting/index'),
    meta: {
      title: 'Options'
    }
  },
  {
    path: RouteEnum.RESULT,
    name: RouteEnum.RESULT,
    component: () => import(/* webpackChunkName: "result_page" */ 'kg-mobile/views/result/index'),
    meta: {
      title: 'Result'
    }
  },
  {
    path: RouteEnum.SPORT_DETAIL,
    name: 'Detail',
    component: () => import(/* webpackChunkName: "detail_page" */ 'kg-mobile/views/detail/index'),
    beforeEnter: (to: any, _from: any, next: () => void) => {
      if (!to.query.matchId) {
        console.error('缺少 比赛Id');
        return;
      }
      next();
    },
    meta: {
      title: 'Detail'
    }
  },
  // {
  //   path: RouteEnum.SEARCH,
  //   name: RouteEnum.SEARCH,
  //   component: () => import('../views/search/index'),
  //   meta: {
  //     title: 'Search'
  //   }
  // },
  {
    path: RouteEnum.NOTICE,
    name: RouteEnum.NOTICE,
    component: () => import('kg-mobile/views/notice/index'),
    meta: {
      title: 'Notice',
      keepAlive: false
    }
  },
  {
    path: RouteEnum.ORDERS,
    name: RouteEnum.ORDERS,
    component: () => import('kg-mobile/views/orders/index.vue'),
    meta: {
      title: 'Orders'
    }
  }
];

export const routes = showDevRoutes ? [...mainRoutes, ...devRoute] : mainRoutes;

function registerRouter(router: any) {
  router.beforeEach((_to: { path: RouteEnum }, _from: RouteLocationNormalized, next: Fn) => {
    next();
  });
}

export function initRouter(): Router {
  const router = createRouter({
    history: createWebHistory('/'),
    routes,
    strict: true,
    scrollBehavior: () => ({ left: 0, top: 0 })
  });
  registerRouter(router);
  return router;
}
