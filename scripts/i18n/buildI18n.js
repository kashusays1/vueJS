const { log, spinner, getI18nJson, getSystemConfig } = require('../server/index');
const pMap = require('p-map');
const path = require('upath');
const fs = require('fs-extra');

// TODO 对应租户语言列表
const defaultLangList = ['zh-CN', 'en', 'th', 'vi', 'ja', 'ko', 'my', 'tr', 'ar', 'es', 'pt', 'zh-TW', 'id', 'ms'];

// 路径
const i18nPath = path.join('./src/i18n', 'lang');

const creatI18nFile = async function (app) {
  try {
    let langList = [];
    let isLoad = false;
    log.info(`拉取系统配置`);
    const systemConfig = await getSystemConfig();
    if (systemConfig.status === 200 && systemConfig.data.code === 0) {
      let { data } = systemConfig.data;
      if (data.langueList?.length) {
        isLoad = true;
        langList = data.langueList.map(item => item.seriNo);
        log.info(`国际化语言配置表:${langList.join(',')}`);
        // 创建写入支持的语言列表
        log.info(`创建写入支持的语言列表:`);
        const langListPath = path.join(i18nPath, 'langConfig.json');
        await fs.writeJson(langListPath, data.langueList, { spaces: 2 });
      }
    }
    spinner.succeed(`拉取系统配置完成`);
    if (!isLoad && app) {
      let tenantsConfig = tenants.find(item => item.name === app);
      if (tenantsConfig && Array.isArray(tenantsConfig.defaultLangList)) {
        langList = tenantsConfig.defaultLangList;
      }
    }

    if (!langList.length) {
      langList = defaultLangList;
    }
    await fs.ensureDir(i18nPath);
    for (let lang of langList) {
      let langData = [];
      const { status, data } = await getI18nJson(lang, 'sport');
      if (status === 200 && data.code === 0) {
        langData = [].concat(langData, data.data);
      }
      spinner.succeed(`拉取模块${lang}国际化json完成,正在解析`);
      const langJson = {};
      const insterI18nJson = item => {
        langJson[item.seriNo] = item.info;
      };
      await pMap(langData, insterI18nJson, { concurrency: 5 });
      const filePath = path.join(i18nPath, `${lang}.json`);
      await fs.writeJson(filePath, langJson, { spaces: 2 });
      log.info(`${lang}国际化json解析写入完成`);
    }
  } catch (e) {
    log.error('国际化资源拉取失败' + e);
  }
};

module.exports = {
  creatI18nFile
};
