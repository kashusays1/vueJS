const { join } = require('path');
const path = require('path'); //引入path模块
const autoprefixer = require('autoprefixer');
const postcssPxtorem = require('postcss-pxtorem');
const resolve = dir => path.resolve(__dirname, dir);
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const { BannerPlugin } = require('webpack');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const env = process.env;
const isDev = env.NODE_ENV === 'development';
const IS_PROD = ['prod', 'production'].includes(process.env.NODE_ENV);
const SERVER_URL = process.env.VUE_APP_SERVER_IP;
const project = process.env.VUE_PAGE;
// const isIm = project === 'im';
// const isJbb = project === 'jbb';

const { name } = require('./package');
const dayjs = require('dayjs');

const pxtorem = postcssPxtorem({
  rootValue: 37.5,
  propList: ['*']
});

// const demo = {
//   entry: resolve('./src/pages/demo/main.ts'),
//   filename: 'demo.html'
// };

// TODO 生成页面 展示只生成jbb-mobile
function gneratePageEntry() {
  console.log('Sport Name:', project);
  let appPages = {};
  const mobilePath = `./src/pages/${project}-mobile/main.ts`;
  const webPath = `./src/pages/${project}-web/main.ts`;
  appPages[project + '-mobile'] = {
    entry: resolve(mobilePath),
    filename: 'index.html'
  };
  appPages[project + '-web'] = {
    entry: resolve(webPath),
    filename: 'web.html'
  };
  return appPages;
}

module.exports = {
  pages: {
    // 'sb-mobile': {
    //   entry: resolve('./src/pages/sb-mobile/main.ts'),
    //   filename: isIm || isJbb ? 'sb-mobile.html' : 'index.html'
    // },
    // 'sb-web': {
    //   entry: resolve('./src/pages/sb-web/main.ts'),
    //   filename: isIm || isJbb ? 'sb-web.html' : 'web.html'
    // },
    // 'im-mobile': {
    //   entry: resolve('./src/pages/im-mobile/main.ts'),
    //   filename: isIm && !isJbb ? 'index.html' : 'sb-index.html'
    // },
    // 'im-web': {
    //   entry: resolve('./src/pages/im-web/main.ts'),
    //   filename: isIm && !isJbb ? 'web.html' : 'im-web.html'
    // },
    // 'jbb-mobile': {
    //   entry: resolve('./src/pages/jbb-mobile/main.ts'),
    //   filename: 'index.html'
    // },
    ...gneratePageEntry()
    // demo
  },
  outputDir: 'sport_bet_' + project,

  css: {
    extract: IS_PROD,
    sourceMap: isDev,
    requireModuleExtension: true,
    loaderOptions: {
      less: {
        globalVars: {
          // hack: `true; @import '${join(__dirname, './src/style/var.less')}'`
          hack: `true; @import '${join(__dirname, `./src/pages/${project}-mobile/style/themes/index.less`)}'`
        },
        javascriptEnabled: true
      },
      css: {
        modules: {
          auto: true,
          localIdentName: '[local]-[hash:base64:5]'
        }
      },
      postcss: {
        plugins: function ({ resourcePath: path }) {
          const isH5 = path.includes('mobile');
          if (isH5) {
            if (/[\\/]node_modules[\\/].+\.css$/.test(path) || isDev) {
              return [pxtorem];
            }
            return [pxtorem, autoprefixer];
          } else if (!isDev) {
            return [autoprefixer];
          }
          return [autoprefixer];
        }
      }
    }
  },
  configureWebpack: config => {
    config.output.library = `${name}-[name]`;
    config.output.libraryTarget = 'umd';
    config.output.jsonpFunction = `webpackJsonp_${name}`;

    config.module.rules.push({
      test: /\.webp$/i,
      use: ['file-loader']
    });
    const vendorsTest = module => {
      const { resource: path } = module;
      if (!path) return false;
      if (
        /[\\/]node_modules[\\/]vant[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]hls.js[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]@vueuse[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]dayjs[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]dplayer[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]lodash[\\/]/.test(path) ||
        /[\\/]node_modules[\\/]lodash-es[\\/]/.test(path)
      ) {
        return false;
      }
      return /[\\/]node_modules[\\/]/.test(path);
    };
    if (!isDev) {
      config.optimization.splitChunks = {
        chunks: 'async',
        automaticNameDelimiter: '_',
        maxAsyncRequests: 6,
        maxInitialRequests: 4,
        cacheGroups: {
          common: {
            test: /[\\/]node_modules[\\/]/,
            name: 'chunk-commons',
            chunks: 'initial',
            reuseExistingChunk: true,
            enforce: true,
            priority: 1,
            minChunks: 4
          },
          vendors: {
            name: 'chunk-vendors',
            test: vendorsTest,
            chunks: 'initial',
            priority: 2,
            enforce: true,
            reuseExistingChunk: true
          },
          styles: {
            name: 'chunk-styles', // 将多个入口文件中的样式文件全部合并打包
            test: module => {
              const { resource } = module;
              if (/\.(sa|sc|c)ss$/.test(resource)) {
                return true;
              }
              return false;
            },
            chunks: 'all',
            priority: -30
          }
        }
      };
    }
  },
  chainWebpack: config => {
    // 移除 prefetch 插件
    config.plugins.delete('prefetch');
    config.resolve.alias
      .set('@', resolve('./src'))
      .set('hooks', resolve('./src/hooks'))
      .set('im-mobile', resolve('./src/pages/im-mobile'))
      .set('im-web', resolve('./src/pages/im-web'))
      .set('jbb-mobile', resolve('./src/pages/jbb-mobile'))
      .set('kg-mobile', resolve('./src/pages/kg-mobile'))
      .set('h5', resolve('./src/pages/sb-mobile'))
      .set('web', resolve('./src/pages/sb-web'))
      .set('components', resolve('./src/components'));
    config.resolve.symlinks(true);
    // svg配置
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.exclude.add(/node_modules/);
    svgRule
      .test(/\.svg$/)
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      });

    const imagesRule = config.module.rule('images');
    imagesRule.exclude.add(resolve(`src/pages/${project}-mobile/icons`));
    imagesRule.exclude.add(resolve(`src/pages/${project}-web/icons`));
    config.module.rule('images').test(/\.(png|jpe?g|gif|svg)(\?.*)?$/);
    config.module
      .rule('less')
      .oneOf('vant')
      .before('vue-modules')
      .test(/[\\/]node_modules[\\/]vant[\\/]/);
    // config.resolve.alias.set('web', resolve('./src/web')).set('h5', resolve('./src/h5'));
    // config.plugin('analyzer').use(BundleAnalyzerPlugin);
    if (!isDev) {
      config.optimization.minimize(true);
      config
        .plugin('compression')
        .use(CompressionWebpackPlugin)
        .tap(() => {
          return [
            {
              test: new RegExp('\\.(js|css)$'),
              threshold: 10240,
              deleteOriginalAssets: false
            }
          ];
        });
      config
        .plugin('banner')
        .use(BannerPlugin)
        .tap(() => {
          return [
            {
              banner: dayjs().format('YYYY/MM/DD HH:mm:ss')
            }
          ];
        });
      config.module
        .rule('images')
        .test(/\.(gif|png|jpe?g|svg)$/i)
        .use('image-webpack-loader')
        .loader('image-webpack-loader')
        .end();
    }

    // config.plugin('define').tap(args => {
    //   return [
    //     {
    //       'process.env': {
    //         ...args[0]['process.env'],
    //         SCOPE: JSON.stringify(process.env.SCOPE),
    //         APP_PATH: JSON.stringify(`@/apps/${process.env.SCOPE}/app.vue`),
    //         ROUTER_PATH: JSON.stringify(`@/router/${process.env.SCOPE}.ts`),
    //         PLUGIN_PATH: JSON.stringify(`@/apps/${process.env.SCOPE}/plugins/index.ts`)
    //       }
    //     }
    //   ];
    // });
  },

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [path.resolve(__dirname, './src/style/mixin.less')]
    }
  },
  devServer: {
    // overlay: { // 让浏览器 overlay 同时显示警告和错误
    //   warnings: true,
    //   errors: true
    // },
    open: true, // 是否打开浏览器
    host: 'localhost',
    https: false,
    hotOnly: true, // 热更新
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    proxy: {
      '/api': {
        target: SERVER_URL, // 目标代理接口地址
        secure: false,
        changeOrigin: true, // 开启代理，在本地创建一个虚拟服务端
        ws: true // 是否启用websockets
      },
      '/api-auth': {
        target: SERVER_URL, // 目标代理接口地址
        secure: false,
        changeOrigin: true // 开启代理，在本地创建一个虚拟服务端
      }
    }
  }
};
