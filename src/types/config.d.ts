/*
 * @Description: 全局配置类型
 */

// 体育数据源标识
export type SourceType = 'sb' | 'bti' | 'im' | 'shaba';
