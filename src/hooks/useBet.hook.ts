import { Toast } from 'vant';
import { useStore } from 'vuex';
/**
 * 投注
 */
import { TimerEnum, useAuth, useBlance, useTimerHook, useI18n, useSportOptins } from '@/hooks';
import { isEmpty, keys, map, values, forEach, min, max, sumBy, remove, every, some } from 'lodash-es';
import { computed, reactive, ref, Ref } from 'vue';
import { RemovableRef, useStorage } from '@vueuse/core';
import { getParlayApi, doBetApi } from '@/services/kg-api';
import { getLoginData, MarketType, MarketTypeList } from '@/common';

export const BET_TICKET_KEY = 'SP_BET_TICKETS';
// const BET_LIMIT_KEY = 'SP_BET_LIMIT_TICKETS';

// 投注参数列表
const betTickets: RemovableRef<any> = useStorage(BET_TICKET_KEY, {});

const betLimit: Ref<any> = ref({});
interface BetState {
  isShowPopup: boolean;
  typeTabs: string[];
  eachAmount: string | number;
  tabActive: number;
  isCheckedAll: boolean;
  checkedAll: number[];
  showParylayType: boolean;
  parylayTypeResult: string;
  parlayColumns: string[];
  betted: boolean;
  betParlyCombo: Map<string, any>;
  currentCombo: any;
  loading: boolean;
  betResultList: any[];
  betComoResult: any[];
}
const initState = () => ({
  isShowPopup: false,
  typeTabs: ['lang.sport_bet_singBets', 'lang.sport_bet_stray', 'lang.sport_bet_duplex'],
  eachAmount: '',
  tabActive: 0,
  isCheckedAll: true,
  checkedAll: [],
  showParylayType: false,
  parylayTypeResult: '',
  parlayColumns: [],
  betted: false,
  betParlyCombo: new Map(),
  currentCombo: {},
  loading: false,
  betResultList: [],
  betComoResult: []
});
const state: BetState = reactive(initState());
const { jumpToken } = getLoginData();

// 检查是否有投注参数id
export const hasTicketId = ({ matchId, market_id, key }: { matchId: number; market_id: number; key: string }) => {
  const hasMatch = betTickets.value[matchId];
  if (hasMatch && jumpToken) {
    return !!betTickets.value[matchId][`${market_id}_${key}`];
  }
  return false;
};

export interface BetParam {
  bettype: number;
  matchId: number;
  market_id: number;
  sport_type: number;
  odds_type: number;
  wagerSelectionId: number;
  key: string;
  price: number;
  periodId: number;
  point: string;
}

export interface BetInfoParam {
  keyName: string;
  home: string;
  away: string;
  combo: number;
  leagueName: string;
  betTypeName: string;
  market: string;
}

export interface BactchParam {
  price: number;
  point: string;
}

export function useBet() {
  const { checkIsLogin } = useAuth();
  const { t } = useI18n();
  const { optionsStore } = useSportOptins();
  const { sportBalance } = useBlance({
    platformCode: 'im'
  });
  const { addTimer, clearTimer } = useTimerHook();
  const store = useStore();
  const currencySymbol = computed(() => store.state.user.currency);
  const checdkInfo = computed(() => prebetInfo.value.filter(p => state.checkedAll.includes(p.uniqKey)) || []);
  const betAmount = computed(() => {
    return sumBy(checdkInfo.value, e => Number(e.stake || 0)) || '';
  });
  const winAmount = computed(() => {
    return Number(sumBy(checdkInfo.value, e => Number(e.stake || 0) * e.estimatedPayoutAmount).toFixed(2)) || '';
  });
  const openTickets = () => {
    state.isShowPopup = true;
    handleCheckAll();
    getData();
  };
  const handleClosePopup = () => {
    state.loading = false;
    if (state.betted) {
      handleDoneBet();
    }
  };
  const handleCheckAll = () => {
    state.isCheckedAll = true;
    state.checkedAll = prebetInfo.value.map(p => p.uniqKey);
  };
  const handleBranchChange = (val: number) => {
    state.eachAmount = val;
    prebetInfo.value.forEach(e => {
      if (state.checkedAll.includes(e.uniqKey)) {
        betLimit.value[e.uniqKey].stake = val;
      }
    });
  };
  const showHandicapName = (oddsType: MarketType) => {
    return t(MarketTypeList.find(m => Number(oddsType) === m.value)!.label);
  };
  const needRecharge = computed(() => {
    if (state.tabActive === 0) {
      return betAmount.value > sportBalance.value;
    } else if (state.tabActive === 1) {
      const current = Array.from(state.betParlyCombo.values()).find(b => b.bet_count === 1);
      return current && current.stake > sportBalance.value;
    } else {
      const current = state.betParlyCombo.get(state.parylayTypeResult);
      return current && current.stake * current.bet_count > sportBalance.value;
    }
  });
  const branchLimit = computed(() => {
    const low = max(map(checdkInfo.value, c => c.min_bet)) || 0;
    const up = min(map(checdkInfo.value, c => c.max_bet)) || 0;
    return [low, up];
  });
  /**
   * 添加投注信息
   */
  const addTicket = async (oSel: BetParam & BetInfoParam & BactchParam) => {
    const sel = { ...oSel, sort: prebetInfo.value.length + 1, uniqKey: `${oSel.market_id}_${oSel.key}` };
    if (!checkIsLogin()) {
      return;
    }
    console.log('PPrice:', sel.price);
    console.log('PPoint:', sel.point);
    // 判断当前比赛
    if (betTickets.value[sel.matchId]) {
      const match = betTickets.value[sel.matchId];
      // 相同盘口
      if (match[sel.uniqKey]) {
        delete match[sel.uniqKey];
        isEmpty(match) && delete betTickets.value[sel.matchId];
      } else {
        if (prebetInfo.value.length >= 10) {
          Toast('投注项数量已达上限（10项）');
          return;
        }
        match[sel.uniqKey] = sel;
      }
    } else {
      if (prebetInfo.value.length >= 10) {
        Toast('投注项数量已达上限（10项）');
        return;
      }
      betTickets.value[sel.matchId] = {
        [sel.uniqKey]: sel
      };
      if (Object.keys(betTickets.value).length === 1) {
        state.isShowPopup = true;
        handleCheckAll();
      }
    }
    getData();
  };

  const getData = () => {
    const fn = () => {
      if (state.tabActive === 0) {
        getSingleData();
      } else {
        getPalaryData();
      }
    };
    addTimer(TimerEnum.PREBET, fn, 3000);
  };

  const onTabChange = (index: number) => {
    if (index === state.tabActive) return;
    state.tabActive = index;
    getData();
  };

  const _formatSelParam = (sel: BetParam & BetInfoParam) => ({
    beType: sel.bettype,
    matchId: sel.matchId,
    marketId: sel.market_id,
    sportId: sel.sport_type,
    oddsId: optionsStore.value.marketType,
    wagerSelectionId: sel.wagerSelectionId, //Not Exist
    key: sel.key, // Exist in header
    price: sel.price,
    periodId: sel.periodId, // NOt exist
    point: sel.point
  });

  const getSingleData = async () => {
    const list: any[] = [];
    forEach(betTickets.value, value => {
      values(value).forEach(v => {
        list.push(v);
        if (!betLimit.value[v.uniqKey]) {
          betLimit.value[v.uniqKey] = {
            max_bet: 0,
            min_bet: 0,
            stake: '',
            closed: false,
            sort: v.sort
          };
        }
      });
    });
    interface bData {
      betData: {
        sort: number;
        sportId: number; //4
        matchId: number; //2
        marketId: string; //3
        point: string; //6
        price: number; //5
        betTeam: string;
        oddsId: number | null; //7
        betType: number; //1
        marketType: string;
      };
    }

    const single: bData = {
      betData: {
        sort: 1,
        sportId: 2, //4
        matchId: 54009224, //2
        marketId: '434565613', //3
        point: '155', //6
        price: 1.91, //5
        betTeam: 'h',
        oddsId: null, //7
        betType: 3, //1
        marketType: '2'
      }
    };
    /*
        const single = list.map(sel => ({
      singleTicketInfo: _formatSelParam(sel),
      sort: sel.sort,
      OpenParlay: false,
      palaryInfoArray: null
    }));`
*/
    // sort
    console.log('LLLL');
    console.log(single);
    console.log('MMMMMMMM121212');
    //const [res, data] = await getParlayApi(single);
    const [res, data] = await getParlayApi();
    // 更新限额以及赔率

    if (res) {
      data.forEach((v: any) => {
        if (v.code === 1) {
          const uniqKey = `${v.market_id}_${v.key}`;
          betLimit.value[uniqKey] = {
            ...v,
            max_bet: Number(v.max_bet) || 0,
            min_bet: Number(v.min_bet) || 0,
            stake: betLimit.value[uniqKey]?.stake || ''
          };
          if (betTickets.value[v.matchId] && betTickets.value[v.matchId][uniqKey]) {
            betTickets.value[v.matchId][uniqKey].price = v.price;
          }
        } else {
          forEach(betLimit.value, (val, key) => {
            if (Number(v.sort) === Number(val.sort)) {
              betLimit.value[key] = {
                ...betLimit.value[key],
                min_bet: 0,
                max_bet: 0,
                message: v.message,
                closed: true
              };
              remove(state.checkedAll, key);
            }
          });
        }
      });
    }
  };

  const getPalaryData = async () => {
    const list: any[] = [];
    forEach(betTickets.value, value => {
      values(value).forEach(v => {
        list.push(_formatSelParam(v));
        const { max_bet = 0, min_bet = 0, stake = '' } = betLimit.value[v.uniqKey] || {};
        betLimit.value[v.uniqKey] = {
          max_bet: max_bet || 0,
          min_bet: min_bet || 0,
          stake: stake || ''
        };
      });
    });
    if (list.length < 2) return;
    /*
    const palary = [
      {
        singleTicketInfo: null,
        sort: `${list.length + 1}`,
        OpenParlay: true,
        palaryInfoArray: list
      }
    ];*/

    const [_res, [data]] = await getParlayApi();
    const { combos = [], price_info = [] } = data;
    price_info.forEach(v => {
      const uniqKey = `${v.market_id}_${v.key}`;
      betLimit.value[uniqKey] = {
        ...betLimit.value[uniqKey],
        ...v
      };
      if (betTickets.value[v.matchId] && betTickets.value[v.matchId][uniqKey]) {
        betTickets.value[v.matchId][uniqKey].price = v.current_price;
      }
    });
    const temp: any[] = [];
    combos.forEach(c => {
      const old = state.betParlyCombo.get(mulTypeToName(c.combo_type)) || { stake: '' };
      temp.push({
        ...c,
        ...old,
        mulType: mulTypeToName(c.combo_type)
      });
    });
    state.parlayColumns = temp.filter(bet => bet.bet_count !== 1).map(b => mulTypeToName(b.combo_type));
    if (state.parylayTypeResult === '') {
      state.parylayTypeResult = state.parlayColumns[0];
      state.currentCombo = temp.find(bet => bet.bet_count !== 1) || {};
    }
    state.betParlyCombo = new Map(temp.map(t => [t.mulType, t]));
  };

  const prebetInfo = computed(() => {
    const list: any[] = [];
    forEach(betTickets.value, e => {
      const isSameGame: boolean = keys(e).length > 1;
      forEach(e, (val: any) => {
        list.push({ ...val, isSameGame, ...betLimit.value[val.uniqKey] });
      });
    });
    return list;
  });

  const doSingleBet = async () => {
    const single = prebetInfo.value.map(p => ({
      OpenParlay: false,
      sort: p.sort,
      bettype: p.bettype,
      customerIP: '127.0.0.1',
      isComboAcceptAnyOdds: false,
      key: p.key,
      live_away_score: p.live_away_score,
      live_home_score: p.live_home_score,
      market: p.market,
      matchId: p.matchId,
      marketId: p.market_id,
      oddsType: p.odds_type,
      outrightTeamId: p.outrightTeamId,
      point: p.point,
      price: p.price,
      sportType: p.sport_type,
      stake: p.stake,
      wagerSelectionId: p.wagerSelectionId
    }));
    const [res, data] = await doBetApi(single);
    state.loading = false;

    if (res) {
      state.betted = true;
      state.betResultList = data.map(d => ({
        sort: d.sort,
        isSuccess: d.code === 1,
        betStatus: d.BetStatus,
        price: d.BetPrice,
        winAmount: d.EstimatedPayoutFullAmount,
        ticketStatus: d.TicketStatus
      }));
    }
  };

  const doParlayBet = async (current: any) => {
    const arr: any[] = [current];
    // if (isAll) {
    //   state.betParlyCombo.forEach(value => {
    //     if (value.bet_count === 1) {
    //       arr.push(value);
    //       return;
    //     }
    //   });
    // } else {
    //   arr.push(state.betParlyCombo.get(state.parylayTypeResult));
    // }

    const matches = prebetInfo.value.map(p => ({
      bettype: p.bettype,
      isComboAcceptAnyOdds: true,
      key: p.key,
      live_away_score: p.live_away_score,
      live_home_score: p.live_home_score,
      market: p.market,
      matchId: p.matchId,
      marketId: p.market_id,
      market_id: p.market_id,
      oddsType: p.odds_type,
      outrightTeamId: p.outrightTeamId,
      point: p.point,
      price: p.price,
      sportType: p.sport_type,
      sport_type: p.sport_type,
      wagerSelectionId: p.wagerSelectionId
    }));
    const [res, [data]] = await doBetApi([
      {
        OpenParlay: true,
        parlay_odds_option: true,
        sort: '0',
        bet_matches: matches,
        customerIP: '127.0.0.1',
        bet_combos: arr.map(a => ({
          combo_type: a.comboSelection,
          stake: a.stake,
          bet_count: a.bet_count
        }))
      }
    ]);
    state.loading = false;
    if (res) {
      state.betComoResult = data.current_combos.map(d => ({
        isSuccess: d.code === 1,
        betStatus: d.BetStatus,
        winAmount: d.EstimatedPayoutFullAmount,
        ticketStatus: d.TicketStatus,
        betCount: d.bet_count,
        comboPrice: d.combo_price,
        comboType: mulTypeToName(d.combo_type),
        stake: d.stake
      }));
      state.betted = true;
      // state.betResultList = data.map(d => ({
      //   sort: d.sort,
      //   isSuccess: d.code === 1,
      //   betStatus: d.BetStatus,
      //   winAmount: d.EstimatedPayoutFullAmount,
      //   ticketStatus: d.TicketStatus
      // }));
    }
  };

  const handleBet = () => {
    clearTimer(TimerEnum.PREBET);
    if (state.loading) return;
    if (state.tabActive === 0) {
      // 校验单投
      if (every(checdkInfo.value, c => c.stake === '')) {
        return;
      }
      if (some(checdkInfo.value, c => !c.closed && c.price > 0 && c.stake > 0 && c.stake < c.min_bet)) {
        return;
      }
      state.loading = true;
      if (state.checkedAll.length > 0) {
        doSingleBet();
      }
    } else {
      // 是否全串
      const isAll = state.tabActive === 1;
      let current: any = null;
      if (isAll) {
        state.betParlyCombo.forEach(value => {
          if (value.bet_count === 1) {
            current = value;
            return;
          }
        });
      } else {
        current = state.betParlyCombo.get(state.parylayTypeResult);
      }
      // 串关
      if (Number(current.stake) >= Number(current.min_bet) && Number(current.stake) <= Number(current.max_bet)) {
        state.loading = true;
        doParlayBet(current);
      }
    }
  };

  // 清除
  const clearAllTickets = () => {
    clearTimer(TimerEnum.PREBET);
    handleDoneBet();
  };

  const deleteTicket = ({ matchId, market_id, key }: { matchId: number; market_id: number; key: string }) => {
    const match = betTickets.value[matchId];
    // 相同盘口
    delete match[`${market_id}_${key}`];
    isEmpty(match) && delete betTickets.value[matchId];
    if (isEmpty(betTickets.value)) {
      clearTimer(TimerEnum.PREBET);
      handleDoneBet();
    }
    state.betParlyCombo = new Map();
    getData();
  };

  const ticketSize = computed(() => {
    let size = 0;
    forEach(betTickets.value, value => {
      size += keys(value).length;
    });
    return size;
  });

  const handleDoneBet = () => {
    betTickets.value = {};
    betLimit.value = {};
    const newState = initState();
    Object.keys(state).forEach(key => {
      state[key] = newState[key];
    });
  };

  const handleToRecharge = () => {
    const backUrl = localStorage.getItem('backUrl');
    if (!backUrl) return;
    window.location.href = `${backUrl}/m/recharge`;
  };

  const mulTypeToName = (mulType: string) => {
    switch (mulType) {
      case 'Doubles':
        return `2${t('lang.sport_bet_connect')}1`;
      case 'Trebles':
        return `3${t('lang.sport_bet_connect')}1`;
      case 'Trixie':
        return `3${t('lang.sport_bet_connect')}4`;
      case 'Lucky7':
        return `幸运7`;
      case 'Fold4':
        return `4${t('lang.sport_bet_connect')}1`;
      case 'Yankee':
        return `洋基`;
      case 'Lucky15':
        return `幸运15`;
      case 'Fold5':
        return `5${t('lang.sport_bet_connect')}1`;
      case 'Canadian':
        return `超级美国佬`;
      case 'Lucky31':
        return `幸运31`;
      case 'Fold6':
        return `6${t('lang.sport_bet_connect')}1`;
      case 'Heinz':
        return `亨氏`;
      case 'Lucky63':
        return `幸运63`;
      case 'Fold7':
        return `7${t('lang.sport_bet_connect')}1`;
      case 'SuperHeinz':
        return `超级亨氏`;
      case 'Lucky127':
        return `幸运127`;
      case 'Fold8':
        return `8${t('lang.sport_bet_connect')}1`;
      case 'Goliath':
        return `大亨`;
      case 'Lucky255':
        return `幸运255`;
      case 'Fold9':
        return `9${t('lang.sport_bet_connect')}1`;
      case 'Fold10':
        return `10${t('lang.sport_bet_connect')}1`;
      default:
        return mulType;
    }
  };

  return {
    openTickets,
    addTicket,
    clearAllTickets,
    deleteTicket,
    handleBet,
    handleToRecharge,
    handleCheckAll,
    handleBranchChange,
    handleClosePopup,
    showHandicapName,
    needRecharge,
    state,
    currencySymbol,
    betAmount,
    winAmount,
    betLimit,
    branchLimit,
    betTickets,
    ticketSize,
    prebetInfo,
    checdkInfo,
    handleDoneBet,
    mulTypeToName,
    onTabChange
  };
}
