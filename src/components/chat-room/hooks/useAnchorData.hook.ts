import { getAnchorInfoApi } from '@/services';
import { ref, onMounted, Ref } from 'vue';

export interface AnchorData {
  id: string;
  liveRoomId: number;
  liveStatus: string;
  matchId: number;
  m3u8: string;
  name: string;
  photoId: string;
  status: string;
  videoSource: {
    flv: string;
    m3u8: string;
    rtmp: string;
  };
}
export function useAnchorData(matchId: number) {
  // 主播信息
  const anchorInfo: Ref<AnchorData> = ref({}) as Ref<AnchorData>;
  const anchorInited = ref(false);
  const getAnchorData = async () => {
    const [result, [data]] = await getAnchorInfoApi({ matchId });
    anchorInited.value = true;
    if (result && data) {
      anchorInfo.value = { ...data, m3u8: data.videoSource.m3u8 };
    }
  };
  // TODO 主播信息获取
  onMounted(() => {
    1 > 2 && getAnchorData();
  });
  return {
    anchorInfo,
    anchorInited
  };
}
