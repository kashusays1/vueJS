import { ActionContext } from 'vuex';
import { DataTypeEnum, PreferenceEnum, SportIdEnum, SportTimerEnum } from '@/common/enums';
import { MenuItem, SportsState, SportLeagueMatch } from '../types';
import { getEventSummary, queryEventsAndMarkets, queryLeagueList } from '@/services/sb-api';
import { getMatchInfoByIds } from '@/services/im-api';
import { useSBsports } from '@/hooks/useSBsports';
import { FAST_BET_TYPES, DEFAULT_BET_TYPES } from '@/common/consts';
import { isEqual, groupBy, map, flatten, sortBy, isEmpty } from 'lodash-es';

const initState = (): SportsState => ({
  // 今日、滚球、早盘 类型
  periodId: DataTypeEnum.Init,
  // 运动类型
  sportId: SportIdEnum.Init,
  sportInfo: { sportId: SportIdEnum.Soccerball, sportName: '足球', count: 0 },
  betSetType: PreferenceEnum.NotAccepted,
  sortByTime: false,
  sportTypes: new Map(),
  sportMatchList: [],
  sportsSource: 'im',
  sportTimer: new Map(),
  sportListCollapse: false,
  showOdds: DEFAULT_BET_TYPES,
  leagueList: [],
  leagueIds: [],
  sportDate: 0,
  // 左侧菜单 类型，首页 1，详情页2
  menuActiveTabIndex: 1,
  // 赛事详情数据
  sportMatchDetail: {
    enLeagueName: '',
    leagueId: 0,
    key: '',
    time: 0,
    leagueName: '',
    matches: []
  },
  // 统计赔率变化，首页
  oddTempList: {},
  // 统计赔率变化，赛事详情
  oddTempDetail: {}
});

const state: SportsState = initState();

const getters = {
  sportTypes: (state: SportsState) => state.sportTypes,
  periodId: (state: SportsState) => state.periodId,
  sportMenu: (state: SportsState) => state.sportTypes.get(state.periodId) || [],
  sportId: (state: SportsState) => state.sportId,
  sportInfo: (state: SportsState) => state.sportInfo,
  leagueIds: (state: SportsState) => state.leagueIds,
  sportDate: (state: SportsState) => state.sportDate,
  sortByTime: (state: SportsState) => state.sortByTime,
  leagueList: (state: SportsState) => state.leagueList,
  sportMatchList: (state: SportsState) => state.sportMatchList,
  sportListCollapse: (state: SportsState) => state.sportListCollapse,
  sportMatchNames: (state: SportsState) => state.sportMatchList.map(e => e.key),
  menuActiveTabIndex: (state: SportsState) => state.menuActiveTabIndex,
  showOdds: (state: SportsState) => state.showOdds,
  sportMatchDetail: (state: SportsState) => state.sportMatchDetail
};

const actions = {
  /**
   * 获取数据体育分类
   */
  async GET_SPORTS_EVENT_SUMMARY({ commit, dispatch, state }: ActionContext<SportsState, any>) {
    // 请求数据
    // 沙巴请求
    const fn = async () => {
      const [res, data] = await getEventSummary({ language: 'zh-CN' });
      const { convertSportsSort_v2 } = useSBsports();
      if (res) {
        // 格式化
        const sorts = convertSportsSort_v2(data);
        commit('SET_SPORTS_EVENT_SUMMARY', sorts);
        // 如果是第一次获取到菜单数据
        if (state.periodId === DataTypeEnum.Init && state.sportId === SportIdEnum.Init) {
          commit('SET_PERIOD_ID', DataTypeEnum.Live);
          commit('SET_SPORT_ID', sorts.get(DataTypeEnum.Live)![0].sportId);
          commit('SET_SPORT_INFO', sorts.get(DataTypeEnum.Live)![0]);
          dispatch('GET_SPORT_EVENT_LIST');
          dispatch('GET_LEAGUE_LIST');
        }
        dispatch('SET_SPORT_TIMER', {
          key: SportTimerEnum.Menu,
          value: setTimeout(fn, 15000)
        });
      }
    };
    await fn();
  },
  /**
   * 获取沙巴体育赛事详情数据
   */
  async GET_SPORT_MATCH_DETAIL(
    { commit, dispatch, state }: ActionContext<SportsState, any>,
    params?: {
      sportId?: number;
      periodId?: number;
      eventIds?: number;
      sendType?: number;
    }
  ) {
    // 为了保持页面数据持久，用户刷新页面，可以从url中赛事的参数

    const urlSearch = location.href.slice(1).split('&');
    const sportId = params?.sportId || +urlSearch.filter(item => item.includes('sportId'))[0]?.split('=')[1];
    const periodId = params?.periodId || +urlSearch.filter(item => item.includes('periodId'))[0]?.split('=')[1];
    const eventIds = params?.eventIds || +urlSearch.filter(item => item.includes('eventIds'))[0]?.split('=')[1];
    const [res, data] = await queryEventsAndMarkets({ sportId, periodId, eventIds });
    const { convertSportsHandicap } = useSBsports();
    if (res) {
      const { list, odd } = convertSportsHandicap(data, {
        periodId: periodId,
        sportId: sportId,
        showOdds: Object.keys(groupBy(data[0]?.matches[0].oddset, 'Bettype')).map(k => Number(k)),
        oldOdds: state.oddTempDetail
      });
      commit('SET_ODD_TEMP_DETAIL', odd);
      // sendType 0 || 1 ,0表示默认的，1表示定时器轮询的情况。界面初始化的时候轮询的时候与默认加载接口的时候。逻辑有些许小差别
      !isEmpty(list) && (list[0]['sendType'] = params?.sendType || 0);
      map(list[0]?.matches[0]?.odds, (item: any) => {
        // 处理波胆需要分开显示
        if ([405, 413, 414].includes(item.betType)) {
          const winArr: any = [];
          const flatAtt: any = [];
          const loseArr: any = [];
          !isEmpty(item.sels) &&
            map(item.sels, list => {
              const keys = list.key && list.key !== 'aos' ? (list.key as string).split(':') : 'aos';
              if (keys[0] > keys[1]) {
                winArr.push(list);
              } else if (keys[0] === keys[1]) {
                flatAtt.push(list);
              } else {
                loseArr.push(list);
              }
            });
          item.sels = [winArr, flatAtt, loseArr];
        }
        // 胜平负排序
        if (item.betType === 5) {
          item.sels = [item.sels[0], item.sels[2], item.sels[1]];
        }
      });
      // console.log(list[0]);
      commit('SET_SPORT_MATCH_DETAIL', list[0]);
      let timer;
      if (location.pathname === '/web/detail') {
        const time = periodId === 3 ? 6 : 20; // 滚球6s轮询一次，其他20s轮询一次
        timer = setTimeout(() => {
          dispatch('GET_SPORT_MATCH_DETAIL', { sendType: 1 });
        }, time * 1000);
      } else {
        clearTimeout(timer);
      }
    }
  },
  /**
   *获取赛事列表
   * @param {ActionContext<SportsState, any>} { commit, state }
   */
  async GET_SPORT_EVENT_LIST({ commit, dispatch, state }: ActionContext<SportsState, any>) {
    // 在详情页面防止刷新的时候sportId 被初始化
    if (location.pathname === '/web/detail') {
      const urlSearch = location.search.slice(1).split('&');
      const sportId = +urlSearch.filter(item => item.includes('sportId'))[0]?.split('=')[1];
      const periodId = +urlSearch.filter(item => item.includes('periodId'))[0]?.split('=')[1];
      commit('SET_SPORT_ID', sportId);
      commit('SET_PERIOD_ID', periodId);
    }
    const getParam = () => ({
      periodId: state.periodId,
      sportId: state.sportId,
      leagueIds: state.leagueIds.join(','),
      selectDate: state.sportDate,
      isParlay: false,
      betTypeIds: '1,2,3,5,20,473,501,7,8,12,15'
    });
    const fn = async () => {
      const params = getParam();
      const [res, data] = await queryEventsAndMarkets(params);
      // 获取显示盘口
      // 判断请求过程中参数是否改变
      if (res && isEqual(getParam(), params)) {
        const { convertSportsHandicap } = useSBsports();
        const showOdds = FAST_BET_TYPES.get(params.sportId) || DEFAULT_BET_TYPES;
        const { list, odd } = convertSportsHandicap(data, {
          periodId: params.periodId,
          sportId: params.sportId,
          showOdds,
          oldOdds: state.oddTempList
        });
        commit('SET_SPORT_EVENT_LIST', state.sortByTime ? sortBy(list, l => l.time) : list);
        commit('SET_ODD_TEMP_LIST', odd);
        commit('SET_SPORT_SHOW_ODD', showOdds);
        dispatch('GET_SOCCERBALL_TECH', data);
        dispatch('SET_SPORT_TIMER', {
          key: SportTimerEnum.LIST,
          value: setTimeout(fn, 15000)
        });
      }
    };
    await fn();
  },
  async GET_SOCCERBALL_TECH({ commit, state }: ActionContext<SportsState, any>, list: any[]) {
    // 足球滚球
    if (state.sportId === SportIdEnum.Soccerball && state.periodId === DataTypeEnum.Live) {
      // 获取所有比赛的半场比分
      const matchIds = map(flatten(map(list, l => l.matches)), m => m.MatchId);
      const [res, data] = await getMatchInfoByIds(matchIds.join(','));
      if (res) {
        const htData = new Map(
          map(data, d => {
            const { match_id, ht_home_score, ht_away_score } = d;
            return [match_id, [ht_home_score, ht_away_score]];
          })
        );
        commit('GET_HALF_DATA_SUCCESS', htData);
      }
    }
  },
  async GET_SOCCERBALL_TECH_IM({ commit }: ActionContext<SportsState, any>, list: any[]) {
    // 足球滚球
    // 获取所有比赛的半场比分
    const matchIds = map(flatten(map(list, l => l.matches)), m => m.MatchId);
    const [res, data] = await getMatchInfoByIds(matchIds.join(','));
    if (res) {
      const htData = new Map(
        map(data, d => {
          const { match_id, ht_home_score, ht_away_score } = d;
          return [match_id, [ht_home_score, ht_away_score]];
        })
      );
      commit('GET_HALF_DATA_SUCCESS', htData);
    }
  },
  /**
   *切换玩法
   * @param {ActionContext<SportsState, any>} { commit, state }
   */
  async CHANGE_PERIOD_ID({ commit, dispatch, state }: ActionContext<SportsState, any>, periodId: DataTypeEnum) {
    // 修改玩法ID
    commit('SET_PERIOD_ID', periodId);
    // 运动ID切换到当前玩法第一个
    commit('SET_SPORT_ID', state.sportTypes.get(periodId)![0].sportId);
    commit('SET_SPORT_INFO', state.sportTypes.get(periodId)![0]);
    commit('SET_LEAGUE_IDS', []);
    dispatch('GET_LEAGUE_LIST');
    // 获取赛事列表
    await dispatch('GET_SPORT_EVENT_LIST');
  },
  /**
   *切换运动类型
   * @param {ActionContext<SportsState, any>} { commit, state }
   */
  async CHANGE_SPORT_INFO({ commit, dispatch }: ActionContext<SportsState, any>, sportInfo: MenuItem) {
    // 修改运动类型
    commit('SET_SPORT_ID', sportInfo.sportId);
    commit('SET_SPORT_INFO', sportInfo);
    commit('SET_LEAGUE_IDS', []);
    dispatch('GET_LEAGUE_LIST');
    // 获取赛事列表
    await dispatch('GET_SPORT_EVENT_LIST');
  },
  CHANGE_SPORT_ID({ commit, dispatch }: ActionContext<SportsState, any>, sportId: SportIdEnum) {
    // 修改运动ID
    commit('SET_SPORT_ID', sportId);
    commit('SET_LEAGUE_IDS', []);
    // 获取赛事列表
    dispatch('GET_SPORT_EVENT_LIST');
  },
  // 获取联赛列表
  async GET_LEAGUE_LIST({ state, commit }: ActionContext<any, any>) {
    const params = {
      // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球 、4: Follow 关注, 5.plary 串关
      periodId: state.periodId,
      // 体育项目ID
      sportId: state.sportId,
      isParlay: false
    };
    const [res, data] = await queryLeagueList(params);
    if (res) {
      commit('SET_LEAGUE_LIST', data);
    }
  },
  async CHANGE_SPORT_SORT({ state, commit, dispatch }: ActionContext<any, any>) {
    commit('SET_SPORT_SORT', !state.sortByTime);
    await dispatch('GET_SPORT_EVENT_LIST');
  },
  SET_SPORT_TIMER({ state, commit }: ActionContext<any, any>, { key, value }: { key: SportTimerEnum; value: number }) {
    clearTimeout(state.sportTimer.get(key));
    commit('SET_SPORT_TIMER', { key, value });
  },
  DELETE_SPORT_TIMER({ state }: ActionContext<any, any>, { key }: { key: SportTimerEnum }) {
    clearTimeout(state.sportTimer.get(key));
    state.sportTimer.delete(key);
  },
  DELETE_ALL_SPORT_TIMER({ dispatch, state }: ActionContext<any, any>) {
    state.sportTimer.forEach((_value: number, key: SportTimerEnum) => {
      dispatch('DELETE_SPORT_TIMER', key);
    });
  }
};

// mutations
const mutations = {
  SET_SPORTS_EVENT_SUMMARY(state: SportsState, paylod: Map<DataTypeEnum, any>) {
    state.sportTypes = paylod;
  },
  SET_PERIOD_ID(state: SportsState, payload: DataTypeEnum) {
    state.periodId = payload;
  },
  SET_SPORT_ID(state: SportsState, payload: SportIdEnum) {
    state.sportId = payload;
  },
  SET_SPORT_INFO(state: SportsState, payload: MenuItem) {
    state.sportInfo = payload;
  },
  SET_SPORT_EVENT_LIST(state: SportsState, payload: any[]) {
    state.sportMatchList = payload;
  },
  SET_SPORT_LIST_COLLAPSE(state: SportsState, payload: boolean) {
    state.sportListCollapse = payload;
  },
  SET_SPORT_SHOW_ODD(state: SportsState, payload: number[]) {
    state.showOdds = payload;
  },
  SET_SPORT_DATE(state: SportsState, payload: number) {
    state.sportDate = payload;
  },
  SET_SPORT_SORT(state: SportsState, payload: boolean) {
    state.sortByTime = payload;
  },
  SET_LEAGUE_LIST(state: SportsState, payload: any[]) {
    state.leagueList = payload;
  },
  SET_LEAGUE_IDS(state: SportsState, payload: any[]) {
    state.leagueIds = payload;
  },
  SET_MENU_ACTIVE_TABINDEX(state: SportsState, payload: number) {
    state.menuActiveTabIndex = payload;
  },
  SET_SPORT_MATCH_DETAIL(state: SportsState, playload: SportLeagueMatch) {
    state.sportMatchDetail = playload;
  },
  SET_SPORT_TIMER(state: SportsState, { key, value }) {
    state.sportTimer.set(key, value);
  },
  SET_ODD_TEMP_LIST(state: SportsState, payload: any) {
    state.oddTempList = payload;
  },
  SET_ODD_TEMP_DETAIL(state: SportsState, payload: any) {
    state.oddTempDetail = payload;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
