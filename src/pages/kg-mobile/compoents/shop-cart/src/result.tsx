import { SportIcon, SvgIcon } from '@/components';
import { useBet, useI18n } from '@/hooks';
import { sumBy } from 'lodash-es';
import { defineComponent } from 'vue';
import result from './style/result.module.less';

export default defineComponent({
  name: 'BettingResult',
  setup() {
    debugger;
    const { state, checdkInfo, currencySymbol, handleDoneBet, showHandicapName } = useBet();
    const { t } = useI18n();
    const isSingle = state.tabActive === 0;
    const comboResult = state.betComoResult[0] || {};
    const resultInfo = isSingle
      ? checdkInfo.value.map(c => ({
          ...c,
          ...(state.betResultList.find(b => b.sort === c.sort) || {})
        }))
      : [...checdkInfo.value];
    const allBet = isSingle ? sumBy(resultInfo, r => r.stake) : comboResult.stake * Number(comboResult.bet_count) || 0;
    const allWin = isSingle ? sumBy(resultInfo, r => r.winAmount) : comboResult.winAmount || 0;
    return () => (
      <div class={result.page}>
        <div class={result.header}>
          <div class={result.headerItem}>
            <span class={result.labelTitle}>{isSingle ? t('lang.sport_bet_singBets') : comboResult.comboType}</span>
            <div class={result.content}>
              <SvgIcon name="bet_success" class={result.resultIcon} />
              <span class={result.contentBet}>
                {isSingle ? resultInfo.length : comboResult.betCount}
                {t('lang.sport_bet_betsNumber')}
              </span>
            </div>
          </div>
          <div class={result.headerItem}>
            <span class={result.label}>{t('lang.sport_bet_betAmount')}</span>
            <div class={result.content}>
              <span>
                {allBet}({currencySymbol.value})
              </span>
            </div>
          </div>
          <div class={result.headerItem}>
            <span class={result.label}>{t('lang.sport_bet_winAmount')}</span>
            <div class={result.content}>
              <span>
                {allWin}({currencySymbol.value})
              </span>
            </div>
          </div>
        </div>
        <div class={result.titleBox}>
          <span class={result.titleText}>{t('lang.sport_bet_betSucced')}</span>
        </div>
        <div class={result.betBox}>
          <div class={result.betBoxInner}>
            {resultInfo.map(c => (
              <div class={result.betItemBox}>
                <div class={result.sportIcon}>
                  <SportIcon sportId={c.sport_type} size="18" />
                </div>
                <div class={result.betInfo}>
                  <div class={result.betTop}>
                    <div class={result.betTeam}>
                      <span>
                        {c.keyName} {c.point}
                        <span class={result.odds}>@{c.price}</span>
                      </span>
                      <SvgIcon name="bet_success" class={result.resultIcon} />
                    </div>
                    <div class={result.type}>
                      <span>
                        {c.periodId === 3 ? <span>{t('lang.sport_handicap_live')} </span> : null} {c.betTypeName}
                      </span>
                      <span class={result.oddTypeName}>{showHandicapName(c.odds_type)}</span>
                    </div>
                    <div class={result.teamBox}>
                      <span class={result.teamName}>{c.home}</span>
                      <span class={result.vs}>VS</span>
                      <span class={result.teamName}>
                        {c.away}
                        {c.periodId === 3 ? (
                          <span class="t-teams-score">{`(${c.live_home_score}-${c.live_away_score})`}</span>
                        ) : null}
                      </span>
                    </div>
                  </div>
                  {isSingle ? (
                    <div class={result.betBottom}>
                      <div class={result.betAmountItem}>
                        <span class={result.betAmountLabel}>{t('lang.sport_bet_betAmount')}</span>
                        <span class={result.betAmountText}>
                          {c.stake}
                          <span class={result.betAmountDic}>({currencySymbol.value})</span>
                        </span>
                      </div>
                      <div class={result.betAmountItem}>
                        <span class={result.betAmountLabel}>{t('lang.sport_bet_winAmount')}</span>
                        <span class={result.betAmountText}>
                          {c.winAmount}
                          <span class={result.betAmountDic}>({currencySymbol.value})</span>
                        </span>
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div class={result.closeBtn} onClick={handleDoneBet}>
          {t('lang.sport_bet_complete')}
        </div>
      </div>
    );
  }
});
