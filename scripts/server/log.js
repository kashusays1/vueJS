const log = require('npmlog');
log.level = 'info';
log.heading = 'Sport:';
log.addLevel('success', 2000, { fg: 'green', bold: true });
log.addLevel('notice', 2000, { fg: 'blue', bg: 'black' });
module.exports = log;
