const { NodeSSH } = require('node-ssh');

async function sshmkdir(dir, options = {}) {
  const { host, username, password } = options;
  const ssh = new NodeSSH();
  await ssh.connect({
    host,
    username,
    password
  });
  await ssh.mkdir(dir);
  await ssh.dispose();
}
module.exports = {
  sshmkdir
};
