import { defineComponent, onMounted, ref } from 'vue';
import { LiveVideo } from 'kg-mobile/compoents';
import { getSportVideoByMatchID } from '@/services/kg-api';

export default defineComponent({
  name: 'VideoView',
  props: {
    videoSource: {
      type: String,
      default: ''
    }
  },
  setup(props) {
    const videoUrls = ref([]);

    onMounted(async () => {
      if (!props.videoSource) return;
      const url = props.videoSource.replace('/api', '');
      const [_res, data] = await getSportVideoByMatchID(url);
      if (_res && data.urlList) {
        videoUrls.value = data.urlList;
      }
    });

    return () => <div>{videoUrls.value.length > 0 ? <LiveVideo urls={videoUrls.value} /> : '暂无任何视频'}</div>;
  }
});
