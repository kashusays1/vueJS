import { defineComponent, PropType } from 'vue';
import headerStyle from './detail-header.module.less';
import { SvgIcon } from '@/components';
import { ActionSheet } from 'vant';

import { useWindowScroll } from '@vueuse/core';

import { useRouter, useRoute } from 'vue-router';
import { RouteEnum } from '@/pages/kg-mobile/router/router.enum';
import RelatedMatch from '../related-match';
import { useAuth, useDetailHook } from '@/hooks';

interface DataType {
  leagueName: string;
  sportName: string;
  sportid: number;
  home: string;
  away: string;
  periodId: number;
  time: string;
  date: string;
  isLive: number;
  score: number[];
}

const isQianKunApp = !!window.__POWERED_BY_QIANKUN__;

export default defineComponent({
  name: 'JBBdetailHeader',
  props: {
    data: {
      type: Object as PropType<DataType>,
      default: () => ({})
    }
  },
  setup(props) {
    const { y } = useWindowScroll();
    const { checkIsLogin } = useAuth();
    const router = useRouter();
    const route = useRoute();
    const { store } = useDetailHook();

    const openMassoetSheet = () => {
      store.showMatchs = true;
    };

    const backToHome = () => {
      const { sportId, periodId } = route.query;
      router.push({
        path: RouteEnum.ROOT,
        query: { sportId, periodId }
      });
    };

    const toOrderPage = () => {
      if (!checkIsLogin()) return;
      if (isQianKunApp) {
        alert('主项目投注记录');
      } else {
        router.push(RouteEnum.ORDERS);
      }
    };

    // 普通显示
    const NomalView = () => (
      <div class={headerStyle.detailHeaderNomal}>
        <div
          class={[headerStyle.leagueName, store.showMatchs ? headerStyle.up : null]}
          onClick={() => openMassoetSheet()}
        >
          <span>{props.data.leagueName || '--'}</span>
          <SvgIcon name="arrow_bottom" v-show={y.value < 100} />
        </div>
      </div>
    );

    // 非滚球显示
    const ScrollView = () => (
      <div
        class={headerStyle.detailHeaderScroll}
        style={{ opacity: (y.value > 100 ? 100 : y.value) / 100, zIndex: y.value > 2 ? 2 : y.value }}
      >
        <div class={headerStyle.home}>{props.data.home || '--'}</div>
        {props.data.isLive ? (
          <div class={headerStyle.sorces}>
            {props.data.score[0]}:{props.data.score[1]}
          </div>
        ) : (
          <span class={headerStyle.timeLabel}>{props.data.date + ' ' + props.data.time}</span>
        )}
        <div class={headerStyle.away}>{props.data.away || '--'}</div>
      </div>
    );
    return () => (
      <>
        <div class={headerStyle.detailHeader}>
          <div class={headerStyle.detailHeaderLeft} onClick={() => backToHome()}>
            <SvgIcon name="back" />
          </div>
          <div class={headerStyle.detailHeaderCenter}>
            <NomalView />
            <ScrollView />
          </div>
          <div class={headerStyle.detailHeaderRight} onClick={() => toOrderPage()}>
            <SvgIcon name="order" />
          </div>
        </div>
        <div class={headerStyle.noneBlock}></div>
        <ActionSheet teleport="#microApp" v-model:show={store.showMatchs} style="height: 80%">
          {store.showMatchs ? <RelatedMatch sportName={props.data.sportName} leagueId={store.leagueId} /> : null}
        </ActionSheet>
      </>
    );
  }
});
