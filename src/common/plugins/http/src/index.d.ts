import { AxiosRequestConfig, AxiosResponse } from 'axios';
export declare function createHttp(config?: AxiosRequestConfig): {
  request<T = any>(config: AxiosRequestConfig): Promise<T>;
  get<T_1 = any>(url: string, config?: AxiosRequestConfig): Promise<T_1>;
  delete<T_2 = any>(url: string, config?: AxiosRequestConfig): Promise<T_2>;
  head<T_3 = any>(url: string, config?: AxiosRequestConfig): Promise<T_3>;
  post<T_4 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_4>;
  put<T_5 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_5>;
  patch<T_6 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_6>;
  abort(url: string): void;
  abortAll(): void;
  defaults: AxiosRequestConfig;
  interceptors: {
    request: import('axios').AxiosInterceptorManager<AxiosRequestConfig>;
    response: import('axios').AxiosInterceptorManager<AxiosResponse<any>>;
  };
  getUri(config?: AxiosRequestConfig): string;
  options<T_7 = any, R = AxiosResponse<T_7>>(url: string, config?: AxiosRequestConfig): Promise<R>;
};
declare const http: {
  request<T = any>(config: AxiosRequestConfig): Promise<T>;
  get<T_1 = any>(url: string, config?: AxiosRequestConfig): Promise<T_1>;
  delete<T_2 = any>(url: string, config?: AxiosRequestConfig): Promise<T_2>;
  head<T_3 = any>(url: string, config?: AxiosRequestConfig): Promise<T_3>;
  post<T_4 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_4>;
  put<T_5 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_5>;
  patch<T_6 = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T_6>;
  abort(url: string): void;
  abortAll(): void;
  defaults: AxiosRequestConfig;
  interceptors: {
    request: import('axios').AxiosInterceptorManager<AxiosRequestConfig>;
    response: import('axios').AxiosInterceptorManager<AxiosResponse<any>>;
  };
  getUri(config?: AxiosRequestConfig): string;
  options<T_7 = any, R = AxiosResponse<T_7>>(url: string, config?: AxiosRequestConfig): Promise<R>;
};
export { http };
export * from 'axios';
