let eventForParent: any;

export function setEvent(event: any) {
  if (event) {
    eventForParent = event;
  }
}

export function emitParent(...args) {
  if (!eventForParent) {
    return console.error('No Event For Parent');
  }
  return eventForParent.emit(...args);
}
