// 数字键盘
import { defineComponent, ref } from 'vue';
import { keyboardAdd, keyboardNumbers } from './config';
import './style.less';
import { useBet } from '@/hooks/useBet';

export default defineComponent({
  name: 'NumberKeyborad',
  props: {
    max: {
      type: Number,
      default: 0
    },
    min: {
      type: Number,
      default: 0
    },
    statusValue: {
      type: Number,
      default: 0
    },
    // 是否显示键盘
    activeKeyboard: {
      type: Boolean,
      default: false
    },
    amount: {},
    onOpenKeyboard: {}
  },
  slots: {
    parlayName: null
  },
  emits: ['update:amount', 'open-keyboard'],
  setup(props, { emit }) {
    const statusValue: any = ref('');
    const { sbWalletBalance } = useBet();

    // 扩展预设数字
    const handleClickExpend = (inputValue: 'MAX' | number) => {
      let newValue: any = '';
      const oldValue = Number(statusValue.value);
      // 处理数字
      if (typeof inputValue === 'number') {
        newValue = oldValue + inputValue;
      }
      // 处理最大Max
      if (inputValue === 'MAX') {
        newValue = props.max;
      }
      // 检查值是否大于最大 或 者大于钱包
      const flag = newValue >= props.max || newValue >= sbWalletBalance.value;
      if (flag) {
        newValue = props.max - sbWalletBalance.value < 0 ? props.max : Math.floor(sbWalletBalance.value);
      }
      newValue = newValue === '' ? 0 : Number(newValue);
      statusValue.value = newValue;
      emit('update:amount', newValue || '');
    };

    // 处理普通数字
    const handleClickNumber = (value: 'del' | number) => {
      let newValue: any = '';
      const oldValue = Number(statusValue.value);
      // 数字
      if (typeof value === 'number') {
        newValue =
          oldValue === 0 && value === 0 ? '0' : `${statusValue.value === '0' ? '' : statusValue.value}${value}`;
      }
      // 处理删除
      if (value === 'del') {
        const oldStrValue = String(oldValue);
        if (!oldStrValue.length) return;
        newValue = oldStrValue.substring(0, oldStrValue.length - 1);
      }
      // 检查值是否大于最大 或 者大于钱包
      const flag = newValue >= props.max || newValue >= Number(sbWalletBalance.value);
      if (flag) {
        newValue = Math.floor(props.max - sbWalletBalance.value < 0 ? props.max : Number(sbWalletBalance.value));
      }
      newValue = newValue === '' ? 0 : Number(newValue);
      statusValue.value = newValue;
      emit('update:amount', newValue || '');
    };

    return () => (
      <div class="key-board-container" v-show={props.activeKeyboard}>
        <van-row class="key-board-numbers">
          {keyboardNumbers.map((num: number | 'del', index: number) => (
            <van-col class="btn-item-warp" key={index + '_BTN_KEY'} span="8">
              <div class={{ 'btn-item': true, del: num === 'del' }} onClick={() => handleClickNumber(num)}>
                {num === 'del' ? <svg-icon name="del" /> : num}
              </div>
            </van-col>
          ))}
        </van-row>
        <van-row class="key-board-expand">
          {keyboardAdd.map((num: number | 'MAX', index: number) => (
            <van-col class="btn-item-warp" key={index + '_BTN_KEY'} span="24">
              <div class={{ 'btn-item': true, max: num == 'MAX' }} onClick={() => handleClickExpend(num)}>
                {num}
              </div>
            </van-col>
          ))}
        </van-row>
      </div>
    );
  }
});
