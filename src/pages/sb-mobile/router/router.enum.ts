export enum RouteEnum {
  ROOT = '/',
  HOME = '/home',
  SPORT_DETAIL = '/detail',
  BET_HISTORY = '/bet-history',
  LOGIN = '/login',
  NOT_FOUND = '/404',
  DEMO = '/demo',
  NOTICE = '/notice'
}
