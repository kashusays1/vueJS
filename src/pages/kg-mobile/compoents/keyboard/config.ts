export const keyboardExtras: any[] = [100, 200, 500, 'MAX'];

export const keyboardNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, '.', 0, 'del'];

export type KeyType = '.' | 'delete' | 'extra' | 'close' | 'MAX';
