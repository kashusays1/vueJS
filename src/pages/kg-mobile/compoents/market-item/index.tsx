import { defineComponent, PropType } from 'vue';
import { CollapsePanel, FavoritStar } from 'kg-mobile/compoents';
// import { eventTimeFormatForIM } from '@/common';
// import { EventItem, EventsAndMarkets } from '@/services/kg-api';
import { useRoute, useRouter } from 'vue-router';

import MatchOdds from './odds/index';
import LiveStatistics from './live-statistics/index';

import { StoreMarketData, useI18n, useBet, StoreMatchInfo } from '@/hooks';

import './market-item.less';

// '让球', '大小', '独赢'
const typesI118n = ['lang.sport_common_handicap', 'lang.sport_common_size', 'lang.sport_common_winAlone'];

export default defineComponent({
  name: 'MarketItem',
  props: {
    data: {
      type: Object as PropType<StoreMarketData>,
      default: () => ({})
    }
  },
  setup(props) {
    const route = useRoute();
    const router = useRouter();
    const { addTicket } = useBet();
    // const { mark } = useFavortMatch();
    const { t } = useI18n();

    const pathToDetail = (matchId: number) => {
      router.push({
        path: '/detail',
        query: { ...route.query, matchId }
      });
    };

    const MatchInfoToolsRender = ({ matchData }: { matchData: StoreMatchInfo }) => (
      <div class={'match-info-tools'}>
        <FavoritStar
        // class="mark-icon"
        // isMarked={matchData.isMark}
        // onMark={() => mark({ matchId: matchData.id, sportId: matchData.sportId })}
        />
        <div class="match-time">
          {matchData.isLive ? <span>{t(matchData.period)}</span> : <span>{matchData.date}</span>}
          <span>{matchData.time}</span>
        </div>
        {matchData.isNeutral ? (
          <div class="is-neutral">
            <svg-icon name="neutral" />
          </div>
        ) : null}
        <div class="match-medias">
          <div class={['icon', !matchData.isLive ? 'is-early' : null]} v-show={matchData.video}>
            <svg-icon name="event_video" />
          </div>
          <div class={['icon', !matchData.isLive ? 'is-early' : null]} v-show={matchData.animate}>
            <svg-icon name="event_animation" />
          </div>
        </div>
        <div class="more-matchs">
          <span>{matchData.count || 0}</span>
          <svg-icon name="arrow_right" />
        </div>
      </div>
    );

    const MatchInfo = ({ matchData }: { matchData: StoreMatchInfo }) => (
      <div class={'team-info'} onClick={() => pathToDetail(matchData.id)}>
        <div class="home-team">
          <span class={'team-name'}>{matchData.home}</span>
          {matchData.tech?.red[0] ? <span class="red-card">{matchData.tech.red[0]}</span> : null}
          {matchData.tech?.yellow[0] ? <span class="yellow-card">{matchData.tech?.yellow[0]}</span> : null}
          {matchData.score ? <span class={'team-score hight'}>{matchData.score[0]}</span> : null}
        </div>
        <div class={'away-team'}>
          <span class={'team-name'}>{matchData.away}</span>
          {matchData.tech?.red[1] ? <span class="red-card">{matchData.tech.red[1]}</span> : null}
          {matchData.tech?.yellow[1] ? <span class="yellow-card">{matchData.tech?.yellow[1]}</span> : null}
          {matchData.score ? <span class={'team-score hight'}>{matchData.score[1]}</span> : null}
        </div>
        <LiveStatistics info={matchData} class="evets-statistics-container" />
      </div>
    );

    return () => (
      <CollapsePanel
        class={'match-info'}
        v-slots={{
          header: () => (
            <div class={'match-info-header'}>
              <div class={'league-name'}>{props.data.leagueName}</div>
              <div class={'events-types'}>
                {typesI118n.map(item => (
                  <span key={item}>{t(item)}</span>
                ))}
              </div>
              <div class={'match-count'}>
                <span>{props.data.matchs.length || 0}</span>
              </div>
            </div>
          ),
          default: () =>
            props.data.matchs.map(matchItem => (
              <>
                <MatchInfoToolsRender matchData={matchItem.info} />
                <div
                  class={['match-markets', Number(route.query.matchId) === matchItem.info.id ? 'selected' : null]}
                  key={matchItem.info.id}
                >
                  <MatchInfo matchData={matchItem.info} />
                  <MatchOdds
                    onAddBetTicket={sel =>
                      addTicket({
                        ...sel,
                        home: matchItem.info.home,
                        away: matchItem.info.away,
                        leagueName: props.data.leagueName,
                        market: matchItem.info.market
                      })
                    }
                    oddset={matchItem.oddset}
                    sportId={matchItem.info.sportId}
                    matchId={matchItem.info.id}
                  />
                </div>
              </>
            ))
        }}
      />
    );
  }
});
