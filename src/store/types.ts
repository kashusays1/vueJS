import { DataTypeEnum, SportIdEnum, SportTimerEnum } from '@/common/enums';
import { ParlayCombosItem, PlatformBalance, SportSortItem, TicketItemComp } from '@/services';
import { SourceType } from '@/types/config';

export interface MenuItem {
  sportId: SportIdEnum;
  sportName: string;
  count: number;
}
export interface SportsState {
  // 当前玩法 对应 滚球今日早盘
  periodId: DataTypeEnum;
  // 运动ID 足篮球乒乓球网球
  sportId: SportIdEnum;
  // 当前选中菜单信息
  sportInfo: MenuItem;
  // 赔率设置
  betSetType: number;
  // 按联赛|时间排序
  sortByTime: boolean;
  // 玩法菜单项
  sportTypes: Map<DataTypeEnum, MenuItem[]>;
  sportsSource: SourceType;
  // 大厅赛事列表
  sportMatchList: SportLeagueMatch[];
  // 定时器
  sportTimer: Map<SportTimerEnum, any>;
  // 赛事列表收起全部
  sportListCollapse: boolean;
  // 赛事列表显示盘口
  showOdds: number[];
  // 联赛列表
  leagueList: any[];
  // 选择联赛ID
  leagueIds: any[];
  // 早盘选中日期
  sportDate: number;
  // 左侧菜单栏 赛事列表与体育赛事列表tab
  menuActiveTabIndex: number;
  // 赛事详情数据
  sportMatchDetail: SportLeagueMatch;
  // 列表盘口缓存
  oddTempList: any;
  // 详情盘口缓存
  oddTempDetail: any;
}
export interface SportsTechState {
  isConnected: boolean;
  dataMap: Map<number, any>;
  halfScoreMap: Map<number, number[]>;
  wsClient: any;
  [key: string]: any;
}

export interface UserState {
  // 用户余额
  balance: number;
}

export interface SettingsState {
  // 投注规则
  betRuleType: number;
}

export interface BetState {
  singlePreOddKey: string | null;
  singlePreBetInfo: any;
  /**
   * 投注返回信息
   */
  doBetInfo: {
    isSuccess: boolean;
    doneBet: boolean;
    message: string;
  };
  /**
   * 串关预投参数
   */
  tickets: TicketItemComp[];
  /**
   * 串投ID缓存
   */
  oddsIds: { [key: string]: number };
  /**
   * 串关投注组合
   */
  ticketsCombos: ParlayCombosItem[];
}

export interface Modules {
  bet: BetState;
  sports: SportsState;
  sportsTech: SportsTechState;
  settings: SettingsState;
}

export interface SportsData {
  sportsSource: {
    name: string;
    code: string;
  };
  sportTypes: SportSortItem[];
}

export interface SportTypeItem {
  sportId: SportIdEnum;
  sportName: string;
  count: number;
}

export interface SportLeagueMatch {
  leagueId: number;
  leagueName: string;
  key: string;
  time: number;
  enLeagueName: string;
  matches: SportMatchItem[];
  sendType?: boolean;
}

export interface SportMatchItem {
  info: {
    home: string;
    away: string;
    leagueName: string;
    enLeagueName: string;
    id: number;
    count: number;
    date: string | null;
    timeText: string;
    score: number[];
    current?: number;
    pointScore: number[];
    tech?: {
      red: number[];
      yellow: number[];
    };
    sections?: number[];
  };
  odds: {
    betType: number;
    sels: OddSelItem[];
  }[];
}

export interface OddSelItem {
  betType: number;
  betTypeName: string;
  category: number;
  marketStatus: number;
  maxBet: number;
  oddsId: number;
  oddsType: number;
  key: string;
  selKey: string;
  keyName: string;
  name: string;
  price: number;
  point: number;
  change: string;
  filled?: boolean;
  locked?: boolean;
}

// export interface UserInfo extends UserHeartBeatInfo <BalanceData>;

export interface StoreUserInfo extends PlatformBalance {
  nickname: string;
  token: string;
  currency: string;
  /**
   * 账户
   */
  account: string;
  sex: number;
}
