import { CMS_API_KEY, SPORT_API_KEY, SPORT_LOGO_URL_KEY, SPORT_TENANT_KEY, WS_API_KEY } from '@/common/enums';

const env = process.env;

const CMS_SERVER_URL = env.VUE_APP_CMS_API; // 平台api
const SP_SERVER_RUL = env.VUE_APP_SERVER_IP; // 体育开发api
const SP_WS_RUL = env.VUE_APP_WS_URL; // webcoket
const isDev = env.NODE_ENV === 'development';

// 获取租户代码
export const getTenant = (): string => {
  if (isDev) {
    const devTenantCode = process.env.VUE_APP_TENANT_CODE || 'intsit';
    return devTenantCode;
  }
  const webTenant = localStorage.getItem(SPORT_TENANT_KEY) || '';
  return webTenant;
};

// 获取CMS平台api
export function getCMSapi(): string {
  if (isDev) {
    return CMS_SERVER_URL || '';
  }
  const beseAPI = localStorage.getItem(CMS_API_KEY) || '';
  return beseAPI;
}

// 获取体育api
export function getSportApi(): string {
  if (isDev) {
    return SP_SERVER_RUL + '/api';
  }
  const beseAPI = localStorage.getItem(SPORT_API_KEY) || '';
  return beseAPI + '/api';
}

// 获取websocket api域名
export function getWebsocketApi(): string {
  if (isDev) {
    return SP_WS_RUL || '';
  }
  const beseAPI = localStorage.getItem(WS_API_KEY) || '';
  return beseAPI;
}

// 获取球队标志cdn
export function getCNDlogoUrl(): string {
  const beseURL = localStorage.getItem(SPORT_LOGO_URL_KEY) || '';
  return beseURL;
}

// 语言编码转换
const LANG_CODES = {
  en: 'en-US',
  id: 'in-ID',
  th: 'th-TH',
  vi: 'vi-VN'
};

export function langFormat(lang: string) {
  const code = LANG_CODES[lang];
  return code || lang;
}
