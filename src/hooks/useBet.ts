/**
 * 投注
 * @description: 投注动作  获取预投注 投注状态 投注数据等
 */

import { Ref, computed, ComputedRef } from 'vue';
import { Fn } from '@/types/global';
import store from '@/store';
import { isEmpty, sortBy } from 'lodash-es';
import { PlaceBetPrams, ParlayTicketPrams, TicketItemComp, ParlayCombosItem, PlaceParlayBetData } from '@/services';
import { useGameSet } from '@/hooks/useGameSet';
import { RouteEnum } from '@/pages/sb-mobile/router';
import { useRoute } from 'vue-router';

interface UseBetFnReturn {
  doParlayBetAction: ({ BetTickets, ticketsCombos, AmountList }: DoBetPrams) => Promise<{
    isBetSuccess: boolean;
    message: string;
    betData?: PlaceParlayBetData;
  }>;
  doSingleBetAction: Fn;
  closeBetDialog: Fn;
  maxCombo: ComputedRef<number>;
  openBetDialog: Fn;
  removeTicket: (market_id: string | number) => void;
  removeAllTickets: Fn;
  initSingleBetPrams: Fn;
  gameBalance: ComputedRef<number>;
  sbWalletBalance: ComputedRef<number>;
  isParlayComputed: ComputedRef<boolean>;
  visibleBetDialog: Ref<boolean>;
  visibleBetCart: ComputedRef<boolean>;
  ticketsList: ComputedRef<TicketItemComp[]>;
  ticket: ComputedRef<any>;
  ticketsCombos: ComputedRef<ParlayCombosItem[]>;
  changeParlayStatus: ({ isParlay }: { isParlay: boolean }) => void;
  initPreBet: Fn;
  balance: ComputedRef<number>;
  isBetting: ComputedRef<boolean>;
  addBet: Fn;
  isShowInsufficient: ComputedRef<boolean>;
  insufficientAmount: ComputedRef<boolean>;
}

interface DoBetPrams {
  /**
   * 投注参数列表
   */
  BetTickets: TicketItemComp[];
  /**
   * 串关组合
   */
  ticketsCombos: ParlayCombosItem[];
  /**
   * 投注金额列表对象
   */
  AmountList: { [name: string]: any };
}

interface DoSingleBetPrams {
  /**
   * 投注参数
   */
  betTicket: TicketItemComp;
  /**
   * 投注金额
   */
  amount: number;
}

export function useBet(): UseBetFnReturn {
  // s是否显示余额不足弹窗
  const isShowInsufficient = computed(() => store.getters.isShowInsufficient.flag);
  // 串关投注金额
  const insufficientAmount = computed(() => store.getters.isShowInsufficient.amount);
  const route = useRoute();

  // 主钱包余额
  const balance = computed(() => store.getters.balance);

  // 最大maxCombo
  const maxCombo = computed(() => store.getters.maxCombo);

  // 游戏钱包余额
  const gameBalance = computed(() => store.getters.gameBalance);

  // 沙巴钱包余额
  const sbWalletBalance = computed(() => store.getters.SBwallet);

  /**
   * 是否为串
   */
  const isParlayComputed = computed(() => store.getters.isParlay);

  /**
   * 显示投注购物弹框
   */
  const visibleBetDialog = computed(() => {
    return false;
    // return !!store.state.bet.showBetDialog;
  });

  // 显示投注购物车按钮
  const visibleBetCart = computed(() => {
    const FilterRouteList = [RouteEnum.ROOT, RouteEnum.SPORT_DETAIL, RouteEnum.DEMO] as string[];
    const showBetCart = store.getters.showBetCart;
    return FilterRouteList.includes(route.path) && isParlayComputed.value && showBetCart;
  });

  /**
   * 预投注选中列表
   */
  const ticketsList = computed(() => store.getters.tickets);

  /**
   * 预投注选中列表
   */
  const ticket = computed(() => store.getters.ticket);

  /**
   * 关闭投注窗口
   */
  const closeBetDialog = async () => {
    await store.dispatch('OPEN_BET_DIALOG', false);
  };

  /**
   * 清除投注参数
   */
  const initPreBet = (isParlay: boolean) => {
    store.dispatch('INIT_PRE_BET', isParlay);
  };
  /**
   * 清除投注参数
   */
  const initSingleBetPrams = () => {
    store.dispatch('INIT_SINGLE_PRE_BET');
  };

  /**
   * 打开投注窗口
   */
  const openBetDialog = async () => {
    await store.dispatch('OPEN_BET_DIALOG', true);
  };

  /**
   * 正在投注
   */
  const isBetting = computed(() => store.getters.isBetting);

  const ticketsCombos = computed(() => {
    const Combos = store.getters.ticketsCombos as ParlayCombosItem[];
    if (isEmpty(Combos)) {
      return [];
    }
    return sortBy(Combos, (e: ParlayCombosItem) => e.bet_count);
  });

  /**
   * 添加单投投注信息
   */
  const addBet = async (betInfoItem: any) => {
    const { OddsId, Key, SportType, oddsType, KeyName } = await betInfoItem; // 投注参数
    const { BettypeName, AwayName, HomeName, LeagueName, Price, MatchId, ParentID, ScoreA, ScoreH } = betInfoItem; // 比赛信息
    const { Combo, Point, Point2, MarketStatus, Bettype, IsLive } = betInfoItem; // 其他盘口预显示信息
    const betPram = {
      key: Key,
      key_name: KeyName,
      market_id: OddsId,
      odds_type: oddsType,
      sport_type: SportType
    };
    const matchInfo = {
      AwayName,
      HomeName,
      LeagueName,
      MatchId,
      awayScore: ScoreA,
      homeScore: ScoreH
    };
    const oddInfo = {
      Price,
      Combo,
      Point,
      Point2,
      Bettype,
      IsLive,
      OddStatus: 0,
      OddStatusLabel: '',
      BettypeName,
      MarketStatus
    };

    if (isParlayComputed.value) {
      await store.dispatch('ADD_PARLAY_PRE_BET_ITEM', {
        betPram,
        matchInfo: {
          ...matchInfo,
          MatchId: ParentID || MatchId
        },
        oddInfo
      });
    } else {
      // 添加单投
      await store.dispatch('ADD_PRE_SINGLE_BET_ITEM', { betPram, matchInfo, oddInfo });
      openBetDialog();
    }
  };

  /**
   * 投注行为
   */
  const doParlayBetAction = async ({
    BetTickets,
    ticketsCombos,
    AmountList
  }: DoBetPrams): Promise<{ isBetSuccess: boolean; message: string; betData?: PlaceParlayBetData }> => {
    const { currentBetOption } = useGameSet();
    store.commit('IS_BETTING', true);
    // await store.dispatch('GET_PARLAY_PRE_BET_DATA');
    if (isEmpty(BetTickets)) {
      store.commit('IS_BETTING', false);
      return { isBetSuccess: false, message: '投注参数异常' };
    }
    /**
     * 串关参数处理
     */
    const bet_combos: any = [];
    const bet_matches: any = [];

    ticketsCombos.map((e: ParlayCombosItem, index: number) => {
      if (!!AmountList[index.toString()]) {
        bet_combos.push({
          combo_type: e.combo_type,
          stake: AmountList[index.toString()]
        });
      }
    });

    BetTickets.map((e: TicketItemComp) => {
      bet_matches.push({
        key: e.betPram.key,
        market_id: e.betPram.market_id,
        point: e.oddInfo.Point,
        price: e.oddInfo.Price,
        sport_type: e.betPram.sport_type
      });
    });

    const temParlayPrams: ParlayTicketPrams = {
      bet_matches,
      bet_combos,
      parlay_odds_option: currentBetOption.value
    };

    const { isBetSuccess, message, betData } = await store.dispatch('DO_PARLAY_BET_ACTION', temParlayPrams);
    return { isBetSuccess, message, betData };
  };

  /**
   * 单投投注行为
   */
  const doSingleBetAction = async ({
    betTicket,
    amount
  }: DoSingleBetPrams): Promise<{ isBetSuccess: boolean; message: string }> => {
    // await store.dispatch('GET_PRE_SINGLR_BET_DATA');
    if (isEmpty(betTicket)) {
      return { isBetSuccess: false, message: '投注参数为空' };
    }
    /**
     * 单投参数处理
     */
    const tempPrams: PlaceBetPrams = {
      oddsType: betTicket.betPram.odds_type,
      sportType: betTicket.betPram.sport_type,
      marketId: betTicket.betPram.market_id,
      price: betTicket.oddInfo.Price,
      stake: amount,
      point: betTicket.oddInfo.Point,
      key: betTicket.betPram.key
    };
    const { isBetSuccess, message } = await store.dispatch('DO_SINGLE_BET_ACTION', tempPrams);
    return { isBetSuccess, message };
  };

  /**
   * 串关 / 单投切换
   */
  const changeParlayStatus = ({ isParlay }: { isParlay: boolean }) => {
    store.dispatch('CHANGE_PARLAY_STATUS', { isParlay });
  };

  /**
   * 删除所有投注单
   */
  const removeAllTickets = () => {
    store.commit('INIT_PRE_BET_SUCCESS');
  };

  /**
   * 删除单条投注单
   */
  const removeTicket = (market_id: string | number) => {
    if (!market_id) return;
    store.dispatch('REMOVE_BET_TICKET', market_id);
  };

  return {
    changeParlayStatus,
    initSingleBetPrams,
    doSingleBetAction,
    doParlayBetAction,
    visibleBetDialog,
    isParlayComputed,
    gameBalance,
    closeBetDialog,
    visibleBetCart,
    removeAllTickets,
    openBetDialog,
    removeTicket,
    ticketsCombos,
    ticketsList,
    initPreBet,
    isBetting,
    maxCombo,
    balance,
    ticket,
    addBet,
    sbWalletBalance,
    isShowInsufficient,
    insufficientAmount
  };
}
