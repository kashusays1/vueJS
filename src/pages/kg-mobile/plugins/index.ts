import { App } from 'vue';
import componentList from './comp';
import { Lazyload } from 'vant';

export function initPlugin(app: App): void {
  app.use(Lazyload);
  [...componentList].forEach(component => app.component(component.name, component));
}
