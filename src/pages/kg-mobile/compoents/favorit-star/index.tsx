import { defineComponent, ref } from 'vue';
import { SvgIcon } from '@/components';
import style from './favorit-star.module.less';

export default defineComponent({
  name: 'FavoritStar',
  props: {
    isMarked: {
      type: Boolean,
      require: true
    }
  },
  emits: ['mark'],
  setup(props, { emit }) {
    const mark = ref(props.isMarked);

    const onMark = () => {
      mark.value = !mark.value;
      emit('mark');
    };

    return () => (
      <div class={[style.favoritStar, mark.value ? style.isMark : null]} onClick={() => onMark()}>
        <SvgIcon name="star" />
      </div>
    );
  }
});
