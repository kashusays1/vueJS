/**
 * 关注比赛
 */
import { computed } from 'vue';

import { Toast } from 'vant';
import { pull } from 'lodash-es';
import { FAVORT_MATCHS_KEY } from '@/common';
import { RemovableRef, useStorage } from '@vueuse/core';
import { useSportTypes } from '@/hooks';

const FAVORT_KEY = FAVORT_MATCHS_KEY;

interface Match {
  matchId: number;
  sportId: number;
}
export interface useFavortMatchRn {
  mark: (matchInfo: Match) => void;
  isMarkedById: (matchInfo: Match) => boolean;
}

export const localMarketedMatchs: RemovableRef<Map<number, number[]>> = useStorage(FAVORT_KEY, new Map());

// 关注总数量
export const markMatchsCount = computed(() => {
  let count = 0;
  const deepList = localMarketedMatchs.value;
  deepList.forEach(e => {
    count = count + e.length;
  });
  return count;
});

export function useFavortMatch(): useFavortMatchRn {
  const { updateMatchMarks } = useSportTypes();
  /**
   * 关注比赛
   */
  const mark = ({ matchId, sportId }: Match) => {
    const ids = localMarketedMatchs.value.get(sportId) || [];
    if (!sportId || !matchId || ids.length < 0) return;
    // 是否已关注
    const isExist = ids.includes(matchId);
    if (isExist) {
      pull(ids, matchId);
    } else {
      ids.push(matchId);
    }
    Toast({
      message: isExist ? '取消成功' : '关注成功',
      forbidClick: true
    });
    localMarketedMatchs.value.set(sportId, ids);
    updateMatchMarks();
  };

  const isMarkedById = ({ matchId, sportId }: Match) => {
    const ids = localMarketedMatchs.value.get(sportId) || [];
    if (!sportId || !matchId || ids.length === 0) return false;
    return ids.includes(matchId);
  };

  return {
    mark,
    isMarkedById
  };
}
