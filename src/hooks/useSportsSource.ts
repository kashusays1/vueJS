/**
 * 切换数据源
 */
import { computed, ComputedRef } from 'vue';
import store from '@/store';
import { Fn } from '@/types/global';

interface useSportsSourceFnReturn {
  getCurrentSportsSource: ComputedRef<any>;
  setSportsSource: Fn;
}

export function useSportsSource(): useSportsSourceFnReturn {
  const source: ComputedRef<any> = computed(() => store.state.sports.sportsSource);
  const setSportsSource = (sportSource: { name: string; code: string }) => {
    store.dispatch('SET_SPORTS_SOURCE', sportSource);
  };
  return {
    getCurrentSportsSource: source,
    setSportsSource
  };
}
