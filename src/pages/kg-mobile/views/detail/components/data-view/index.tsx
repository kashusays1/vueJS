import { defineComponent, provide } from 'vue';

import { SvgIcon } from '@/components';
import { DataTotal, TextLive, LineUp } from '../../components';
import RangeData from '../range-data/index.vue';
// import Intelligence from '../intelligence/index.vue';
import dataStyle from './data-view.module.less';
import { useSheetIndex, useGameInfo, useEventsData } from '@/hooks/useDetailData';

const TAB_ICONS = [
  'detail_tab_ranking',
  'detail_tab_live',
  'event_animation',
  'detail_tab_lineup'
  // 'detail_tab_intelligence'
];

export default defineComponent({
  name: 'DetailData',
  props: {
    showSheet: {
      type: Boolean
    }
  },
  setup(props) {
    const { state, changeSheetIndex } = useSheetIndex();
    const { moreMatchInfo, matchInfo, matchId, sportType } = useGameInfo();
    // 获取足球事件
    const { soccerBallEventsData, GetRealTimelist } = useEventsData({
      matchId
    });
    provide('matchInfo', { ...moreMatchInfo.value, matchInfo, sportType, moreMatchInfo });
    provide('soccerBallEventsData', soccerBallEventsData);

    return () => (
      <div class={dataStyle.dataSheet}>
        <div class={dataStyle.sheetTabs}>
          {TAB_ICONS.map((icon, index) => (
            <div
              key={icon}
              onClick={() => changeSheetIndex(index)}
              class={[dataStyle.sheetTabItem, state.sheetIndex === index ? dataStyle.active : null]}
            >
              <SvgIcon name={icon} />
            </div>
          ))}
        </div>
        <div class={dataStyle.tabContainer}>
          {
            state.sheetIndex === 0 ? (
              props.showSheet && <DataTotal />
            ) : state.sheetIndex === 1 ? (
              <TextLive realTimelist={soccerBallEventsData.value.real} GetRealTimelist={GetRealTimelist} />
            ) : state.sheetIndex === 2 ? (
              <RangeData analylist={soccerBallEventsData.value.analysis} />
            ) : state.sheetIndex === 3 ? (
              <LineUp lineUpData={soccerBallEventsData.value.lineUp} />
            ) : null
            // <Intelligence />
          }
        </div>
      </div>
    );
  }
});
