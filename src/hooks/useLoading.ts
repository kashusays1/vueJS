import { createApp } from 'vue';
import Loading from '@/pages/sb-web/components/common/loading.vue';

export const createLoading = (flag: boolean, target: any = document.body): void => {
  const createLoadingApp = createApp(Loading, { flag });
  if (flag) {
    if (!document.getElementsByClassName('loadingNode')[0]) {
      const loadingNode = document.createElement('div');
      loadingNode.className = 'loadingNode';
      loadingNode.style.width = target?.offsetWidth + 'px';
      loadingNode.style.height = target?.offsetHeight + 'px';
      target ? target.appendChild(loadingNode) : document.body.appendChild(loadingNode);
      createLoadingApp.mount(loadingNode);
    }
  } else {
    const loadingNode = document.getElementsByClassName('loadingNode')?.[0];
    // createLoadingApp.unmount();
    loadingNode?.parentElement?.removeChild(loadingNode);
  }
};
