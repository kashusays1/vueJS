import { computed, defineComponent, onMounted } from 'vue';
import style from './result.module.less';

import { Popup } from 'vant';
import { Collapse, CollapsePanel, LeagueFilter } from 'kg-mobile/compoents';
import { DataEmpty } from 'kg-mobile/compoents';
import { SvgIcon } from '@/components';
import { useEventResult, LAST_7_DAYS, SPORTS_LIST, useI18n } from '@/hooks';
import { isEmpty } from 'lodash-es';
import { useVirtualList } from '@vueuse/core';

import ResultItem from './item';

export default defineComponent({
  name: 'JBBsportPersonal',
  components: { Popup },
  setup() {
    const { getData, state, openSheet, openLeagues, onHandleDate, onHandleSport, closeSheet } = useEventResult();
    const { t } = useI18n();

    const sourceList = computed(() => {
      return Object.keys(state.matchMapByLeagueId);
    });

    const { list, containerProps, wrapperProps } = useVirtualList(sourceList, {
      itemHeight: 100,
      overscan: 50
    });

    onMounted(getData);

    const HeaderRender = () => (
      <div class={style.resultHeader}>
        <div class={style.labelTitle}>赛事</div>
        <div class={style.sortSwitch} onClick={() => openSheet({ type: 'time' })}>
          {t(LAST_7_DAYS[state.currentTimeIndex].week)}
          <SvgIcon class="pull-down-icon" name="pull_down" />
        </div>
        <div class={style.sortSwitch} onClick={() => openSheet({ type: 'sport' })}>
          {SPORTS_LIST[state.currentSportIndex].label}
          <SvgIcon class="pull-down-icon" name="pull_down" />
        </div>
        <p class={style.labelTitle} onClick={() => openLeagues()}>
          {t('lang.sport_filterLeague_allLeagues')}
        </p>
      </div>
    );

    const WeekSheetRender = () => (
      <div class={style.sheet}>
        <div class={style.sheetTitle}>
          时间筛选
          <span class={style.sheetTitleCancel} onClick={() => closeSheet()}>
            {t('lang.sport_common_cancel')}
          </span>
        </div>
        <div class={style.sheetContent}>
          {LAST_7_DAYS.map((day, index) => (
            <div
              onClick={() => onHandleDate(index)}
              key={index}
              class={[style.item, state.currentSportIndex === index ? style.active : null]}
            >
              {t(day.week)} ({day.date})
            </div>
          ))}
        </div>
      </div>
    );

    const SportSheetRender = () => (
      <div class={style.sheet}>
        <div class={style.sheetTitle}>
          {t('lang.sport_common_ballList')}
          <span class={style.sheetTitleCancel} onClick={() => closeSheet()}>
            {t('lang.sport_common_cancel')}
          </span>
        </div>
        <div class={style.sheetContent}>
          {SPORTS_LIST.map((sp, index) => (
            <div
              onClick={() => onHandleSport(index)}
              key={sp.value}
              class={[style.item, state.currentSportIndex === index ? style.active : null]}
            >
              {sp.label}
            </div>
          ))}
        </div>
      </div>
    );

    return () => {
      return (
        <>
          <HeaderRender />
          <div class={style.result} {...containerProps} style="overflow:auto;">
            {!isEmpty(list.value) ? (
              <div {...wrapperProps.value}>
                {list.value.map(({ index, data }) => (
                  <Collapse key={index} class="result-l-name">
                    <CollapsePanel
                      v-slots={{
                        header: () => (
                          <div class={style.leagueName}>{state.matchMapByLeagueId[data][0]?.leagueName}</div>
                        )
                      }}
                    >
                      {!isEmpty(state.matchMapByLeagueId[data]) &&
                        state.matchMapByLeagueId[data].map((info: any) => (
                          <ResultItem info={info} key={info.matchId} />
                        ))}
                    </CollapsePanel>
                  </Collapse>
                ))}
              </div>
            ) : (
              <DataEmpty
                type={'message'}
                title={t('lang.sport_common_noAnyData')}
                desc={t('lang.sport_common_lookElsewhere')}
              />
            )}
            {/* 弹出框 */}
            <Popup safe-area-inset-bottom v-model:show={state.showSheet} teleport="#microApp" position="bottom" round>
              {state.showTypeKey === 'time' ? <WeekSheetRender /> : null}
              {state.showTypeKey === 'sport' ? <SportSheetRender /> : null}
            </Popup>
            <Popup
              safe-area-inset-bottom
              v-model:show={state.showLeagus}
              teleport="#microApp"
              position="right"
              style={{ height: '100%', width: '100%' }}
            >
              {state.showLeagus ? (
                <LeagueFilter
                  isResult
                  sportId={SPORTS_LIST[state.currentSportIndex].value}
                  onConfirm={confirm}
                  time={{
                    startTime: LAST_7_DAYS[state.currentTimeIndex].startTime,
                    endTime: LAST_7_DAYS[state.currentTimeIndex].endTime
                  }}
                  onClose={() => openLeagues()}
                />
              ) : null}
            </Popup>
          </div>
        </>
      );
    };
  }
});
