import { createApp } from 'vue';

import App from './app.vue';

let instance: any;

function render() {
  instance = createApp(App);
  instance.mount('#microApp');
}
render();
