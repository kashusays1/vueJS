import { defineComponent, ref } from 'vue';
import './style/betting-orders.style.less';

import { HeaderBalance, KeyboardInput } from 'jbb-mobile/compoents';
import { Checkbox, CheckboxGroup, Popup, Picker } from 'vant';
import type { CheckboxGroupInstance } from 'vant';
import BetTicket from './bet-ticket';
import { SvgIcon } from '@/components';
import { useAuth, useBet, useI18n } from '@/hooks';

export default defineComponent({
  name: 'BettingOrders',
  emits: ['clearAllTickets'],
  setup(_props, { emit }) {
    const checkboxGroup = ref<CheckboxGroupInstance>();
    const { isLogin } = useAuth();
    const { t } = useI18n();

    const {
      state,
      prebetInfo,
      checdkInfo,
      betLimit,
      betAmount,
      winAmount,
      branchLimit,
      currencySymbol,
      needRecharge,
      handleToRecharge,
      handleBet,
      onTabChange,
      handleBranchChange
    } = useBet();
    // 全选
    const checkAll = () => {
      checkboxGroup.value && checkboxGroup.value.toggleAll(state.isCheckedAll);
    };

    const onConfirm = value => {
      state.parylayTypeResult = value;
      state.showParylayType = false;
    };

    const TabsRender = () => (
      <div class="tabs">
        {state.typeTabs.map((item, index) => (
          <div
            onClick={() => onTabChange(index)}
            class={{ 'tabs-item': true, active: index === state.tabActive }}
            key={index}
          >
            <span>{t(item)}</span>
          </div>
        ))}
      </div>
    );

    // 单投
    const SingleBetRenader = () => (
      <div class="single-bet">
        <div class="bet-tools">
          <div onClick={() => checkAll()} class="check-all">
            <Checkbox v-model={state.isCheckedAll}>{t('lang.sport_common_selectAll')}</Checkbox>
          </div>
          <div class="delelt-all" onClick={() => emit('clearAllTickets')}>
            <SvgIcon name="clear" />
          </div>
        </div>
        <CheckboxGroup v-model={state.checkedAll} ref={checkboxGroup}>
          {prebetInfo.value.map((item, index) => (
            <BetTicket bet={item} key={index}>
              <KeyboardInput
                currency={currencySymbol.value}
                max={betLimit.value[item.uniqKey]?.max_bet || 0}
                min={betLimit.value[item.uniqKey]?.min_bet || 0}
                v-model:amount={betLimit.value[item.uniqKey].stake}
              />
            </BetTicket>
          ))}
        </CheckboxGroup>
      </div>
    );

    // 串关
    const ParlayBetRender = () => (
      <div class="single-bet">
        <div class="bet-tools">
          <div onClick={() => checkAll()} class="check-all">
            <Checkbox v-model={state.isCheckedAll}>{t('lang.sport_common_selectAll')}</Checkbox>
          </div>
          <div class="delelt-all" onClick={() => emit('clearAllTickets')}>
            <SvgIcon name="clear" />
          </div>
        </div>
        <CheckboxGroup v-model={state.checkedAll} ref={checkboxGroup}>
          {prebetInfo.value.map((item, index) => (
            <BetTicket bet={item} key={index}></BetTicket>
          ))}
        </CheckboxGroup>
        {checdkInfo.value.length < 2 ? (
          <span class="num-text">{t('lang.sport_bet_InsufficientNumberOfHandicapUnableToJoinTheField')}</span>
        ) : checdkInfo.value.find(c => c.isSameGame) ? (
          <span class="num-text">{t('lang.sport_bet_pleaseDeleteItemsThatDoNotComplyWithTheStringFieldRules')}</span>
        ) : null}
      </div>
    );

    const DuplexParlayBetRender = () => (
      <div class="duplex-parlay">
        {state.parylayTypeResult ? (
          <div class="duplex-parlay-select" onClick={() => (state.showParylayType = true)}>
            <span>{state.parylayTypeResult}</span>
            <SvgIcon class="pull-down-icon" name="pull_down" />
          </div>
        ) : null}
        <div class="bet-tools">
          <div onClick={() => checkAll()} class="check-all">
            <Checkbox v-model={state.isCheckedAll}>{t('lang.sport_common_selectAll')}</Checkbox>
          </div>
          <div class="delelt-all" onClick={() => emit('clearAllTickets')}>
            <SvgIcon name="clear" />
          </div>
        </div>
        <CheckboxGroup v-model={state.checkedAll} ref={checkboxGroup}>
          {prebetInfo.value.map((item, index) => (
            <BetTicket bet={item} key={index} />
          ))}
        </CheckboxGroup>
        {checdkInfo.value.length < 3 ? (
          <span class="num-text">盘口数量不足，无法复式串场</span>
        ) : checdkInfo.value.find(c => c.isSameGame) ? (
          <span class="num-text">{t('lang.sport_bet_pleaseDeleteItemsThatDoNotComplyWithTheStringFieldRules')}</span>
        ) : null}
      </div>
    );

    // 复式串场选择
    const DuplexParlaySelectedRender = () => (
      <Popup
        v-model:show={state.showParylayType}
        teleport="#microApp"
        safeAreaInsetBottom
        round
        position="bottom"
        style="height:50%"
      >
        <Picker
          title={t('lang.sport_bet_duplexLinkOption')}
          columns={state.parlayColumns}
          onCancel={() => (state.showParylayType = false)}
          onConfirm={value => onConfirm(value)}
        />
      </Popup>
    );

    const SingleBetFooter = () => (
      <>
        <KeyboardInput
          currency={currencySymbol.value}
          max={branchLimit.value[1]}
          min={branchLimit.value[0]}
          amount={state.eachAmount}
          class="branch-input"
          placeholder={t('lang.sport_bet_betAmount')}
          showCurrency={false}
          onUpdate:amount={handleBranchChange}
        >
          <div>{t('lang.sport_bet_singBets')}：</div>
          {state.eachAmount > 0 && state.eachAmount < branchLimit.value[0] ? (
            <span class="error-range">{t('lang.sport_bet_belowTheMinimumStake')}</span>
          ) : state.eachAmount > 0 && state.eachAmount > branchLimit.value[1] ? (
            <span class="error-range">高于最大投注额</span>
          ) : null}
        </KeyboardInput>
        <div class="sj">
          <span>
            {t('lang.sport_bet_betAmount')}：{betAmount.value}
            {betAmount.value > 0 ? `(${currencySymbol.value})` : ''}
          </span>
          <span>
            {t('lang.sport_bet_winAmount')}：
            <span class="canwin-amount">
              {winAmount.value}
              {winAmount.value > 0 ? `(${currencySymbol.value})` : ''}
            </span>
          </span>
        </div>
      </>
    );

    const ParlayBetFooter = () => {
      const allCombo = Array.from(state.betParlyCombo.values()).find(b => b.bet_count === 1);
      const canWin = allCombo ? Number((allCombo.odds * (allCombo.stake || 0)).toFixed(2)) || '' : 0;
      return allCombo ? (
        <>
          <KeyboardInput
            currency={currencySymbol.value}
            max={Number(allCombo.max_bet)}
            min={Number(allCombo.min_bet)}
            v-model:amount={allCombo.stake}
          >
            <div>
              {t('lang.sport_bet_stringField')}：{allCombo.mulType}
            </div>
            {allCombo.stake > 0 && allCombo.stake < Number(allCombo.min_bet) ? (
              <span class="error-range">{t('lang.sport_bet_belowTheMinimumStake')}</span>
            ) : allCombo.stake > 0 && allCombo.stake > Number(allCombo.max_bet) ? (
              <span class="error-range">高于最大投注额</span>
            ) : null}
            {/* <span>限额：5.0-1500.0</span> */}
          </KeyboardInput>
          <div class="sj">
            <span>
              {t('lang.sport_bet_odds')}：{allCombo.odds}
            </span>
            <span>
              {t('lang.sport_bet_winAmount')}：
              <span class="canwin-amount">
                {canWin}
                {canWin > 0 ? `(${currencySymbol.value})` : ''}
              </span>
            </span>
          </div>
        </>
      ) : null;
    };

    const DuplexParlayBetFooter = () => {
      const current = state.betParlyCombo.get(state.parylayTypeResult);
      const betAmount = current ? Number((current.stake * current.bet_count).toFixed(2)) || '' : 0;
      const canWin = current ? Number((current.odds * current.bet_count * (current.stake || 0)).toFixed(2)) || '' : 0;
      return current ? (
        <>
          <KeyboardInput
            currency={currencySymbol.value}
            max={Number(current.max_bet)}
            min={Number(current.min_bet)}
            v-model:amount={current.stake}
          >
            <div>
              {t('lang.sport_bet_stringField')}：{current.mulType} <span>*{current.bet_count}</span>
            </div>
            {/* <span>限额：5.0-1500.0</span>
          <span>低于最小投注</span> */}
          </KeyboardInput>
          <div class="sj">
            <span>
              {t('lang.sport_bet_betAmount')}：{betAmount}
              {betAmount > 0 ? `(${currencySymbol.value})` : ''}
            </span>
            <span>
              {t('lang.sport_bet_winAmount')}：
              <span class="canwin-amount">
                {canWin}
                {canWin > 0 ? `(${currencySymbol.value})` : ''}
              </span>
            </span>
          </div>
        </>
      ) : null;
    };

    // footer
    const BetFooterRender = () => (
      <div class="bet-footer">
        {state.tabActive === 0 ? (
          <SingleBetFooter />
        ) : state.tabActive === 1 ? (
          <ParlayBetFooter />
        ) : (
          <DuplexParlayBetFooter />
        )}

        <div class="bet-button">
          {needRecharge.value ? (
            <button onClick={handleToRecharge}>
              {t('lang.sport_bet_insufficientWalletBalancePleaseGoToRecharge')}&gt;&gt;
            </button>
          ) : (
            <button onClick={handleBet}>{t('lang.sport_bet_bet')}</button>
          )}
        </div>
      </div>
    );

    return () => [
      <div class="betting-orders">
        <div class="betting-orders-header">
          <span class="betting-orders-header-title">{t('lang.sport_bet_bettingSlip')}</span>
          {isLogin.value ? <HeaderBalance /> : null}
        </div>
        <TabsRender />
        {state.tabActive === 0 ? (
          <SingleBetRenader />
        ) : state.tabActive === 1 ? (
          <ParlayBetRender />
        ) : (
          <DuplexParlayBetRender />
        )}
        <BetFooterRender />
      </div>,
      <DuplexParlaySelectedRender />
    ];
  }
});
