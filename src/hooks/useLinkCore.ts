import { Ref, ref, computed } from 'vue';
import { Fn } from '#/global';
import { useStore } from 'vuex';
import dayjs from 'dayjs';
import { debounce, DebouncedFunc } from 'lodash-es';
import { ObjectName, getLoginData, getTenant, getWebsocketApi } from '@/common';
import { Client, Message, messageCallbackType, StompConfig } from '@stomp/stompjs';

const WS_URL = getWebsocketApi() + '/system/websocket';

export interface MessageItem {
  account: string;
  anchorId: string;
  chId: string;
  createTime: string;
  createTimeL: string;
  id: string;
  msg: string;
  msgType: number;
  nick: string;
  role: number;
  tenant: string;
  time: string;
  uid: string;
  vip: number;
}

export type CmdTypes = 'M01' | 'M02' | 'L03';

// 广播订阅url: /topic/当前租户.赛事id.主播id.message   示例：/topic/ylbl.4445847.34.message
// 用户私有消息订阅url: /topic/user.租户标识.用户账号.message   示例： /topic/user.ylbl.ceshi123.message
// 发消息url: /message

interface UseChatRn {
  isShowId: Ref<boolean>;
  Resobj: Ref<any>;
  /**
   * 是否链接
   */
  isConnected: Ref<boolean>;
  /**
   * 初始化聊天
   */
  initChat: Fn;
  /**
   * 聊天信息列表
   */
  messageList: Ref<MessageItem[]>;
  /**
   * 发送消息
   */
  sendMessage: DebouncedFunc<(msg: string, matchId?: string | number) => void>;
  /**
   * 关闭聊天
   */
  closeChat: Fn;
  /**
   * 订阅
   */
  subscribe: (
    { destination, matchId }: { destination: string; matchId?: string | number },
    fn?: messageCallbackType
  ) => void;
  /**
   * 取消订阅
   */
  unSubscribe: (destination: string, fn: messageCallbackType) => void;
}

export function useLinkChat(): UseChatRn {
  const store = useStore();
  const userInfo = computed(() => store.state.user);
  const ResData = ref([]);
  const Resobj = ref<null | any>(null);
  const { token = '', uid = '' } = getLoginData() as any; // 获取令牌
  const isShowId = ref(false);
  const isConnected = ref(false);

  const headers = {
    ack: 'auto',
    tenant: getTenant(),
    token: token,
    uid: uid,
    userType: 'system',
    nick: 'xO5QI6tJ',
    isFirst: 1,
    guestTeamId: 0,
    homeTeamId: 0,
    chId: 0
  };

  const messageList: Ref<MessageItem[]> = ref([]);
  const chatConfig: StompConfig = {
    connectHeaders: {
      token, //协议新增
      tenant: getTenant() //租户标识
    },
    brokerURL: WS_URL,
    reconnectDelay: 2000,
    heartbeatIncoming: 210 * 1000,
    heartbeatOutgoing: 210 * 1000,
    onConnect: () => {
      console.log(
        '%c==========================\n= Events Data Server Connected! =\n==========================',
        'font-size:14px;color:#069B71'
      );
      isShowId.value = true;
      isConnected.value = window.WSclient2.connected;
    },
    onStompError: (error: any) => {
      console.log(
        '%c===============================\n= Events Data Server Connected Fail! =\n===============================',
        'font-size:14px;color:red'
      );
      console.error(error, '<<<<<<<<<<<,,,,,,,,');
    },
    onDisconnect: (error: any) => {
      console.error('断开了 ws', error);
      isConnected.value = window.WSclient2.connected;
    }
  };

  // 发送消息
  const sendMessage = (msg: string, matchId: string) => {
    const { nickName, username, vipLevel, id } = userInfo.value;
    if (!msg) return;
    const data = {
      cmd: 'M01',
      data: {
        account: username,
        chId: matchId,
        createTimeL: dayjs().valueOf(),
        ext: null,
        msg: msg,
        msgType: 1,
        nick: nickName,
        role: 0,
        time: 0,
        token: token,
        uid: id,
        vip: vipLevel
      }
    };
    window.WSclient2.publish({
      destination: '/message',
      body: JSON.stringify(data),
      headers: { token }
    });
  };

  // 订阅
  const subscribe = ({ destination, matchId }, fn?: messageCallbackType) => {
    !!window.WSclient2.subscribe &&
      window.WSclient2.subscribe(
        destination,
        !!fn ? fn : ({ body }: Message) => onMessage(JSON.parse(body)),
        !!matchId
          ? {
              ...headers,
              matchId
            }
          : headers
      );
  };

  // 订阅
  const unSubscribe = () => {
    window.WSclient2.unsubscribe();
  };

  // 关闭聊天
  const closeChat = () => {
    window.WSclient2.deactivate();
  };

  // 初始化聊天
  const initChat = async () => {
    window.WSclient2 = new Client(chatConfig);
    window.WSclient2.activate();
  };
  const buildTechinData = self => {
    const str = self.split(';');
    ResData.value = [];
    for (let i = 0; i < str.length; i++) {
      const str1 = str[i].split(',');
      if (str1.length === 3) {
        Resobj.value[ObjectName[str1[0]]] = { homeData: str1[1], awayData: str1[2] };
      } else if (str1.length === 2) {
        Resobj.value[ObjectName[str1[0]]] = { homeData: str1[1], awayData: '' };
      }
    }
  };
  // 消息接收
  const onMessage = async (chatData: { cmd: CmdTypes; data: string }) => {
    const { data } = chatData;
    try {
      const technicS = data.split('&');
      for (let i = 0; i < technicS.length; i++) {
        const technicID = technicS[i].split('^');
        if (technicID[0] == '45298468') {
          buildTechinData(technicID[1]);
        }
      }
    } catch (ex) {
      console.error('msg parse failed', ex);
    }
  };
  return {
    isShowId,
    Resobj,
    isConnected,
    messageList,
    sendMessage: debounce(sendMessage, 5000, { leading: true }),
    unSubscribe,
    closeChat,
    subscribe,
    initChat
  };
}
