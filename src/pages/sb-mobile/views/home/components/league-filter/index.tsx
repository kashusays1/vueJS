import { defineComponent, ref, Ref, computed, toRaw, watch, nextTick, onMounted, onBeforeUnmount } from 'vue';
import { LeagueItem } from '@/services';
import { useStore } from 'vuex';
import { isEmpty } from 'lodash-es';
import './style.less';

interface AllLeagueList {
  [name: string]: LeagueItem[];
}

export default defineComponent({
  name: 'LeagueFilter',
  props: {
    sportId: {
      type: Number,
      require: true
    },
    isParlay: {
      type: Boolean
    }
  },
  emits: ['confirm-checked', 'close'],
  setup(props, { emit }) {
    const store = useStore();

    const allLeagueChecked = ref([]);
    const checkboxGroup: Ref<any> = ref(null);

    // 联赛列表(带头字母的)
    const leagueList = computed(() => toRaw(store.getters.leagueLetters));

    // 是否可以点击
    const isBtnDisabled = computed(() => isEmpty(allLeagueChecked.value));

    // 時候全選
    const isCheckedAll = ref(false);

    // 锁定滚动
    const lockScroll = () => {
      document.body.style.setProperty('height', '100%');
      document.documentElement.style.setProperty('height', '100%');
      document.body.classList.add('van-overflow-hidden');
    };

    // 解锁滚动
    const unlockScroll = () => {
      document.body.style.setProperty('height', 'auto');
      document.documentElement.style.removeProperty('height');
      document.body.classList.remove('van-overflow-hidden');
    };

    onMounted(lockScroll);
    onBeforeUnmount(unlockScroll);

    // 首字母
    const letters = computed(() => Object.keys(store.getters.leagueLetters) || []);
    watch(letters, val => {
      if (isEmpty(val)) return;
      nextTick(() => {
        checkAll();
      });
    });
    const allLeagueCheckedlength = computed(() => allLeagueChecked.value.length);
    watch(allLeagueCheckedlength, val => {
      isCheckedAll.value = leagueListRSt.value === val;
    });
    const leagueListRSt = computed(() => {
      let reulst = 0;
      letters.value.map((el: any) => {
        reulst += leagueList.value[el].length;
      });
      return reulst;
    });

    // 全选
    const checkAll = () => {
      const oldChecked = isCheckedAll.value;
      !!checkboxGroup.value && checkboxGroup.value.toggleAll(!oldChecked);
      isCheckedAll.value = !oldChecked;
    };

    // 反选
    const toggleAll = () => {
      // if (!isCheckedAll.value) return;
      // isCheckedAll.value = !isCheckedAll.value;
      isCheckedAll.value = allLeagueCheckedlength.value === 0;
      !!checkboxGroup.value && checkboxGroup.value.toggleAll();
    };

    // 关闭
    const onHandleClose = async () => emit('close');

    // 确认选中
    const confirmChecked = async () => {
      emit('confirm-checked', toRaw(allLeagueChecked.value));
      await onHandleClose();
    };

    // 多选框UI
    const renderCheckbox = (item: LeagueItem) => (
      <van-checkbox shape="round-corner" name={item.leagueId} class="league-check">
        <div class="league-info van-ellipsis">
          <team-logo sportId={props.sportId} leagueName={item.leagueId || item.leagueName} size="md" />
          {item.leagueName}
        </div>
      </van-checkbox>
    );

    // 所有联赛dom
    const renderAllLeague = () => (
      <div class="all-league">
        <div class="text-01">全部联赛A-Z</div>
        <van-index-bar class="index-bar" indexList={letters.value} sticky={false}>
          <van-checkbox-group v-model={allLeagueChecked.value} ref={checkboxGroup}>
            <div style={{ height: '5px' }}></div>
            {!isEmpty(letters.value) &&
              letters.value.map((el: keyof AllLeagueList, index: number) => {
                return (
                  <>
                    <van-index-anchor index={`${index}`}>{el}</van-index-anchor>
                    {!!leagueList.value[el] &&
                      leagueList.value[el].map((item: LeagueItem, i: number) => (
                        <div class="league-select" key={i + '_LI_KEY'}>
                          {renderCheckbox(item)}
                        </div>
                      ))}
                  </>
                );
              })}
          </van-checkbox-group>
        </van-index-bar>
      </div>
    );

    // 底部Dom
    const renderFooter = () => (
      <div class="filter-footer">
        <div class="console-btn">
          <div
            onClick={checkAll}
            class={[
              'van-checkbox__icon van-checkbox__icon--round-corner league-check',
              !!isCheckedAll.value ? 'van-checkbox__icon--checked' : null
            ]}
            style="display: flex;"
          >
            <div class="van-checkbox__icon">
              <i class="van-badge__wrapper van-icon van-icon-success"></i>
            </div>
            <span class="invert-checked">全选</span>
          </div>
          <span class="invert-checked" onClick={toggleAll}>
            反选
          </span>
        </div>
        <van-button disabled={isBtnDisabled.value} round class="confirm" onClick={confirmChecked}>
          确定({allLeagueCheckedlength.value})
        </van-button>
      </div>
    );

    return () => {
      const title = props.isParlay ? '联赛筛选' : '赛事筛选';
      return (
        <>
          <div class="filter-header">
            <kg-nav-bar onClickLeft={onHandleClose} title={title + leagueListRSt.value} />
          </div>
          {isEmpty(leagueList.value) ? (
            <data-empty class="nodata" />
          ) : (
            <div class="filter-content">{renderAllLeague()}</div>
          )}
          {renderFooter()}
        </>
      );
    };
  }
});
