import { defineComponent } from 'vue';
import './collapse.style.less';

export default defineComponent({
  name: 'Collapse',
  props: {
    expandAll: {
      type: Boolean,
      default: false
    }
  },
  setup(props, { slots }) {
    return () => <div class={['collapse', props.expandAll ? 'expand-all' : null]}>{slots.default?.()}</div>;
  }
});
