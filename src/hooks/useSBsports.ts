/* eslint-disable prettier/prettier */
/**
 * 沙巴数据格式化
 */

import { isEmpty, isArray, map, filter, sortBy, fill } from 'lodash-es';
import { EventSummary, SportSortItem } from '@/services';
import { DataTypeEnum, SportIdEnum } from '@/common/enums';
import { SportTypeItem } from '@/store/types';
import dayjs from 'dayjs';
import { eventTimeFormat } from '@/common/utils';
let oddTemp: any = {};
// type
type SportsSortResoult = [SportSortItem, any];

export function useSBsports() {
  /**
   * 格式化体育分类
   */
  const convertSportsSort = (list: EventSummary[], collectMatches: { [key in string]: number[] }) => {
    if (isEmpty(list) || !isArray(list)) return [];
    const tmpList: SportSortItem[] = [];
    list.map((e: EventSummary) => {
      tmpList.push({
        sportId: e.sport_type,
        sportName: e.sport_type_name,
        earlyCount: e.early,
        todayCount: e.today,
        liveCount: e.live,
        parlayCount: e.parlay,
        followdEventIds: collectMatches[e.sport_type] || [],
        followdCount: (collectMatches[e.sport_type] || []).length
      });
    });
    return tmpList as SportsSortResoult;
  };

  const convertSportsSort_v2 = (list: EventSummary[]) => {
    const temp: {
      live: SportTypeItem[];
      today: SportTypeItem[];
      early: SportTypeItem[];
    } = {
      live: [],
      today: [],
      early: []
    };
    const keys = Object.keys(temp);
    list.forEach(item => {
      keys.forEach(k => {
        item[k] > 0 &&
          temp[k].push({
            sportId: item.sport_type,
            sportName: item.sport_type_name,
            count: item[k],
            live: item.live
          });
      });
    });
    return new Map([
      [DataTypeEnum.Live, temp.live],
      [DataTypeEnum.Today, temp.today],
      [DataTypeEnum.Early, temp.early]
    ]);
  };

  /**
   * 格式化体育盘口列表数据
   */
  const convertSportsHandicap = (
    list: any[],
    {
      periodId,
      sportId,
      showOdds,
      oldOdds
    }: { periodId: DataTypeEnum; sportId: SportIdEnum; showOdds: number[]; oldOdds: any }
  ) =>
    // type: string
    {
      // 是否滚球
      const isInProgress = periodId === DataTypeEnum.Live;
      // 暂时先注解掉
      // const filterList = filter(list, l => !isEmpty(filter(l.matches, m => m.ParentID === 0)));
      const formatedList = map(list, league => {
        return {
          leagueId: league.LeagueId,
          key: `${league.LeagueId}_${league.matches[0].MatchId}`,
          time: dayjs(league.matches[0].ShowTime).valueOf(),
          leagueName: league.LeagueName,
          enLeagueName: league.EnLeagueName,
          matches: map(league.matches, match => ({
            info: _formatMatchInfo({ match, league, isInProgress, sportId }),
            odds: _formatMatchOdds({ odds: match.oddset, showOdds, oldOdds })
          }))
        };
      });
      // let i = 0;
      // // 计算赔率变化
      // !isEmpty(store.getters[type]) &&
      //   map(formatedList, item => {
      //     const filterItem =
      //       type === 'sportMatchDetail' ? store.getters[type] : filter(store.getters[type], l => item.key === l.key)[0];
      //     !isEmpty(filterItem) &&
      //       map(item.matches, matches => {
      //         const filterMatches = filter(filterItem.matches, m => matches?.info.id === m.info.id)[0];
      //         !isEmpty(filterMatches) &&
      //           map(matches.odds, odds => {
      //             const filterOdds = filter(filterMatches?.odds, n => odds.betType === n.betType)[0];
      //             !isEmpty(filterOdds) &&
      //               map(odds.sels, sel => {
      //                 // 详情中波胆数据有特殊处理，store中的数据需要统一格式
      //                 const selsArr = isArray(filterOdds?.sels[0])
      //                   ? filterOdds?.sels.reduce((a, b) => a.concat(b))
      //                   : filterOdds?.sels;
      //                 const filterSels = filter(selsArr, q => q.keyName === sel.keyName)[0];
      //                 console.log(++i);
      //                 if (sel?.price && filterSels?.price) {
      //                   if (sel.price < filterSels.price) {
      //                     sel.change = 'reduce';
      //                   } else if (sel.price === filterSels.price) {
      //                     sel.change = 'smooth';
      //                   } else {
      //                     sel.change = 'rise';
      //                   }
      //                 }
      //               });
      //           });
      //       });
      //   });
      setTimeout(() => {
        oddTemp = {};
      });
      return { list: formatedList, odd: oddTemp };
    };

  /**
   *格式化赛事基本信息
   *
   * @param {*} { match, isInProgress, sportId }
   * @return {*}
   */
  function _formatMatchInfo({ match, league, isInProgress, sportId }: any) {
    //console.log({ match, isInProgress, sportId });
    const {
      score = {},
      date,
      timeText
    } = isInProgress
      ? _formatScoreAndTimeText({ match, sportId })
      : { date: dayjs(match.ShowTime).format('MM/DD'), timeText: dayjs(match.ShowTime).format('HH:mm') };
    return {
      home: match.HomeName,
      away: match.AwayName,
      leagueName: league.LeagueName,
      enLeagueName: league.EnLeagueName,
      id: match.MatchId,
      IsLive: match.IsLive || match.HasLive,
      count: match.MarketCount,
      date,
      timeText,
      ...score
    };
  }

  /**
   *格式化比分信息和时间文本
   *
   * @param {*} { match, isInProgress, sportId }
   */
  function _formatScoreAndTimeText({ match, sportId }: any) {
    return {
      score: _formatScore({ match, sportId }),
      date: null,
      timeText: eventTimeFormat({ ...match, ...match.MoreInfo, SportType: sportId })
    };
  }
  /**
   *格式化比分
   *
   * @param {*} {match, sportId}
   */
  function _formatScore({ match, sportId }) {
    // 主队分数 客队分数 目前进行到第几节 赛事总节数 目前赛事时间
    const { ScoreH, ScoreA } = match.MoreInfo;
    // console.log(LivePeriod, GameSession);
    // 足球
    if (sportId === SportIdEnum.Soccerball) {
      const { RedCardH, RedCardA, YellowCardH, YellowCardA } = match.MoreInfo;
      return {
        sportId: sportId,
        score: [ScoreH, ScoreA],
        tech: {
          red: [RedCardH, RedCardA],
          yellow: [YellowCardH, YellowCardA]
        }
      };
    }
    // 篮球
    if (sportId === SportIdEnum.Basketball) {
      const { A1Q, A2Q, A3Q, A4Q, H1Q, H2Q, H3Q, H4Q, LLP } = match.MoreInfo;
      return {
        sportId: sportId,
        score: [ScoreH, ScoreA],
        current: Number(LLP) - 1,
        sections: [
          [H1Q, A1Q],
          [H2Q, A2Q],
          [H3Q, A3Q],
          [H4Q, A4Q]
        ]
      };
    }
    //网球
    if (sportId === SportIdEnum.Tennis) {
      const { TennisAwayGameScore, TennisHomeGameScore, TennisHomePointScore, TennisAwayPointScore, TennisCurrentSet } =
        match.MoreInfo;
      return {
        sportId: sportId,
        score: [ScoreH, ScoreA],
        current: TennisCurrentSet - 1,
        pointScore: [TennisHomePointScore, TennisAwayPointScore],
        sections: map(TennisHomeGameScore, (v, k) => {
          return [v, TennisAwayGameScore[k]];
        })
      };
    }
    //排球、羽毛球、乒乓球
    if (
      sportId === SportIdEnum.Volleyball ||
      sportId === SportIdEnum.Badminton ||
      sportId === SportIdEnum.TableTennis
    ) {
      const { H1S, H2S, H3S, H4S, H5S, A1S, A2S, A3S, A4S, A5S, GetGamesH, GetGamesA, LLP } = match.MoreInfo;
      return {
        sportId: sportId,
        score: [ScoreH, ScoreA],
        current: Number(LLP) - 1,
        sections: [
          [H1S, A1S],
          [H2S, A2S],
          [H3S, A3S],
          [H4S, A4S],
          [H5S, A5S],
          [GetGamesH, GetGamesA]
        ]
      };
    }
  }
  /**
   *格式化赛事盘口信息
   *
   * @param {{ odds: any[]; showOdds: number[] }} { odds, sportId }
   * @return {*}
   */
  function _formatMatchOdds({ odds, showOdds, oldOdds }: { odds: any[]; showOdds: number[]; oldOdds: any }) {
    // const showOdd = FAST_BET_TYPES.get(sportId) || DEFAULT_BET_TYPES;
    return map(showOdds, id => {
      const oldOdd = sortBy(
        filter(odds, o => id === o.Bettype),
        s => -s.Sort
      )[0] || {
        Bettype: id
      };
      // 判断当前比赛是否有此玩法
      // 足球1x2独赢玩法
      // const odd =
      //   oldOdd.Bettype === 5
      //     ? { ...oldOdd, sels: [oldOdd.sels[0] || {}, oldOdd.sels[2] || {}, oldOdd.sels[1] || {}] }
      //     : { ...oldOdd, sels: isEmpty(oldOdd.sels) ? [{}, {}] : oldOdd.sels };
      const odd = { ...oldOdd, sels: _fixListOddSel(oldOdd.sels, id) };
      return {
        betType: odd.Bettype,
        sels: map(odd.sels, sel => {
          const change =
            sel.Price > oldOdds[`${odd.OddsId}_${sel.Key}`]
              ? 'rise'
              : sel.Price < oldOdds[`${odd.OddsId}_${sel.Key}`]
              ? 'reduce'
              : 'smooth';
          sel.Price && (oddTemp[`${odd.OddsId}_${sel.Key}`] = sel.Price);
          return sel.filled
            ? sel
            : {
                betType: odd.Bettype,
                betTypeName: odd.BettypeName,
                category: odd.Category,
                marketStatus: odd.MarketStatus,
                maxBet: odd.MaxBet,
                oddsId: odd.OddsId,
                oddsType: odd.oddsType,
                key: sel.Key,
                selKey: `${odd.OddsId}_${sel.Key}`,
                keyName: sel.KeyName,
                name: _formatSelName(odd.Bettype, sel),
                price: sel.Price,
                point: sel.Point,
                locked: odd.MarketStatus !== 'running' || sel.Price <= 0,
                change
              };
        })
      };
    });
  }

  /**
   *补充列表盘口
   *
   * @param {any[]} sels
   * @param {number} betType
   */
  function _fixListOddSel(sels: any[] | undefined, betType: number) {
    // 独赢处理
    if (betType === 5 || betType === 15) {
      return sels
        ? sels.length === 3
          ? [sels[0], sels[2], sels[1]]
          : fill(Array(3), { locked: true, filled: true })
        : fill(Array(3), { filled: true });
    }
    return sels
      ? sels.length > 0
        ? sels
        : fill(Array(2), { locked: true, filled: true })
      : fill(Array(2), { filled: true });
  }

  function _formatSelName(betType: number, { Key, KeyName, Point }: { Key: string; KeyName: string; Point: number }) {
    if ([1, 7].includes(betType)) {
      return Point;
    } else if (
      [
        // 单双类型
        2, 6, 12, 16, 9078,
        // 普通显示3列类型
        22, 707, 713,
        // 波胆类型
        405, 406, 412, 413, 414, 419,
        // 全场波胆类型
        408, 416, 467
      ].includes(betType)
    ) {
      return KeyName;
    } else if (
      [
        // 显示主客类型
        20, 21, 153, 154, 155, 501, 609, 612, 9001, 9002, 9006, 9007, 9008, 9011, 9046, 9049, 9052, 9055
      ].includes(betType)
    ) {
      return {
        h: '主',
        a: '客'
      }[Key];
    } else if (
      [
        // 大小盘类型
        3, 8, 156, 401, 402, 403, 404, 461, 462, 463, 464, 468, 469, 615, 616, 9410, 9411, 9428, 9429
      ].includes(betType)
    ) {
      return KeyName + (Point ? Point : '');
    } else if (
      [
        // 独赢类型
        5, 15, 28, 177, 430, 458, 459
      ].includes(betType)
    ) {
      return {
        '1': '主',
        x: '和',
        '2': '客'
      }[Key];
    } else if (
      [
        // 双重机会类型
        24
      ].includes(betType)
    ) {
      return {
        '1x': '主 或 和',
        '12': '主 或 客',
        '2x': '客 或 和'
      }[Key];
    } else if (
      [
        // 显示大小
        702, 705, 709, 9003, 9009, 9013, 9019, 9025, 9029, 9035, 9047, 9053, 9058, 9060, 9070
      ].includes(betType)
    ) {
      return {
        o: '大',
        u: '小'
      }[Key];
    } else {
      return KeyName || Point;
    }
  }

  /**
   * 格式化联赛列表数据
   */
  const convertLeagueList = (list: any[]) => {
    return list;
  };

  /**
   * 格式化比赛体育数据
   */
  const convertMatchInfo = (list: any[]) => {
    return list;
  };

  /**
   * 赛事详情-列表数据
   */
  const convertSportsDetailList = (
    list: any[],
    { periodId, sportId }: { periodId: DataTypeEnum; sportId: SportIdEnum }
  ) => {
    if (isEmpty(list) || !isArray(list) || isEmpty(list[0].matches) || !isArray(list[0].matches)) return {};
    const newList = _mergeSameData(list[0].matches[0].oddset);
    let formatedList = map(newList, item => {
      return {
        Bettype: item[0].Bettype,
        BettypeName: item[0].BettypeName,
        Category: item[0].Category,
        list: map(item, item2 => {
          return {
            OddsId: item2.OddsId,
            MaxBet: item2.MaxBet,
            sels: _setSels(item2.sels, item2.Bettype)
          };
        })
      };
    });
    //过滤掉空数据sels
    formatedList = formatedList.filter(x1 => !isEmpty(x1.list.filter(x2 => !isEmpty(x2.sels))));
    // 是否滚球
    const isInProgress = periodId === DataTypeEnum.Live;
    const match = list[0].matches[0];
    const result = {
      info: {
        ..._formatMatchInfo({ match, league: list[0], isInProgress, sportId }),
        LeagueName: list[0].LeagueName,
        EnLeagueName: list[0].EnLeagueName
      },
      odds: formatedList
    };
    console.log('pppppp', result);
    return result;
  };
  function _setSels(sels: any[], Bettype: number) {
    const arr = map(sels, item => {
      return {
        name: _formatSelName(Bettype, item),
        price: item.Price
      };
    });
    return arr;
  }
  /**
   * 将相同的Bettype放一个数组里面,并排序
   */
  function _mergeSameData(list: any[]) {
    const keyArr: Array<any> = [];
    const newArr: Array<any> = [];
    map(list, item => {
      if (!keyArr.includes(item.Bettype)) {
        keyArr.push(item.Bettype);
        newArr.push(
          list
            .filter(x => x.Bettype === item.Bettype)
            .sort((a, b) => {
              return a.Sort - b.Sort;
            })
        );
      }
    });
    return newArr;
  }
  return {
    convertSportsSort,
    convertSportsSort_v2,
    convertLeagueList,
    convertMatchInfo,
    convertSportsHandicap,
    convertSportsDetailList
  };
}
