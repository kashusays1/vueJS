import { Client, Message, messageCallbackType, StompConfig } from '@stomp/stompjs';
import { Fn } from '#/global';
import { Ref, ref, computed } from 'vue';
import { getLoginData, getTenant, toTime, getWebsocketApi } from '@/common/utils';
import { useStore } from 'vuex';
import dayjs from 'dayjs';
import { Toast } from 'vant';
import { remove, isArray, isObject } from 'lodash-es';

const WS_URL = getWebsocketApi() + '/anchor/websocket';

export interface MessageItem {
  id: string;
  uid: string;
  msg: string;
  cmd?: string;
  account?: string;
  amountDifference?: number;
  anchorId?: string;
  cycle?: number;
  chId?: string;
  createTime?: string;
  createTimeL?: string;
  msgType?: number;
  nick?: string;
  role?: number;
  tenant: string;
  time?: string;
  vip?: number;
}

export type CmdTypes = 'M01' | 'M02' | 'M03' | 'M04' | 'R00' | 'L03' | 'R03' | 'L01' | 'D01' | 'DCLEAN';

// L03 频道历史消息
// M01 频道内消息
// L02 频道内球队点赞数，
// R01 token格式错误
// R02 tenant数据错误
// R03 用户被禁言
// L01 欢迎进入直播间
// M02 消息过于频繁
// R04 作废
// R08 用户踢出
// R09 用户已被踢出，不能登出或发言
// M04 限制条件
// A01 点赞球队
// G01 接受礼物
// G02 G03 o1退出聊天室
// R07 用户禁言时间到期
// R00 用户发言内容长度超出限制
// D01 删除对应Id的单条消息
// O1 退出聊天室
// DCLEAN 清屏删除当前指令之前的所有消息
// 广播订阅url: /topic/当前租户.赛事id.主播id.message   示例：/topic/ylbl.4445847.34.message
// 用户私有消息订阅url: /topic/user.租户标识.用户账号.message   示例： /topic/user.ylbl.ceshi123.message
// 发消息url: /message

interface UseChatRn {
  /**
   * 是否锁定输入
   */
  isLockInput: Ref<boolean>;
  /**
   * 需要充值的金额(达到才能聊天)
   */
  needRrchargeAmount: Ref<number>;
  /**
   * 是否链接
   */
  isConnected: Ref<boolean>;
  /**
   * 初始化聊天
   */
  initChat: Fn;
  /**
   * 聊天信息列表
   */
  messageList: Ref<MessageItem[]>;
  /**
   * 发送消息
   */
  sendMessage: (msg: string) => void;
  /**
   * 关闭聊天
   */
  closeChat: Fn;
  /**
   * 未读消息
   */
  unreadCount: Ref<number>;
  /**
   * 订阅
   */
  subscribe: (
    { destination, matchId, options }: { destination: string; matchId?: string | number; options?: Object },
    fn?: messageCallbackType
  ) => void;
  /**
   * 取消订阅
   */
  unSubscribe: ({ destination, id }: { destination: string; id: string }) => void;
  clearUnreadCount: Fn;
}

export function useChat({ matchId }: { matchId: number }): UseChatRn {
  const matchIdStr = String(matchId);
  const store = useStore();
  const userInfo = computed(() => store.state.user);

  const isLockInput: Ref<boolean> = ref(false);
  const needRrchargeAmount: Ref<number> = ref(0);

  const { token = '', uid = '', username = '' } = getLoginData() as any; // 获取令牌

  const unreadCount = ref(0);

  const isConnected = ref(false);
  const subscribeList: Ref<any[]> = ref([]);

  const headers = {
    tenant: getTenant(),
    token: token,
    uid: uid,
    username: username,
    userType: '',
    isFirst: '1',
    guestTeamId: matchIdStr,
    homeTeamId: matchIdStr,
    chId: matchIdStr
  };

  const messageList: Ref<any[]> = ref([]);

  // 聊天配置
  const chatConfig = (): StompConfig => {
    return {
      connectHeaders: {
        token,
        username,
        tenant: getTenant()
      },
      brokerURL: WS_URL,
      reconnectDelay: 2000,
      heartbeatIncoming: 210 * 1000,
      heartbeatOutgoing: 210 * 1000,
      onConnect: () => {
        isConnected.value = window.WSclient.connected;
        console.log(
          '%c===============================\n= Chat Server Connected Success! =\n===============================',
          'font-size:14px;color:green'
        );
      },
      onStompError: (error: any) => {
        console.warn(
          '%c===============================\n= Chat Server Connected Fail! =\n===============================',
          'font-size:14px;color:red'
        );
        console.error(error);
      },
      onDisconnect: (error: any) => {
        console.error('断开了 ws', error);
        isConnected.value = window.WSclient.connected;
      }
    };
  };

  // 发送消息
  const sendMessage = (msg: string) => {
    if (!msg) return;
    const { nickName, username, vipLevel, id } = userInfo.value;
    const data = {
      cmd: 'M01',
      data: {
        account: username,
        chId: matchIdStr,
        createTimeL: dayjs().valueOf(),
        ext: null,
        msg: msg,
        msgType: 1,
        nick: nickName,
        role: 0,
        time: 0,
        token: token,
        uid: id,
        vip: vipLevel
      }
    };
    try {
      window.WSclient.publish({
        destination: '/message',
        body: JSON.stringify(data),
        headers: headers
      });
    } catch (error) {
      console.error(error);
    }
  };

  // 订阅
  const subscribe = ({ destination, options }, fn?: messageCallbackType) => {
    subscribeList.value.push(destination);
    const msgCb = Boolean(fn) ? fn : ({ body }: Message) => onMessage(JSON.parse(body));
    try {
      window?.WSclient?.subscribe(destination, msgCb, { ...headers, ...options });
    } catch (error) {
      console.error(error);
    }
  };

  // 取消全部订阅
  const unSubscribe = async () => {
    subscribeList.value.forEach((dest, index) => {
      window?.WSclient?.unsubscribe(`sub-${index}`, {
        ...headers,
        destination: dest
      });
    });
  };

  // 关闭聊天
  const closeChat = async () => {
    await unSubscribe();
    await window?.WSclient?.deactivate();
  };

  // 初始化聊天
  const initChat = () => {
    window.WSclient = new Client(chatConfig());
    window.WSclient.activate();
  };

  const delMsg = (id: number) => {
    remove(messageList.value, e => e.id === id);
  };

  const clearUnreadCount = () => {
    unreadCount.value = 0;
  };

  // 消息接收
  const onMessage = async (chatData: { cmd: CmdTypes; data: any }) => {
    const { cmd, data } = chatData;
    if (cmd === 'M02') {
      messageList.value.push({
        cmd: 'M02',
        msg: '您的发送频率太快了',
        time: toTime(new Date(), 'HH:mm:ss')
      });
    }
    if (!data) return;
    try {
      if (cmd === 'R00') {
        Toast('发言内容长度超出限制');
      }
      if (cmd === 'M01' && !isArray(data)) {
        unreadCount.value = unreadCount.value + 1;
        const msgItem = {
          cmd,
          ...data
        };
        messageList.value.push(msgItem);
      }
      if (cmd === 'L03' && isArray(data)) {
        data.map(e => (e.cmd = 'L03'));
        messageList.value = data.reverse();
      }
      if (cmd === 'R03') {
        const isMe = !!data.uid && String(data.uid) === String(uid);
        if (isMe) {
          messageList.value.push({
            isMe,
            cmd: 'R03',
            nick: data.nick || data.username,
            jinyanTime: data.jinyanTime,
            time: data.createTime
          });
        } else {
          messageList.value.push({
            cmd: 'R03',
            nick: data.nick || data.username,
            jinyanTime: data.jinyanTime,
            time: data.createTime
          });
        }
      }
      if (cmd === 'M04' && isObject(data)) {
        messageList.value.push({
          cmd: 'M04',
          msg: `${data.cycle}天内，累计充值满${data.amount}元才能发言`,
          time: toTime(new Date(), 'HH:mm:ss')
        });
        isLockInput.value = true;
        needRrchargeAmount.value = data.amount;
        return;
      }
      // 删除单条消息
      if (cmd === 'D01' && !!data) {
        const msgId = data.id;
        delMsg(msgId);
      }
      // 删除消息
      if (cmd === 'DCLEAN' && !!data) {
        messageList.value = [];
      }
      if (cmd === 'M03') {
        const isMe = true;
        // 禁言信息
        if (isMe) {
          Toast(`您已被禁言，禁言剩余时间${data}分钟`);
        }
      } else {
        isLockInput.value = false;
      }
      // if (cmd === 'R03' && !!data) {
      //   Toast('已被禁言');
      // }
      if (messageList.value.length < 200) return;
      setTimeout(() => {
        messageList.value.splice(0, 1);
      }, 300);
    } catch (ex) {
      console.error('msg parse failed', ex);
    }
  };

  return {
    needRrchargeAmount,
    isConnected,
    messageList,
    sendMessage,
    unSubscribe,
    unreadCount,
    isLockInput,
    clearUnreadCount,
    closeChat,
    subscribe,
    initChat
  };
}
