import { CommonTypeItem, ParlayComboType } from '@/common';
import { ParlayCombosItem } from '@/services';
import { isEmpty } from 'lodash-es';

// 串名字combos格式化
export const combosLabel = (combo: ParlayCombosItem | undefined) => {
  if (!combo || isEmpty(combo)) return '';
  const { bet_count, combo_type } = combo;
  const parlayObj = ParlayComboType.find((e: CommonTypeItem) => e.code === combo_type) as any;
  return `${parlayObj.label}  *${bet_count}`;
};
