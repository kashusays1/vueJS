/**
 * 体育枚举
 */

// 赛事区间 市场类型
export enum DataTypeEnum {
  /**
   * 全部
   */
  All = 0,
  /**
   * 今日
   */
  Today = 1,
  /**
   * 早盘
   */
  Early = 2,
  /**
   * 滚球
   */
  Live = 3,
  /**
   * 串关
   */
  Parlay = 4,
  /**
   * 冠军盘
   */
  Champion = 5,
  /**
   * 关注
   */
  Followed = -1,
  /**
   * 初始化
   */
  Init = -10
}

/**
 * MarketStatus
 * 盘口状态
 */

export enum MarketStatusEnum {
  /**
   * 盘口开启且可下注
   */
  running = 0,
  /**
   * 盘口赔率仍开启但无法下注
   */
  suspend = 1,
  /**
   *  盘口关闭且无法下注
   */
  closed = 2,
  /**
   * 盘口赔率关闭且无法下注
   */
  closeprice = 3
}

// 投注类型
export enum BetTypeEnum {
  // 让球
  spreads = 1,
  // 单双
  oddOrEven = 2,
  // 大小
  jolly = 3,
  // 独赢
  proposition = 5,
  // 上半场让球
  firstHalfHandicap = 7,
  // 下半场让球
  secondHalfHandicap = 17,
  // 上半场.单双盘
  firstHalfOddAndDoubleSet = 12,
  // 胜负
  windOrLose = 20,
  // 获胜者
  winner = 501,
  // 角球
  corner = 473,
  // 板球获胜者
  cricketwinner = 501
}

// 筛选tab类型
export type TabTypeKeys = 'periodId' | 'sportId' | 'betType' | 'leagueIds';

export interface CommonTypeItem {
  label: string;
  code: string;
  // 国际化
  i18n?: string;
  value?: any;
  index?: string;
  count?: number;
  sportVisibleEnum?: number[];
}

export const PeriodI18n = {
  1: 'lang.sport_handicap_today', // 今日
  2: 'lang.sport_earlyHandicap', // 早盘
  3: 'lang.sport_handicap_live', // 滚球
  4: 'lang.sport_bet_stray', // 串关
  '-1': 'lang.sport_handicap_focus' // 关注
};

/**
 * 投注类型列表
 * live: 滚球, today: 今日 , early: 早盘,
 */
export const DATE_TYPES: any[] = [
  {
    label: PeriodI18n[DataTypeEnum.Live],
    i18n: 'lang.sport_handicap_live',
    code: 'live',
    value: DataTypeEnum.Live,
    index: 'l'
  },
  {
    label: PeriodI18n[DataTypeEnum.Today],
    i18n: 'lang.sport_handicap_today',
    code: 'today',
    value: DataTypeEnum.Today,
    index: 't'
  },
  {
    label: PeriodI18n[DataTypeEnum.Early],
    i18n: 'lang.sport_earlyHandicap',
    code: 'early',
    value: DataTypeEnum.Early,
    index: 'e'
  }
];

/**
 * 玩法列表
 * spreads:让球,  jolly: 大小,  proposition: 独赢,  corner:角球,
 */
export const BetTypeList: CommonTypeItem[] = [
  {
    label: '让球',
    code: 'spreads',
    value: BetTypeEnum.spreads,
    sportVisibleEnum: [1, 2, 3, 4, 5, 6, 7, 8, 12, 17, 24, 26, 33, 35, 37, 43, 45]
  },
  {
    label: '大小',
    code: 'jolly',
    value: BetTypeEnum.jolly,
    sportVisibleEnum: [1, 2, 3, 4, 5, 24, 17, 43, 26, 33, 35, 37, 45, 56]
  },
  {
    label: '独赢', // 胜负
    code: 'windOrLose',
    value: BetTypeEnum.windOrLose,
    sportVisibleEnum: [
      4, 8, 6, 9, 10, 11, 12, 14, 15, 16, 18, 19, 21, 22, 23, 28, 33, 34, 37, 38, 39, 41, 44, 53, 55, 56, 99
    ]
  },
  {
    label: '独赢',
    code: 'proposition',
    value: BetTypeEnum.proposition,
    sportVisibleEnum: [1, 7, 43]
  },
  // {
  //   label: '获胜者',
  //   code: 'winner',
  //   value: BetTypeEnum.winner,
  //   sportVisibleEnum: []
  // },
  {
    label: '获胜者', // 特指板球
    code: 'cricketwinner',
    value: BetTypeEnum.cricketwinner,
    sportVisibleEnum: [50]
  }
  // {
  //   label: '角球',
  //   code: 'corner',
  //   value: BetTypeEnum['corner']
  // }
];

// 结算状态
// 注单状态 已结算 8:赢半 6:输半 7. won (赢)5. lose (输)9. draw (平)未结算2. running (进行中)1. waiting (等待)其他15. reject(拒绝)16. refund（退款）4. void (取消)
// 已结算 8. WinHalf(赢半) 6. LoseHalf(输半) 7. won (赢)5. lose (输)9. draw (平)
// 未结算 2. running (进行中) 1. waiting (等待)其他  15. reject(拒绝)  16. refund（退款） 4. void (取消)
export type SettlementType = 'won' | 'lose' | 'draw' | 'running' | 'waiting' | 'reject' | 'refund' | 'void';

// 结算状态枚举
export enum SettlementEnum {
  won = 7,
  lose = 5,
  draw = 9,
  running = 2,
  waiting = 1,
  reject = 15,
  refund = 16,
  void = 4,
  LoseHalf = 6,
  WinHalf = 8
}

export const SettlementList: CommonTypeItem[] = [
  {
    i18n: 'lang.sport_orders_loseHalf',
    label: '输半',
    code: 'LoseHalf',
    value: SettlementEnum.LoseHalf
  },
  {
    i18n: 'lang.sport_orders_winHalf',
    label: '赢半',
    code: 'WinHalf',
    value: SettlementEnum.WinHalf
  },
  {
    i18n: 'lang.sport_orders_won',
    label: '胜',
    code: 'won',
    value: SettlementEnum.won
  },
  {
    i18n: 'lang.sport_details_lost',
    label: '输',
    code: 'lose',
    value: SettlementEnum.lose
  },
  {
    i18n: 'lang.sport_common_tie',
    label: '和',
    code: 'draw',
    value: SettlementEnum.draw
  },
  {
    i18n: 'lang.sport_bet_betSucced',
    label: '投注成功',
    code: 'running',
    value: SettlementEnum.running
  },
  {
    i18n: 'lang.sport_orders_waiting',
    label: '待确认',
    code: 'waiting',
    value: SettlementEnum.waiting
  },
  {
    i18n: 'lang.sport_orders_reject',
    label: '投注拒绝',
    code: 'reject',
    value: SettlementEnum.reject
  },
  {
    i18n: 'lang.sport_orders_refund',
    label: '退款',
    code: 'refund',
    value: SettlementEnum.refund
  },
  {
    i18n: 'lang.sport_common_cancel',
    label: '取消',
    code: 'void',
    value: SettlementEnum.void
  }
];

// 赛事状态
// Closed/ Running/ Postponed/ Completed

export const GameStatus = [
  {
    label: '比赛关闭',
    code: 'Closed'
  },
  {
    label: '进行中',
    code: 'Running'
  },
  {
    label: '延期',
    code: 'Postponed'
  },
  {
    label: '已结束',
    code: '已结束'
  }
];

/**
 * 投注类型分类
 */
// 0: None (主要)
// 1: FullTime (全场)
// 2: Half (半场)
// 3: Corners (角球)
// 4: Bookings (罚牌)
// 5: Intervals (区间)
// 6: Specials (特别产品)
// 7: Players (选手)
// 8: ExtraTime (加时)
// 9: FastMarket 快速
// 10: Quarter (节)
// 11~19: E-Sports Map 1~9 (电子竞技 适用 – 地图1~9)

export const BetCategoryList = [
  {
    label: '主要盘口',
    code: 'None',
    value: 0
  },
  {
    label: '全场',
    code: 'FullTime',
    value: 1
  },
  {
    label: '半场',
    code: 'Half',
    value: 2
  },
  {
    label: '角球',
    code: 'Corners',
    value: 3
  },
  {
    label: '罚牌',
    code: 'Bookings',
    value: 4
  },
  {
    label: '区间',
    code: 'Intervals',
    value: 5
  },
  {
    label: '特别产品',
    code: 'Specials',
    value: 6
  },
  {
    label: '选手',
    code: 'Players',
    value: 7
  },
  {
    label: '节',
    code: 'ExtraTime',
    value: 8
  },
  {
    label: '快速',
    code: 'FastMarket',
    value: 9
  },
  {
    label: '节',
    code: 'Quarter',
    value: 10
  }
];

// LivePeriod
// int
// 目前进行到第几节
// 共享

// GameSession
// byte
// 赛事节数
// 共享: 比赛有多少节

// InPlayTime
// string
// 目前赛事时间
// 足球(1)/篮球(2)/美式 足球(3)/冰上曲棍球 (4)

// ScoreA
// int
// 客队分数
// 共享

// ScoreH
// int
// 主队分数
// 共享

// InjuryTime
// short
// 加时时间
// 足球

// RedCardA
// short
// 客队红牌数
// 足球

// RedCardH
// short
// 主队红牌数
// 足球

// YellowCardA
// short
// 客队黃牌数
// 足球

// YellowCardH
// short
// 主队黃牌数
// 足球

// A1S A2S A3S
// string
// 客队第1局比分 – 客队 第5局比分
// 羽球
// 47 / 242
//            A4S A5S
//            H1S H2S H3S H4S H5S
// string
// 主队第1局比分- 主队第 5 局比分
// 羽球
//          ConfirmFlag
// string
// 分数是否确认
// 羽球
//         GetGamesA
// LLP
// string
// 客队总局数
// 羽球
//         GetGamesH
// string
// 主队总局数
// 羽球
//         string
// 目前进行的局数/节数
// 羽球/篮球
//         Serve
// string
// 目前发球方
// 羽球
//         Injury
// string
// 伤停方
// 网球-伤停
//         IsBreak
// byte
// 比赛是否暂停
// 网球
// TennisAwayGameScore
// int
// 客队每盘获得局数
// 网球
//         TennisAwayPointScore
// string
// 客队目前局数比分
// 网球
//         TennisCurrentServe
// int
// 目前发球方
// 网球
//         TennisCurrentSet
// int
// 目前盘数
// 网球
//         TennisHomeGameScore
// int
// 主队每盘获得局数
// 网球
//         TennisHomePointScore
// string
// 主队目前局数比分
// 网球
//            A1Q A2Q A3Q A4Q
// string
// 客队第一节比分 – 客队 第四节比分
// 篮球
//     48 / 242

// H1Q
// H2Q H3Q H4Q
// string
// 主队第一节比分-主队第 四节比分
// 篮球

// OverTimeA
// string
// 客队延长赛比分
// 篮球

// OverTimeH
// string
// 主队延长赛比分
// 篮球

/**
 * Parlay Combo Type (串关组合列表)
 */
export const ParlayComboType = [
  {
    label: '2串1',
    code: 'Doubles',
    value: 0
  },
  {
    label: '3串1',
    code: 'Trebles',
    value: 1
  },
  {
    label: '3串1',
    code: 'Treble',
    value: 1
  },
  {
    label: '3串4',
    code: 'Trixie',
    value: 2
  },
  {
    label: '幸运7',
    code: 'Lucky7',
    value: 3
  },
  {
    label: '4串1',
    code: 'Fold4',
    value: 4
  },
  {
    label: '4串1',
    code: 'FourFolds',
    value: 4
  },
  {
    label: '4串1',
    code: '4-Fold',
    value: 4
  },
  {
    label: '洋基',
    code: 'Yankee',
    value: 5
  },
  {
    label: '幸运15',
    code: 'Lucky15',
    value: 6
  },
  {
    label: '5串1',
    code: 'Fold5',
    value: 7
  },
  {
    label: '5串1',
    code: '5-Fold',
    value: 7
  },
  {
    label: '5串1',
    code: 'FiveFolds',
    value: 7
  },
  {
    label: '超级美国佬',
    code: 'Canadian',
    value: 8
  },
  {
    label: '幸运 31',
    code: 'Lucky31',
    value: 9
  },
  {
    label: '6串1',
    code: 'Fold6',
    value: 10
  },
  {
    label: '6串1',
    code: '6-Fold',
    value: 10
  },
  {
    label: '亨氏',
    code: 'Heinz',
    value: 11
  },
  {
    label: '幸运 63',
    code: 'Lucky63',
    value: 12
  },
  {
    label: '7串1',
    code: 'Fold7',
    value: 13
  },
  {
    label: '7串1',
    code: '7-Fold',
    value: 13
  },
  {
    label: '超级亨氏',
    code: 'SuperHeinz',
    value: 14
  },
  {
    label: '幸运 127',
    code: 'Lucky127',
    value: 15
  },
  {
    label: '8串1',
    code: 'Fold8',
    value: 16
  },
  {
    label: '8串1',
    code: '8-Fold',
    value: 16
  },
  {
    label: '大亨',
    code: 'Goliath',
    value: 17
  },
  {
    label: '幸运255',
    code: 'Lucky255',
    value: 18
  },
  {
    label: '9串1',
    code: 'Fold9',
    value: 19
  },
  {
    label: '9串1',
    code: '9-Fold',
    value: 19
  },
  {
    label: '10串1',
    code: 'Fold10',
    value: 20
  },
  {
    label: '10串1',
    code: '10Fold',
    value: 20
  },
  {
    label: '10串1',
    code: '10-Fold',
    value: 20
  },
  {
    label: '11串1',
    code: 'Fold11',
    value: 21
  }
];

export const PreBetError = [
  {
    // label: '找不到该会员资料',
    label: '盘口关闭',
    code: 'Customer not found',
    value: 3
  },
  {
    label: '球头已不存在',
    code: 'Point isUtil expired',
    value: 6
  },
  {
    label: '赛事 ID 错误或是赛事已关',
    code: 'Match closed or invalid Market ID',
    value: 7
  },
  {
    label: '赔率正在更新',
    code: 'Odds suspend',
    value: 12
  },
  {
    label: '余额不足',
    code: 'Credit problem',
    value: 14
  },
  {
    label: '超过单场投注限额',
    code: 'Over max per match',
    value: 15
  },
  {
    label: '盘口关闭',
    code: 'Close price',
    value: 24
  },
  {
    label: '盘口关闭',
    code: 'Market closed',
    value: 25
  },
  {
    label: '赔率变更',
    code: 'Odds changed',
    value: 8
  }
];

export const DoBetError = [
  {
    label: '执行失败',
    code: 'Failed execution',
    value: 1
  },
  {
    label: '找不到该会员资料',
    code: 'Customer not found',
    value: 3
  },
  {
    label: '厂商注单号码重复',
    code: 'Vendor_Trans_ID Duplicated',
    value: 4
  },
  {
    label: 'SQL 执行错误',
    code: 'SQL Exception',
    value: 5
  },
  {
    label: '球头已不存在',
    code: 'Point is expired',
    value: 6
  },
  {
    label: '赛事 ID 错误或是赛事已关闭',
    code: 'Match closed or invalid Market ID',
    value: 7
  },
  {
    label: '赔率变更',
    code: 'Odds changed',
    value: 8
  },
  {
    label: '厂商标识符错误',
    code: 'Invalidate vendor_id',
    value: 9
  },
  {
    label: '系统维护中',
    code: 'Under maintenance',
    value: 10
  },
  {
    label: '超过最大/最小投注限额',
    code: 'Stake problem',
    value: 11
  },
  {
    label: '赔率正在调整',
    code: 'Odds suspend',
    value: 12
  },
  {
    label: '余额不足',
    code: 'Credit problem',
    value: 14
  },
  {
    label: '超过每场下注限额',
    code: 'Over max per match',
    value: 15
  },
  {
    label: '超过单注下注限额',
    code: 'Over max per bet',
    value: 16
  },
  {
    label: '赔率错误',
    code: 'Odds errorn',
    value: 17
  },
  {
    label: '低于最低串关赛事数量',
    code: 'Under can bet count',
    value: 19
  },
  {
    label: '串关错误',
    code: 'Lucky single problem',
    value: 20
  },
  {
    label: '账号无法投注',
    code: 'Diabled bet',
    value: 21
  },
  {
    label: '赛事比分改变',
    code: 'Score changed',
    value: 22
  },
  {
    label: '赛事球头改变',
    code: 'Point changed',
    value: 23
  },
  {
    label: '盘口暂时关闭',
    code: 'Close price',
    value: 24
  },
  {
    label: '盘口关闭',
    code: 'Market closed',
    value: 25
  }
];

//实时直播  数据源的处理
export enum ObjectName {
  'zero' = 0, //体育类型。0，1标识足球 。 0，2标识篮球
  'one' = 1, //进球
  'two' = 2, //角球
  'three' = 3, //黄牌
  'four' = 4, //红牌
  'five' = 5, //界外球
  'six' = 6, //任意球
  'seven' = 7, //球门球
  'eight' = 8, //点球
  'nine' = 9, //换人
  'ten' = 10, //比赛开始
  'eleven' = 11, //中场
  'twelve' = 12, //结束
  'thirteen' = 13, //半场比分
  'fifteen' = 15, //两黄变红
  'sixteen' = 16, //点球未进
  'seventeen' = 17, //乌龙球
  'nineteen' = 19, //伤停补时
  'twentyone' = 21, //射正
  'twentytwo' = 22, //射偏
  'twentythree' = 23, //进攻
  'twentyfour' = 24, //危险进攻
  'twentyfive' = 25, //控球率
  'twentysix' = 26, //加时赛结束
  'twentyseven' = 27, //点球大战结束
  'twentyeight' = 28, //比赛时间。
  'twentynine' = 29, //比赛状态。 29，1标识未开始
  'thirty' = 30, //当前比赛进行时间 足球
  'thirtyone' = 31 //当前小节剩余时间（篮球）
}

/**
 * 列表排序方式
 * @export
 * @enum {number}
 */
export enum SortTypeEnum {
  League = 'league',
  Time = 'time'
}
