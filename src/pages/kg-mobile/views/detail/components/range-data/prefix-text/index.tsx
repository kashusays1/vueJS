import { defineComponent } from 'vue';
import style from './prefix-text.module.less';

export default defineComponent({
  setup(props, { slots }) {
    props;
    return () => <span class={style.container}>{slots.default?.()}</span>;
  }
});
