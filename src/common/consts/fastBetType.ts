import { SportIdEnum } from '../enums';

export const FAST_BET_TYPES = new Map([
  // 独赢 让球 大小
  [SportIdEnum.Soccerball, [5, 1, 3, 15, 7, 8]],
  //   让球 大小
  [SportIdEnum.Basketball, [1, 3]]
]);

// 键: 运动类型 值:Bettype 盘口类型
// 默认让球 大小
export const DEFAULT_BET_TYPES = [1, 3];
