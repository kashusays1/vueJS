import { PreferenceEnum, MarketType } from '@/common';
import { provide, defineComponent, type PropType, type InjectionKey, type ExtractPropTypes } from 'vue';

export type ConfigProviderProvide = {
  iconPrefix?: string;
};

export const CONFIG_PROVIDER_KEY: InjectionKey<ConfigProviderProvide> = Symbol('CONFIG_PROVIDER_SP');

interface OptionsSeting {
  /**
   * 投注设置
   */
  oddsType: MarketType;
  /**
   *
   */
  oddsPreference: PreferenceEnum;
}

const configProviderProps = {
  themeVars: Object as PropType<Record<string, number>>,
  themeName: String,
  iconPrefix: String,
  sportOptions: Object as PropType<OptionsSeting>
};

export type ConfigProviderProps = ExtractPropTypes<typeof configProviderProps>;

export default defineComponent({
  name: 'CONFIG_PROVIDER',

  props: configProviderProps,

  setup(props, { slots }) {
    provide(CONFIG_PROVIDER_KEY, props);
    return () => slots.default?.();
  }
});
