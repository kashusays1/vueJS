import { defineComponent } from 'vue';
import { HeaderBalance } from 'kg-mobile/compoents';
import './header.less';
import { useAuth, useI18n } from '@/hooks';

export default defineComponent({
  name: 'Header',
  setup() {
    const { isLogin, account, toLogin, toMobileRegedit } = useAuth();
    const { t } = useI18n();

    return () => (
      <div class="home-header">
        <div class="home-header-logo"></div>
        {isLogin.value ? (
          <div class="home-header-user-info">
            <div class="nick-name">
              <span>{account.value || '--'}</span>
            </div>
            <HeaderBalance />
          </div>
        ) : (
          <div class="home-header-auth-login">
            <div class="btn" onClick={() => toLogin()}>
              <div class="icon-lo" />
              {/* 登录 */}
              <span>{t('lang.sport_home_login')}</span>
            </div>
            <div class="btn" onClick={() => toMobileRegedit()}>
              <div class="icon-reg" />
              {/* 注册 */}
              <span>{t('lang.sport_home_register')}</span>
            </div>
          </div>
        )}
      </div>
    );
  }
});
