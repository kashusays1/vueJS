import { defineComponent, PropType } from 'vue';
import emptyStyle from './empty.module.less';
import NO_OTHER_IMG from './assets/no_other.png';
import NO_MSG_IMG from './assets/no_mesaage.png';
import NO_RECORD_IMG from './assets/no_record.png';

const IMGS = {
  other: NO_OTHER_IMG,
  message: NO_MSG_IMG,
  record: NO_RECORD_IMG
};

export default defineComponent({
  name: 'DataEmpty',
  props: {
    desc: {
      default: '',
      type: String
    },
    title: {
      default: '',
      type: String
    },
    type: {
      default: 'other',
      type: String as PropType<'other' | 'message' | 'record'>
    }
  },
  setup(props) {
    return () => (
      <div class={emptyStyle.empty}>
        <img src={IMGS[props.type]} />
        <div class={emptyStyle.dsc}>
          <span>{props.title}</span>
          <span>{props.desc}</span>
        </div>
      </div>
    );
  }
});
