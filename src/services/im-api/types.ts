import { DataTypeEnum, MarketType } from '@/common/enums';
export interface EventSummaryApi {
  /**
   * 早盘赛事数量
   */
  early: number;
  /**
   * 滚球赛事数量
   */
  live: number;
  /**
   * 串关赛事数量
   */
  parlay: number;
  /**
   * 串关赛事数量
   */
  follow: number;
  /**
   * 运动项目 ID
   */
  sport_type: number;
  /**
   * 体育名称
   */
  sport_type_name: string;
  /**
   * 今日赛事数量
   */
  today: number;
}

export interface ClassifyItem {
  /**
   * id
   */
  classifyId: number;
  /**
   * 分类名字
   */
  classifyName: string;
}

export type OddTypsList = 1 | 2 | 3 | 4 | 5;
export interface EventSummary {
  /**
   * 早盘赛事数量
   */
  early: number;
  /**
   * 滚球赛事数量
   */
  live: number;
  /**
   * 串关赛事数量
   */
  parlay: number;
  /**
   * 串关赛事数量
   */
  follow: number;
  /**
   * 运动项目 ID
   */
  sport_type: number;
  /**
   * 体育名称
   */
  sport_type_name: string;
  /**
   * 今日赛事数量
   */
  today: number;
}

export interface GetVirtualEventVideoForm {
  // 体育项目ID
  sportType: number;
  // 视频ID,从GetEvents 取得
  streamingOption: number;
  // 视频代码,从GetEvents 取得
  channelCode: string;
}

export interface EventsAndMarketsPrams {
  oddsType: OddTypsList;
  // 体育项目ID
  sportId: string | number;
  // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球 -1:关注 4: 滚球
  periodId: DataTypeEnum;
  // 查询联赛数据条数
  queryCount?: number;
  // 联赛ids,以,隔开
  leagueIds?: string | null;
  // 查询向后指定天数偏移的matches(赛事)数据，传值范围：1-7，当periodId传值为2时，此值才会生效。
  selectDate?: number | null;
  // 指定赛事 ID 来获取赛事状态(包括半场比分数据)
  eventIds?: string | number;
  // 是否为串关
  isParlay?: boolean;
}

export interface MatchEventsPrams {
  // 1、足球 2、篮球 3、美式足球 4、冰上曲棍球 5、网球 6、排球
  sportId: number;
  // e: Early(早盘)、 l: Live(滚球) 、d: Dead ball(死球：包含早盘与今日赛事)、 t: Today(今日)
  marketType: string;
  // 赛事ID(不传会查全部赛事)
  eventIds?: string;
  betType?: number;
  oddsType?: OddTypsList;
}

export interface EventsAndMarkets {
  LeagueId: string;
  LeagueName: string;
  SportName: string;
  SportType: number;
  matches: MatchItem[];
}

/**
 * 单投预投注参数
 */

export interface IMsingleTicketPrams {
  // 参考比赛与盘口接口Bettype
  bettype: number;
  // 比赛id
  matchId: number;
  // 体育类型
  sport_type: number;
  // 盘口类型
  odds_type: string;
  // 赔率
  price: string;
  // 投注选项的特定 ID
  wagerSelectionId: number;
  // 参考赛事流OddsId
  market_id: number;
  // 比赛时段 ID 1 = 全场 2 = 上半场 3 = 下半场
  periodId: number;
  // 该项目仅适用于让球盘和大小盘.（参考比赛与盘口接口Point）
  point: number;
  // 其他下注资料
  specifiers?: string;
  // 若设置为 :而当前的让球盘选项不再开出, API 将返回投 注选项资料最接近的让球盘.
  returnNearestHandicap: boolean;
  key: number;
  // 参考ID.仅对多个单注是强制性的.（目前不用传值）
  refId?: string;
  // 优胜冠军类型 ID. （只适用于优胜冠军，如果定时赛事会 是0），目前写死（0）
  outrightTeamId?: number;
}

export interface MatchMoreInfo {
  GameSession: null | number;
  GetGamesA: number;
  GetGamesH: number;
  InPlayTime: string;
  LLP: number | string;
  LivePeriod: null | number;
  RedCardA: number;
  RedCardH: number;
  ScoreA: number;
  ScoreH: number;
  TennisAwayGameScore: number[];
  TennisHomeGameScore: number[];
  TennisAwayPointScore: number | string;
  TennisCurrentSet: number | string;
  TennisHomePointScore: number | string;
  YellowCardA: null | number;
  YellowCardH: null | number;
  A1Q: null | number;
  A2Q: null | number;
  A3Q: null | number;
  A4Q: null | number;
  H1Q: null | number;
  H2Q: null | number;
  H3Q: null | number;
  H4Q: null | number;
  A1S: null | number;
  A2S: null | number;
  H1S: null | number;
  H2S: null | number;
}

export interface MatchItem {
  AwayID: number;
  SportType?: number;
  AwayName: string;
  EventGroupTypeId: number;
  GameStatus: string;
  HasLive: number;
  HasLiveScore: null | boolean;
  HomeID: number;
  LeagueName: string;
  LeagueId: string;
  HomeName: string;
  IsBreak: null;
  IsHT: boolean;
  IsLive: 1 | 0;
  IsNeutral: boolean;
  IsStartingSoon: boolean;
  LiveStreamingUrl:
    | {
        Priority: number;
        Referrer: string;
        Type: number;
        Url: string;
      }[]
    | null;
  Market: string;
  MarketCount: number;
  MatchCode: null | number;
  MatchId: number;
  MoreInfo: MatchMoreInfo;
  OpenParlay: boolean;
  ParentID: number;
  RunTime: string;
  ShowTime: string;
  oddset: OddsetData[];
}

export interface OddsetData {
  Bettype: number;
  BettypeName: string;
  Category: number;
  Combo: number;
  MarketLineLevel: number;
  MarketStatus: 'running' | 'suspend' | 'closed' | 'closeprice';
  MaxBet: any;
  OddsId: number;
  OutrightTeamId: number;
  /**
   * 比赛时段 ID 1 = 全场 2 = 上半场 3 = 下半场
   */
  PeriodId: 1 | 2 | 3;
  lock?: boolean;
  group: number[];
  sels: SelType[];
}

export interface SelType {
  BetTypeSelectionId: number;
  EuropePrice: number;
  Key: string;
  KeyName: string;
  OddsType: number;
  Point: string;
  Price: number;
  Specifiers: any;
  WagerSelectionId: number;
}

export interface MatchEventsData {
  event_version_key: number;
  events: MatchItem[] | null;
}

export interface LeagueListPrams {
  // 赛事区间 1: Today 今日(预设) 、2: Early 早盘 、3: Live 滚球
  periodId: number;
  // 体育项目ID
  sportId: number;
  //选择日期
  selectDate?: boolean;
  // 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
  oddsType: MarketType;
}

export interface ShaBaResults {
  //默认值是1 运动项目id  足球1 篮球2
  gameId: number;
  //获取完场 开始时间
  startTime: String;
  //获取完场 结束时间
  endTime: String;
}
// export interface LeagueItem {
//   // 联赛名称
//   leagueName: string;
//   // 联赛id
//   leagueId: string;
// }

export interface SingleTicketData {
  sport_type: number;
  market_id: number;
  is_decimal_type: boolean;
  point: string;
  key: string;
  bet_type: number;
  odds_type: number;
  price: number;
  status: string;
  max_bet: number;
  min_bet: number;
  live_home_score: number;
  live_away_score: number;
}

export interface PlaceBetPrams {
  // 盘口类型 1、马来盘 2、 香港盘 3、欧洲盘 4、印度尼西亚盘 5、美国盘
  oddsType: MarketType;
  // 运动类型 1、足球 2、篮球 3、美式足球 4、冰上曲棍球 5、网球 6、排球
  sportType: number;
  // 赔率ID
  marketId: string | number;
  // 赔率
  price: number;
  // 投注金额
  stake: number;
  // 点数
  point: number;
  // 赔率选项 0:不接受盘口变更 (预设)、 1:只接受更好赔率、 2:接受任何赔率
  oddsOption?: number;
  // 除会员账号外，以上数据均可从GetMarkets或sbGetEventsAndMarkets接口返回的参数列表中获得选取后传入
  key: string;
}

export interface PlaceParlayBetData {
  combos: CombosItem[];
  price_info: ParlayPriceInfoItem[];
  bet_status: number;
  current_combos: ParlayCombosItem[];
  lucky_trans_id: number;
  max_bet: number;
  min_bet: number;
  parlay_bet_accept_sec: number;
  parlay_ticket_status: string;
  single_tickets: any[];
  sys_trans_id: number;
  total_stake: number;
  trans_id: number;
}

export interface ParlayPriceInfoItem {
  bet_price: number;
  bet_type: number;
  current_price: number;
  key: string;
  live_away_score: number;
  live_home_score: number;
  market_id: number;
  odds_type: number;
  point: number;
  sport_type: number;
  state_code: number;
  state_message: string;
}

export interface ParlayCombosItem {
  bet_count: number;
  combo_price: number;
  combo_type: string;
  min_bet: number;
  max_bet: number;
  odds: number;
  stake: number;
}
export interface CombosItem {
  bet_count?: number;
  combo_type?: string;
  max_bet?: number;
  min_bet?: number;
  odds?: string;
  BetCount?: number;
  ComboType?: string;
  MaxBet?: number;
  MinBet?: number;
  Odds?: number;
  combo_price: number;
  stake: number;
}

export interface PriceInfoItem {
  bet_type: number;
  current_price: number;
  key: string;
  live_away_score: number;
  live_home_score: number;
  market_id: number;
  odds_type: number;
  point: number;
  sport_type: number;
  state_code: number;
  state_message: string;
  status: string;
  combo_price: number;
  stake: number;
}

export type MacthMarketInfoPrams = MatchEventsPrams;

export interface RecommendedMatchData {
  mid: number;
  gid: number;
  home: string;
  away: string;
  startDate: string;
  startTime: string;
  status: number;
  type: string;
}

export interface FinishedMatchItem {
  away: string;
  awayHalf: string;
  awayScore: string;
  gameId: string;
  gameName: string;
  home: string;
  homeHalf: string;
  homeScore: string;
  leagueId: number;
  leagueName: string;
  matchId: number;
  startTime: string;
}

export interface LeagueItem {
  // 联赛名称
  leagueName: string;
  // 联赛id
  leagueId: string;
  // 首字母
  letter?: string;
}
