import { defineComponent, PropType, ref, inject } from 'vue';
import { Popover, Grid } from 'vant';
import { isEmpty, reverse } from 'lodash-es';
import { useTextLive } from '@/hooks/useDetailData';
import { DataEmpty, TeamLogo } from 'kg-mobile/compoents';
import { soccerBallStatus, tipsLabelList } from './live-events-status';
import text from './text-live.module.less';
import { useI18n } from 'vue-i18n';
export default defineComponent({
  name: 'JBBsportTextLive',
  components: { DataEmpty, TeamLogo, VanPopover: Popover, VanGrid: Grid },
  props: {
    realTimelist: {
      type: Array as PropType<any[]>,
      default: () => []
    },
    GetRealTimelist: {
      type: Function,
      default: () => void 0
    }
  },
  setup(props) {
    const { t } = useI18n();
    const { matchInfo } = inject('matchInfo') as any;
    const showPopover = ref(false);
    useTextLive(props.GetRealTimelist);

    return () =>
      isEmpty(props.realTimelist) ? (
        <data-empty title={t('lang.sport_details_noTextLive')} />
      ) : (
        <div class={text.page}>
          <div class={text.header}>
            {t('lang.sport_details_textLive')}
            <van-popover
              v-model:show={showPopover.value}
              overlay={true}
              placement={'right-end'}
              duration={0}
              v-slots={{
                reference: () => <div class={text.headerIcon}>!</div>
              }}
            >
              <van-grid square clickable border={false} column-num={'3'} style={'width: 255px'}>
                <div class={text.Tishi_box}>
                  {tipsLabelList.map((item, index) => (
                    <div class={text.card_item} key={index}>
                      <img class={text.icont_box} src={item.value} />
                      <span class={text.text_s}>{item.title}</span>
                    </div>
                  ))}
                </div>
              </van-grid>
            </van-popover>
          </div>
          {console.log('props.realTimelist', props.realTimelist)}
          {reverse(props.realTimelist.slice(0))?.map((item, index) => (
            <div class={text.content} key={index + '_R_KEY'}>
              <div class={text.contentItem}>
                <div>
                  <img class={text.liveIcon} src={soccerBallStatus[item.type].value} />
                </div>
                <div class={text.liveTextBox}>
                  <div class={text.liveHeader}>
                    <team-logo
                      class={text.logo}
                      teamId={matchInfo.value[item.position === 1 || item.position === 0 ? 'HomeID' : 'AwayID']}
                      size={'xs'}
                    />
                    {item.position === 1 || item.position === 0 ? (
                      <span class={text.liveTeamName}>{matchInfo.value.HomeName}</span>
                    ) : (
                      <span class={text.liveTeamName}>{matchInfo.value.AwayName}</span>
                    )}
                  </div>
                  <div class={text.liveText}>{item.data}</div>
                </div>
              </div>
            </div>
          ))}
        </div>
      );
  }
});
