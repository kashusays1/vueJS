import { defineComponent, onMounted, reactive, watch, onBeforeUnmount } from 'vue';
import { ActionSheet, Sticky } from 'vant';
import DataView from './components/data-view/index';
import detail from './detail.module.less';
import { useDetailHook, useBet, hasTicketId, useI18n, useTimerHook, TimerEnum } from '@/hooks';
import { useRoute } from 'vue-router';
import MatchInfo from './components/match-info';
import Progress from './components/progress/index';
import { chunk } from 'lodash-es';
import { MarketType } from '@/common';
import DetailHeader from './components/detail-header';
import { DataEmpty, Collapse, CollapsePanel } from 'kg-mobile/compoents';

export default defineComponent({
  name: 'JBBsportDetail',
  setup() {
    const route = useRoute();
    const { addTicket } = useBet();
    const { t } = useI18n();

    const { store, showOdds, getData, changeGroup } = useDetailHook();
    const { clearTimer } = useTimerHook();
    const state = reactive({
      isVisible: false,
      expanded: true
    });
    const onChange = value => {
      if (value === 'video') {
        state.isVisible = true;
      } else {
        state.isVisible = false;
      }
    };
    onMounted(async () => {
      const { sportId, periodId, matchId } = route.query;
      console.log('Sel-1:', store);

      store.periodId = Number(periodId);
      await getData({
        oddsType: MarketType.Europe, // Parameters
        sportId: Number(sportId), //SportID
        periodId: Number(periodId),
        eventIds: String(matchId)
      });
    });
    onBeforeUnmount(() => {
      clearTimer(TimerEnum.DETAIL);
      store.errorPage = false;
    });
    watch(
      () => route.query,
      async val => {
        console.log('detail route change', route.path);
        if (route.path !== '/detail') return;
        const { sportId, periodId, matchId } = val;
        store.periodId = Number(periodId);
        await getData({
          oddsType: MarketType.Europe,
          sportId: Number(sportId),
          periodId: Number(periodId),
          eventIds: String(matchId)
        });
      }
    );

    const renderScoreOdds = (sels: any[]) => {
      const [team, [other]] = chunk(sels, 3);
      return (
        <>
          <div class={detail.scoreCloumBox}>
            {team.map(sel => (
              <div class={detail.cloumBox}>
                {sel.map(s => (
                  <div class={detail.oddItem}>
                    <div class={detail.oddItemName}>{s.name}</div>
                    <div class={detail.oddItemPoint}>{s.price}</div>
                  </div>
                ))}
              </div>
            ))}
          </div>
          {other.name ? (
            <div class={`${detail.oddItem} ${detail.otherOddItem}`}>
              <div class={detail.oddItemName}>{other.name}</div>
              <div class={detail.oddItemPoint}>{other.price}</div>
            </div>
          ) : null}
        </>
      );
    };
    return () => (
      <>
        {store.errorPage ? (
          <DataEmpty title={t('lang.sport_common_noAnyData')} />
        ) : (
          <div class={detail.detail}>
            <DetailHeader
              data={{
                leagueName: store.info?.leagueName,
                sportid: store.info?.sportId,
                home: store.info?.home,
                away: store.info?.away,
                periodId: store.periodId,
                date: store.info?.date,
                time: store.info?.time,
                isLive: store.info?.isLive,
                score: store.info?.score || [0, 0],
                sportName: store.info?.sportName
              }}
            />
            {!state.isVisible ? <Progress store={store} fixed={true}></Progress> : null}
            <MatchInfo matchInfo={store.info} onOpenData={() => (store.showSheet = true)} onChange={onChange} />
            {state.isVisible ? <Progress store={store}></Progress> : null}
            <div class={detail.handicapBox}>
              <Sticky offsetTop="1.2rem" class={detail.stickyBox}>
                <div class={detail.handicapTypes}>
                  <div
                    class={[detail.arrowIcon, state.expanded ? '' : detail.rotate]}
                    onClick={() => (state.expanded = !state.expanded)}
                  ></div>
                  <div class={detail.groupBox}>
                    <div class={detail.groupBoxInner}>
                      {store.groups.map(g => (
                        <div
                          class={`${detail.groupItem} ${g.group === store.group ? detail.groupActive : ''}`}
                          onClick={() => changeGroup(g.group)}
                        >
                          {t(g.i18n)}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </Sticky>

              {showOdds.value &&
                showOdds.value.map(odd => (
                  <div class={detail.oddBox}>
                    <Collapse expandAll={state.expanded} key={odd.bettype}>
                      <CollapsePanel
                        v-slots={{
                          header: () => <div class={detail.oddName}>{odd.bettypeName}</div>
                        }}
                      >
                        {
                          <div class={detail.oddsContainer}>
                            {odd.showTeamName && (
                              <div class={detail.oddTeamNameBox}>
                                <div class={detail.oddTeam}>{store.info.home}</div>
                                <div class={detail.oddTeam}>{store.info.away}</div>
                              </div>
                            )}
                            {odd.bettype === 6 || odd.bettype === 158 ? (
                              <div class={detail.oddHandicapBox}>
                                {odd.list.map((item: any) => (
                                  <div class={detail.oddHandicapCloum}>{renderScoreOdds(item.sels)}</div>
                                ))}
                              </div>
                            ) : (
                              odd.list.map((item: any) => (
                                <div class={`${detail.oddHandicapItem} ${item.isOneLine ? detail.oneName : ''}`}>
                                  {item.sels.map((sel: any) => (
                                    <div
                                      class={`${detail.oddItem} ${item.isOneLine ? '' : detail.oddItemLeft} ${
                                        // hasTicketId(store.info.id) ? detail.active : ''
                                        hasTicketId(store.info.id) ? detail.active : ''
                                      }`}
                                      onClick={() =>
                                        addTicket({
                                          bettype: odd.Bettype,
                                          betTypeName: odd.BettypeName,
                                          matchId: store.info.id,
                                          market_id: item.OddsId,
                                          sport_type: store.info.sportId,
                                          odds_type: sel.OddsType,
                                          wagerSelectionId: sel.WagerSelectionId,
                                          price: sel.price,
                                          point: sel.point
                                        } as any)
                                      }
                                    >
                                      <div class={detail.oddItemName}>{sel.name}</div>
                                      <div class={detail.oddItemPoint}>{sel.price}</div>
                                    </div>
                                  ))}
                                </div>
                              ))
                            )}
                          </div>
                        }
                      </CollapsePanel>
                    </Collapse>
                  </div>
                ))}
            </div>
          </div>
        )}
        <ActionSheet teleport="#microApp" class={detail.sheetBox} v-model:show={store.showSheet}>
          <DataView showSheet={store.showSheet} />
        </ActionSheet>
      </>
    );
  }
});
