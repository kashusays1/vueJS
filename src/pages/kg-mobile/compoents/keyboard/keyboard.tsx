/**
 * 数字键盘输入
 */
import { defineComponent, ref, reactive } from 'vue';
import { keyboardExtras, keyboardNumbers } from './config';
import { useClickAway } from '@vant/use';
// import { useBet } from '@/hooks/useBet';
import './keyboard.style.less';
import KeyboardKey from './keyboard-key';

export default defineComponent({
  name: 'KeyboradInput',
  props: {
    max: {
      type: Number,
      default: 0
    },
    min: {
      type: Number,
      default: 0
    },
    // 是否激活显示键盘
    activeKeyboard: {
      type: Boolean,
      default: false
    },
    currency: {
      type: String,
      default: ''
    },
    /**
     * 显示小数
     */
    decimal: {
      type: Boolean,
      default: true
    },
    amount: {}
  },
  slots: {
    parlayName: null
  },
  emits: ['update:amount', 'open-keyboard'],
  setup(props, { emit, slots }) {
    const state = reactive({
      defaultValue: '',
      showKeyboard: props.activeKeyboard
    });
    const keyboardRoot = ref();
    // const { sbWalletBalance } = useBet();

    const sbWalletBalance = ref(9999);

    const onKeyboardSwitch = (isShow: boolean) => {
      state.showKeyboard = isShow;
    };

    useClickAway(keyboardRoot, () => onKeyboardSwitch(false), { eventName: 'touchstart' });

    // 扩展预设数字
    const handleClickExpend = (inputValue: 'MAX' | number) => {
      let newValue: any = '';
      const oldValue = Number(state.defaultValue);
      // 处理数字
      if (typeof inputValue === 'number') {
        newValue = oldValue + inputValue;
      }
      // 处理最大Max
      if (inputValue === 'MAX') {
        newValue = props.max;
      }
      // 检查值是否大于最大 或 者大于钱包
      const flag = newValue >= props.max || newValue >= sbWalletBalance.value;
      if (flag) {
        newValue = props.max - sbWalletBalance.value < 0 ? props.max : Math.floor(sbWalletBalance.value);
      }
      newValue = newValue === '' ? 0 : Number(newValue);
      state.defaultValue = newValue;
      emit('update:amount', newValue || '');
    };

    // 处理普通数字
    const _handleClickNumber = (value: 'del' | '.' | number | any) => {
      let newValue: any = '';
      const oldValue = Number(state.defaultValue);
      // 数字
      if (typeof value === 'number') {
        newValue =
          oldValue === 0 && value === 0 ? '0' : `${state.defaultValue === '0' ? '' : state.defaultValue}${value}`;
      }
      // 处理 .
      if (value === '.') {
        newValue = `${state.defaultValue === '0' ? '' : state.defaultValue}${value}`;
        // ....
      }
      // 处理删除
      if (value === 'del') {
        const oldStrValue = String(oldValue);
        if (!oldStrValue.length) return;
        newValue = oldStrValue.substring(0, oldStrValue.length - 1);
      }
      // 检查值是否大于最大 或 者大于钱包
      const flag = newValue >= props.max || newValue >= Number(sbWalletBalance.value);
      if (flag) {
        newValue = Math.floor(props.max - sbWalletBalance.value < 0 ? props.max : Number(sbWalletBalance.value));
      }
      newValue = newValue === '' ? 0 : Number(newValue);
      state.defaultValue = newValue;
      emit('update:amount', newValue || '');
    };

    const _handleClickNumber_v2 = (value: 'del' | '.' | number) => {
      const vlaueStrArray = String(state.defaultValue).split('');
      if (typeof value === 'number') {
        vlaueStrArray.push(String(value));
      }
      const exportValue = vlaueStrArray.join('');
      emit('update:amount', exportValue || '');
    };

    const KeyboardInput = () => (
      <div class="keyboard-input" onClick={() => onKeyboardSwitch(true)}>
        {slots.default?.()}
        <div class="keyboard-input-container">
          <span class="currency-code">{props.currency}</span>
          <span class="placeholder-label">{props.amount || `限额${props.min || 0}～${props.max || 0}`}</span>
        </div>
      </div>
    );

    return () => (
      <div class="keyboard" ref={keyboardRoot}>
        <KeyboardInput />
        <div class="keyboard-container" v-show={state.showKeyboard}>
          <div class="keyboard-numbers">
            {keyboardNumbers.map((num, index: number) => (
              <div class="btn-item-warp" key={index + '_BTN_KEY'}>
                <KeyboardKey class={{ 'btn-item': true, del: num === 'del' }} onPress={() => _handleClickNumber(num)}>
                  {num === 'del' ? <svg-icon name="del" /> : num}
                </KeyboardKey>
              </div>
            ))}
          </div>
          <div class="keyboard-expand">
            {keyboardExtras.map((num: number | 'MAX', index: number) => (
              <div class="btn-item-warp" key={index + '_BTN_KEY'}>
                <KeyboardKey class={{ 'btn-item': true, max: num == 'MAX' }} onPress={() => handleClickExpend(num)}>
                  {num}
                </KeyboardKey>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
});
