/**
 *  比赛与盘口数据
 */
import { reactive } from 'vue';
import { DataTypeEnum, MarketType, _formatMatchInfo, isMobile, _formatHomeMarketOdds, HomeMarketOdds } from '@/common';
import { MatchItem, EventsAndMarkets, EventsAndMarketsPrams, queryEventsAndMarkets } from '@/services/kg-api';
import { TimerEnum, useFavortMatch, useSportOptins, useTimerHook } from '@/hooks';
import { map, isEmpty } from 'lodash-es';
import { Laoding } from 'kg-mobile/compoents';
import { useStore } from 'vuex';

// import { useStore } from 'vuex';

export interface StoreMatchInfo {
  id: number;
  home: string;
  league: string;
  away: string;
  sportId: number;
  current?: number;
  time: string;
  homeId: number;
  awayId: number;
  date: string;
  leagueName: string;
  sportName: string;
  isNeutral: boolean;
  period: string;
  isLive: boolean;
  market: string;
  count: number;
  score?: number[];
  sections?: [[number, number]];
  video?: boolean;
  animate?: boolean;
  isMark: boolean;
  tech?: {
    ht: [number, number];
    red: [number, number];
    yellow: [number, number];
  };
}

export interface StoreMarketData {
  leagueId: string;
  leagueName: string;
  matchs: {
    sportId: number;
    matchId: number;
    info: StoreMatchInfo;
    oddset: HomeMarketOdds[];
  }[];
}

interface EventStore {
  /**
   * 请求参数
   */
  form: EventsAndMarketsPrams;
  loading: boolean;
  /**
   * 转换市场数据
   */
  marketList: StoreMarketData[];
}

const storeMain = reactive<EventStore>({
  form: {
    oddsType: MarketType.Europe,
    sportId: 1,
    periodId: DataTypeEnum.Live,
    leagueIds: null,
    selectDate: null,
    queryCount: 100,
    eventIds: ''
  },
  loading: true,
  marketList: []
});

function getStore() {
  return reactive<EventStore>({
    form: {
      oddsType: MarketType.Europe,
      sportId: 1,
      periodId: DataTypeEnum.Live,
      leagueIds: null,
      selectDate: null,
      queryCount: 100,
      eventIds: ''
    },
    loading: true,
    marketList: []
  });
}

export function useEventsAndMarkets(useOnlyStore = true) {
  let store = storeMain;
  if (useOnlyStore) {
    store = getStore();
  }
  const vuexStore = useStore();
  const { optionsStore } = useSportOptins();
  const { isMarkedById } = useFavortMatch();
  const { addTimer } = useTimerHook();

  /**
   * 获取市场数据
   * {isPull: 是否为下拉}
   */
  const getEventsAndMarkets = async (isPull = false) => {
    if (isPull) {
      store.loading = true;
    } else {
      isMobile && Laoding.show();
    }
    store.form.oddsType = optionsStore.value.marketType;
    const fn = async () => {
      const [res, data] = (await queryEventsAndMarkets(store.form)) as [boolean, EventsAndMarkets[]];
      if (isPull) {
        // 强制给他搞个延迟
        setTimeout(() => {
          store.loading = false;
        }, 1000);
      } else {
        isMobile && Laoding.hide();
      }
      if (res) {
        const tempList: any[] = [];
        // 转换数据
        if (!isEmpty(data)) {
          map(data, (marketItem: any) => {
            const result = {
              leagueId: marketItem.leagueId,
              leagueName: marketItem.LeagueName,
              matchs: marketItem.matches.map((match: MatchItem) => {
                return {
                  info: {
                    ..._formatMatchInfo({
                      match,
                      league: marketItem,
                      isInProgress: match.IsLive == 1,
                      sportId: marketItem.SportType
                    }),
                    video: !!vuexStore.getters?.getVideoByMid(match.MatchId),
                    animate: !!vuexStore.getters?.getAnimateyByMid(match.MatchId),
                    isMark: isMarkedById({ matchId: match.MatchId, sportId: marketItem.SportType })
                  },
                  oddset: _formatHomeMarketOdds(marketItem.SportType, match.oddset, parseInt(match.periodId, 10))
                };
              })
            };
            tempList.push(result);
          });
        }
        store.marketList = tempList;
        // console.log('store.marketList', store.marketList);
        // await vueStore.dispatch('GET_SOCCERBALL_TECH_IM', data);
      }
    };
    addTimer(TimerEnum.LIST, fn, Number(store.form.periodId) === DataTypeEnum.Live ? 6000 : 60 * 1000);
  };

  return {
    eventStore: store,
    getEventsAndMarkets
  };
}
