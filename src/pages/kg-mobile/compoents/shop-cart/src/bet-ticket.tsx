import { defineComponent } from 'vue';
import SportIcon from '@/components/sport-icon';
import { Checkbox } from 'vant';
import './style/ticket.style.less';
import { SvgIcon } from '@/components';
import { useBet, useI18n } from '@/hooks';

export default defineComponent({
  name: 'BetTicketItem',
  props: {
    bet: {
      type: Object as any,
      required: true
    }
  },
  setup(props, { slots }) {
    const { deleteTicket, state } = useBet();
    const { t } = useI18n();
    return () => (
      <div class={['ticket-item', props.bet.isSameGame && state.tabActive !== 0 ? 'ticket-same' : null]}>
        <div class="ticket-item-info">
          <Checkbox name={props.bet.uniqKey} disabled={props.bet.closed || !props.bet.price}></Checkbox>
          <div class="ticket-item-match">
            <div class="t-header">
              <SportIcon sportId={props.bet.sport_type} size="18" />
              <span class="label">
                {props.bet.keyName} {props.bet.point}
              </span>
              {props.bet.closed || !props.bet.price ? (
                <span class="odd-val error">盘口关闭</span>
              ) : props.bet.combo === 0 && state.tabActive !== 0 ? (
                <span class="odd-val error">不支持串场</span>
              ) : (
                <span class="odd-val">@{props.bet.price}</span>
              )}
            </div>
            <div class="t-type">
              <span>
                {props.bet.periodId === 3 ? <span>{t('lang.sport_handicap_live')} </span> : null}
                {props.bet.betTypeName}
              </span>
            </div>
            <div class="t-teams">
              <span class="t-teams-home">{props.bet.home}</span>
              <span class="vs">VS</span>
              <span class="t-teams-away">{props.bet.away}</span>
              {props.bet.periodId === 3 ? (
                <span class="t-teams-score">{`(${props.bet.live_home_score}-${props.bet.live_away_score})`}</span>
              ) : null}
            </div>
          </div>
          <div class="delelt-it" onClick={() => deleteTicket(props.bet)}>
            <SvgIcon name="remove" />
          </div>
        </div>
        {slots.default?.()}
      </div>
    );
  }
});
