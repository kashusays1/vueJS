import { Modules, StoreUserInfo } from '@/store/types';
import { ActionContext } from 'vuex';
import {
  freeTransferApi,
  getAllPlatformBalanceApi,
  getUserInfoApi,
  PlatformBalance
  // uersSignOutApi
  // refreshTokenApi
} from '@/services';
import { getLoginData } from '@/common/utils';
import { useSportOptins } from '@/hooks';

const initState = (): StoreUserInfo => ({
  nickname: '',
  balance: 0,
  token: '',
  gameBalance: [],
  currency: 'CNY',
  account: '',
  sex: 0,
  freeze: 0,
  gameTotal: 0
});

const state: StoreUserInfo = initState();

const getters = {
  nickname: (state: StoreUserInfo) => state.nickname,
  account: (state: StoreUserInfo) => state.account,
  balance: (state: StoreUserInfo) => state.balance,
  getBlacneBySoure: (state: StoreUserInfo) => (source: string) => {
    const ojb = state.gameBalance?.length > 0 && state.gameBalance.find(e => e.platformCode === source);
    return ojb ? ojb.balance : 0;
  },
  gameBalance: (state: StoreUserInfo) => state.gameBalance,
  isLogin: (state: StoreUserInfo) => !!state.token
};
const actions = {
  // 检查登录
  async CHECK_IS_LOGIN({ dispatch, commit }: ActionContext<StoreUserInfo, Modules>) {
    const authInfo = getLoginData();
    if (!!authInfo.token) {
      commit('UPDATE_USER_INFO', authInfo);
      await dispatch('OAUTH_SUCCESS');
      // await refreshTokenApi({ refreshToken: 'xxxx' });
      return;
    }
    console.error('ERROE: 未登录');
  },
  // 登录成功
  async OAUTH_SUCCESS({ dispatch, commit }: ActionContext<StoreUserInfo, Modules>) {
    const authInfo = getLoginData();
    const { result, data } = await getUserInfoApi();
    const { optionsStore } = useSportOptins();
    if (result) {
      commit('UPDATE_USER_INFO', { ...authInfo, ...data }); // 更新用户信息
      optionsStore.value.currency = data.currency;
      await dispatch('GET_SPORTS_BALANCE');
    }
  },
  async GET_SPORTS_BALANCE({ commit }: ActionContext<StoreUserInfo, Modules>) {
    try {
      const { result, data } = await getAllPlatformBalanceApi();
      if (result) {
        commit('GET_SPORTS_BALANCE_SUCCESS', data);
      } else {
        console.warn(data);
      }
    } catch (_error) {}
  },
  async TRANSFER_INGAME_AMOUNT({}, { platformCode }) {
    try {
      const { result, data } = await freeTransferApi({ platformCode });
      if (!result) {
        throw data || 'Error!';
      }
    } catch (error: any) {
      console.error('error:[B0002]:', error);
    }
  },
  async SIGN_OUT({ commit }: ActionContext<StoreUserInfo, Modules>) {
    // const { result, message } = await uersSignOutApi();
    // if (!result) {
    //   console.error(message);
    //   return;
    // }
    commit('SIGN_OUT_SUCCESS');
  }
};

const mutations = {
  // 更新用户信息
  UPDATE_USER_INFO(state: StoreUserInfo, paylod: StoreUserInfo) {
    Object.assign(state, paylod);
  },
  GET_SPORTS_BALANCE_SUCCESS(state: StoreUserInfo, paylod: PlatformBalance) {
    state.balance = paylod.balance || 0;
    state.freeze = paylod.freeze;
    state.gameTotal = paylod.gameTotal;
    state.gameBalance = paylod.gameBalance || [];
  },
  SIGN_OUT_SUCCESS(state: StoreUserInfo) {
    const defaultVal = initState();
    const keys = Object.keys(defaultVal);
    keys.map(key => {
      state[key] = defaultVal[key];
    });
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
