import { ref } from 'vue';
import { useIntervalFn } from '@vueuse/core';

const getSecond = (time: string): number => {
  if (typeof time !== 'string') return 0;
  const timeArray = time && time.split(':');
  const minute = Number(timeArray[0]);
  const second = Number(timeArray[1]);
  if (!minute && !second) return 0;
  const totalTime = minute * 60 + second;
  return totalTime;
};

const formatTime = (time: number): string => {
  if (!time || typeof time !== 'number') return '00:00';
  const minute = Math.floor(time / 60);
  const second = time % 60;
  return `${String(minute).padStart(2, '0')}:${String(second).padStart(2, '0')}`;
};

export function useTimer() {
  const time = ref(0);
  const addTime = () => (time.value += 1);
  let stop: any = () => {};
  const start = () => {
    const { pause } = useIntervalFn(addTime, 1000);
    stop = pause;
  };
  const getTime = () => time.value;
  return {
    getSecond,
    formatTime,
    getTime,
    start,
    stop
  };
}
