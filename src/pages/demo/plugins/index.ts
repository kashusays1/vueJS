import { App } from 'vue';

export function initPlugin(app: App): void {
  [].forEach((component: any) => app.component(component.name, component));
}
