const log = require('./log');
const api = require('./request');
const spinner = require('./spinner');
const json = require('./util');
const ssh = require('./ssh');
module.exports = {
  spinner,
  log,
  ...api,
  ...json,
  ...ssh
};
