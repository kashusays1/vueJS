/**
 * 运动类型
 */

import { reactive, computed, ComputedRef, nextTick } from 'vue';
import { getEventSummaryApi, EventSummaryApi } from '@/services/kg-api';
import { isArray, isEmpty, sumBy } from 'lodash-es';
import { DataTypeEnum, DATE_TYPES, SPORT_MAP_KEY } from '@/common';
import { useRouter, useRoute } from 'vue-router';
import { localMarketedMatchs } from './useFavortMatch.hook';
import { useAuth } from '@/hooks';

interface SportTypesData extends EventSummaryApi {
  i18n: string;
  follow: number;
  markMatchs: any[];
}
interface StoreData {
  /**
   * 源数据
   */
  sportTypes: SportTypesData[];
  loading: boolean;
  /**
   * 数据统计
   */
  dateCounts: {
    live: number;
    today: number;
    early: number;
  };
}

// export const DateTypeKeys = ['live', 'today', 'early'];
export const DateTypeKeys = {
  3: 'live',
  1: 'today',
  2: 'early',
  '-1': 'follow'
};

interface DataTypeItem {
  label: string;
  i18n: string;
  code: string;
  value?: any;
  index?: string;
  count: number;
}

const store = reactive<StoreData>({
  sportTypes: [],
  dateCounts: {
    live: 0,
    today: 0,
    early: 0
  },
  loading: false
});

export function useSportTypes() {
  const { checkIsLogin } = useAuth();
  const router = useRouter();
  const route = useRoute();

  const activePeriodId = computed(() => Number(route.query.periodId) || 0);
  const activeSportType = computed(() => Number(route.query.sportId) || 0);

  // 统计
  const getSportCountByTab = (item: EventSummaryApi, dataTab: string) => {
    if (isEmpty(store.sportTypes)) return 0;
    return item[dataTab] || 0;
  };

  // 切换运动类目
  const onSportTabchange = ({ sportType }: { sportType: number }) => {
    if (activeSportType.value === sportType) return;
    router.push({
      path: '/',
      query: {
        ...route.query,
        sportId: sportType
      }
    });
  };

  const onDateTabchange = (index: DataTypeEnum) => {
    if (activePeriodId.value === index) return;
    if (index === DataTypeEnum.Followed && !checkIsLogin()) return;
    if (index !== DataTypeEnum.Early) {
      delete route.query.selectDate;
    }
    const effectiveSportId = store.sportTypes.find(e => e[DateTypeKeys[index]] > 0)?.sportType;
    router.push({
      path: '/',
      query: {
        ...route.query,
        sportId: effectiveSportId || 1,
        periodId: index,
        selectDate: 1
      }
    });
  };

  const onWeekDayChange = (dayValue: number | '') => {
    router.push({
      path: '',
      query: {
        ...route.query,
        selectDate: dayValue
      }
    });
  };

  // 时间类型
  const dataTypeComp: ComputedRef<DataTypeItem[]> = computed(() => {
    DATE_TYPES.map(dateItem => {
      dateItem.count = store.dateCounts[dateItem.code];
    });
    return DATE_TYPES;
  });

  /**
   * 获取运动列表
   */
  const getSportTypes = async (): Promise<EventSummaryApi[] | null> => {
    store.loading = true;
    const [res, data] = (await getEventSummaryApi()) as any;
    store.loading = false;
    if (res && isArray(data)) {
      store.sportTypes = data || [];
      // 存一份运动类目枚举到缓存
      const sportsMap = {};
      data.map(item => {
        sportsMap[item.sportType] = item.sportTypeName;
      });
      localStorage.setItem(SPORT_MAP_KEY, JSON.stringify(sportsMap));
    }
    // 类目统计
    store.dateCounts = {
      live: sumBy(data, 'live') || 0,
      today: sumBy(data, 'today') || 0,
      early: sumBy(data, 'early') || 0
    };
    updateMatchMarks();
    return res ? data : null;
  };

  /**
   * 更新关注数量
   */
  const updateMatchMarks = () => {
    !isEmpty(store.sportTypes) &&
      store.sportTypes.map((e: { follow: any; sportType: string | number; markMatchs: any }) => {
        const storageMatch = localMarketedMatchs.value.get(Number(e.sportType)) || [];
        e.follow = storageMatch.length;
        e.markMatchs = storageMatch;
      });
  };

  // 初始化tab参数
  const initTab = async () => {
    const data = await getSportTypes();
    if (!data) return;
    const { sportId, periodId, selectDate } = route.query as any;
    const efficaciousSid = () => {
      const periodKey = DateTypeKeys[periodId];
      // 当前是否有该时间段比赛
      const objById = data.find(e => e.sportType === Number(sportId));
      // 找到第一个有效的运动
      const objByEffect = data.find(e => e[periodKey || 'live'] > 0) || { sportType: sportId };
      return objById && objById[DateTypeKeys[periodId]] > 0 ? sportId : objByEffect.sportType;
    };
    const efficaciousPid = () => {
      return Number(periodId || 3);
    };
    nextTick(() => {
      const query = {
        sportId: efficaciousSid(),
        periodId: efficaciousPid(),
        selectDate: selectDate
      };
      if (efficaciousPid() !== 2) {
        delete query.selectDate;
      }
      router.push({
        path: '/',
        query
      });
    });
  };

  return {
    store,
    initTab,
    getSportTypes,
    activePeriodId,
    onWeekDayChange,
    activeSportType,
    onDateTabchange,
    getSportCountByTab,
    updateMatchMarks,
    onSportTabchange,
    dataTypeComp
  };
}
