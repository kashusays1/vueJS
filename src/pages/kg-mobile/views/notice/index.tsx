import { defineComponent, onMounted, ref } from 'vue';
import './notice.style.less';
import { useRouter } from 'vue-router';

import { lastSevenDays, today } from '@/common/utils';
import { MobileHeader } from '@/components/mobile-header';
import { getPublicMessage } from '@/services';
import { toTime } from '@/common/utils';

import { DataEmpty } from 'kg-mobile/compoents';

export default defineComponent({
  name: 'JBBnotice',
  setup() {
    const router = useRouter();
    const getMessage = ref([]);

    const onHandleBack = () => {
      router.go(-1);
    };
    onMounted(async () => {
      const startDate = lastSevenDays();
      const endDate = today();
      const [res, data] = await getPublicMessage({ startDate, endDate, source: 'im' });
      if (res) {
        getMessage.value = data;
      }
    });

    return () => (
      <div class="notice-detail">
        <MobileHeader onClick-left={onHandleBack} shadow fiexd title="所有公告" />
        {getMessage.value.length > 0 ? (
          <div class="notice-list">
            {getMessage.value.map((item: any, index) => (
              <div class="notice-item" key={index}>
                <div class="list_top">
                  <p class="title_text">赛事公告</p>
                  <p class="title_right">{toTime(item.post_time, 'YYYY-MM-DD HH:mm')}</p>
                </div>
                <div style="padding: 5px 0px">
                  <p class="list_text">{item.content}</p>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <DataEmpty type={'message'} title="暂无消息" desc="暂无任何消息可用" />
        )}
      </div>
    );
  }
});
