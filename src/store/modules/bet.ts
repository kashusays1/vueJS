/**
 * 投注store
 */

import { ActionContext } from 'vuex';
import { BetState, Modules, OddSelItem, SportMatchItem } from '../types';
import { maxBy } from 'lodash-es';
import {
  doPlaceBet,
  //   doPlaceParlayBet,
  getSingleTicket
  //   getParlayTickets,
  //   PlaceBetPrams,
  //   MatchTicketItemComp,
  //   OddInfoComp,
  //   ParlayTicketPrams,
  //   SingleTicketPrams,
  //   TicketItemComp,
  //   ParlayCombosItem
} from '@/services/sb-api';

import { PreBetError, DoBetError } from '@/common/consts';

// 获取预投注错误标签
// const getErrorObj = (code: number): { label: string; code: string; value: number } => {
//   return (
//     PreBetError[code] || {
//       label: '',
//       code: '',
//       value: 0
//     }
//   );
// };

// 获取错误标签
// const getBetErrorObj = (code: number): { label: string; code: string; value: number } => {
//   const ob = DoBetError[code] || {
//     label: '',
//     code: '',
//     value: 0
//   };
//   return ob;
// };

// 初始化States
const initState = (): BetState => ({
  // 单投预投注
  singlePreOddKey: null,
  singlePreBetInfo: {},
  doBetInfo: {
    isSuccess: false,
    doneBet: false,
    message: ''
  },
  // 串投投注列表,
  tickets: [],
  ticketsCombos: [], // 串关组合
  // 串投盘口
  oddsIds: {}
});

// states
const state: BetState = initState();

// getters
const getters = {
  // 投注计数器
  betCounter: (state: BetState) => state.tickets.length,
  singlePreBetInfo: (state: BetState) => state.singlePreBetInfo,
  doBetInfo: (state: BetState) => state.doBetInfo,
  singlePreOddKey: (state: BetState) => state.singlePreOddKey,
  tickets: (state: BetState) => state.tickets,
  ticketsCombos: (state: BetState) => state.ticketsCombos,
  maxCombo: (state: BetState) => {
    const macObj = maxBy(state.tickets, (o: any) => o.oddInfo.Combo);
    return !!macObj ? macObj.oddInfo.Combo : 0;
  }
};

// actions
const actions = {
  /**
   * 获取单投预投注数据
   */
  async GET_PRE_SINGLR_BET_DATA(
    { getters, commit }: ActionContext<BetState, Modules>,
    { matchInfo, sel }: { matchInfo: SportMatchItem['info']; sel: OddSelItem }
  ) {
    const [res, data] = await getSingleTicket({
      market_id: sel.oddsId,
      key: sel.key,
      sport_type: getters.sportId,
      odds_type: 3
    });
    const resData = res
      ? {
          betType: data.bet_type,
          oddsId: data.market_id,
          maxBet: data.max_bet,
          minBet: data.min_bet,
          oddsType: data.odds_type,
          point: data.point,
          status: 0,
          resText: ''
        }
      : {
          maxBet: 0,
          minBet: 0,
          status: data.code,
          resTest: PreBetError[data.code].label
        };

    commit('SET_PRE_SINGLR_BET_DATA', {
      ...matchInfo,
      ...sel,
      ...resData
    });
    commit('SET_PRE_SINGLR_ODD_KEY', sel.selKey);
    return res;
  },

  // 单投投注动作
  async DO_SINGLE_BET_ACTION({ state, getters, commit }: ActionContext<BetState, Modules>, stake: number) {
    console.log('[LOG] 2:单投==>投注请求处理', stake);
    const { oddsType, oddsId, price, point, key } = state.singlePreBetInfo;
    const [res, data] = await doPlaceBet({
      oddsType,
      sportType: getters.sportId,
      marketId: oddsId,
      price,
      stake,
      point,
      key
    });
    commit('SET_BET_INFO', {
      doneBet: true,
      isSccuess: res,
      message: res ? '投注成功' : DoBetError[data.code || 1]?.label || '投注失败'
    });
  },

  CALCEL_PRE_BET_INFO({ commit }: ActionContext<BetState, Modules>) {
    const { singlePreOddKey, singlePreBetInfo, doBetInfo } = initState();
    commit('SET_PRE_SINGLR_ODD_KEY', singlePreOddKey);
    commit('SET_PRE_SINGLR_BET_DATA', singlePreBetInfo);
    commit('SET_BET_INFO', doBetInfo);
  },
  KEEP_PRE_BET_INFO({ commit }: ActionContext<BetState, Modules>) {
    const { doBetInfo } = initState();
    commit('SET_BET_INFO', doBetInfo);
  }
};

// mutations
const mutations = {
  SET_PRE_SINGLR_BET_DATA(state: BetState, payload: any) {
    state.singlePreBetInfo = payload;
  },
  SET_PRE_SINGLR_ODD_KEY(state: BetState, payload: any) {
    state.singlePreOddKey = payload;
  },
  SET_BET_INFO(state: BetState, payload: { isSuccess: boolean; doneBet: boolean; message: string }) {
    state.doBetInfo = payload;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
