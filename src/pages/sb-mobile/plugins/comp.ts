import { Button } from '../components/Button';
import { BetCart, SingleBetDialog, InsufficientBalance } from '../components/BetCart';
import PartLoading from '../components/PartLoading/index.vue';
import NavBar from '../components/NavBar/index.vue';
import EmptyComp from '../components/empty/index.vue';
import TeamLogo from '../components/team-logo/index.vue';
import SvgIcon from '@/components/svg-icon/index.vue';

export default [
  SvgIcon,
  Button,
  BetCart,
  SingleBetDialog,
  InsufficientBalance,
  PartLoading,
  NavBar,
  EmptyComp,
  TeamLogo
];
