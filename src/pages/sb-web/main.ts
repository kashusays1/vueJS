import { createApp } from 'vue';
import '../../public-path';
import 'element-plus/dist/index.css';
import './icons';
import '@/style/index.less';

import App from './app.vue';

// import { setupI18n } from '@/i18n/setupI18n';
import { initPlugin } from './plugins';
import { initRouter } from './router';
import { setupStore } from '@/store';
import { setCssVar } from '@/common/utils';
import { ElLoading } from 'element-plus';

import { setOsType } from '@/plugins';

let instance: any;

interface IRenderProps {
  BASE_URL?: string;
  themeColor?: string;
  themeStyle?: string;
  token?: string;
  id?: number;
}

async function render(props: IRenderProps = {}) {
  const { BASE_URL, themeColor } = props;
  instance = createApp(App);
  // 取消inject使用ref提示
  instance.config.unwrapInjectedRef = true;
  // 注册全局组件
  initPlugin(instance);
  const router = initRouter(BASE_URL);
  instance.use(ElLoading);
  instance.use(router);
  setupStore(instance);
  // 设置国际化
  // await setupI18n(instance);
  await router.isReady();
  // 设置主题色
  !!themeColor && setCssVar(themeColor);
  // 设置ostype变量
  setOsType(0);
  instance.mount('#microApp');
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
} else {
  // @ts-ignore;
  document.getElementById('microApp').className = 'is-micro-app';
}

//暴露主应用生命周期钩子
export async function bootstrap() {
  console.log('[sports App] subapp bootstraped');
}

export async function mount(props: IRenderProps) {
  console.log('[sports App] 启动体育!');
  render(props);
}

export async function unmount() {
  console.log('[sports App] 卸载体育APP!');
  instance.unmount();
  instance = null;
}
