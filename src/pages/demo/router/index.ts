import { Fn } from '#/global';
import { createRouter, createWebHashHistory, RouteLocationNormalized, Router } from 'vue-router';

function registerRouter(router: any) {
  router.beforeEach(async (_to: any, _from: RouteLocationNormalized, next: Fn) => {
    if (window.__POWERED_BY_QIANKUN__) {
      next();
      return;
    }
    document.title = localStorage.getItem('title') || 'KG体育';
    next();
  });
}

export function initRouter(BASE_URL = '/demo'): Router {
  const router = createRouter({
    history: createWebHashHistory(window.__POWERED_BY_QIANKUN__ ? BASE_URL : '/'),
    routes,
    strict: true,
    scrollBehavior: () => ({ left: 0, top: 0 })
  });
  registerRouter(router);
  return router;
}

const routes = [
  {
    path: '/demo',
    name: 'demo',
    component: () => import('../views/demo.vue'),
    meta: {
      title: 'Demo'
    }
  }
];
